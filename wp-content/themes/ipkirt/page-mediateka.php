<?php if ( ! defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

$data = get_theme_part_data();

get_header(); ?>

	<?php get_theme_part( 'section', 'breadcrumbs' ) ?>

	<div class="main__inner-content main__inner-content_two-cols">
		<?php get_sidebar( 'mediateka' ) ?>

		<div class="main__inner-col main__inner-col_section">
			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_theme_part( 'section/page', 'title' ) ?>

				<div class="main__intro entry-content program-content__entry"> 
					<?php the_content() ?>
				</div>

			<?php endwhile; ?>
			
			<?php if ( isset( $data['html'] ) ): ?>
				<section class="<?php echo $data['class'] ?>">
					<?php echo $data['html'] ?>	
				</section>
			<?php endif ?>
		</div>
	</div>

<?php get_footer(); 