<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 
/**
 * Template Name: Единичные события
 */

get_theme_part( 'page', '', array(
	'data' => array(
		'class' => 'events',
		'html'  => get_theme_part( 'section/page/events', 'grid', array(
			'return' => true,
			'data'   => array(
				'post_type' => 'event_single'
			) 
		) )
	)
) );