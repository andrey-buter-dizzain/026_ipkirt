<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 
/**
 * Template Name: Обучение Главная
 */

get_header(); ?>

	<?php get_theme_part( 'section', 'breadcrumbs' ) ?>

	<?php while ( have_posts() ) : the_post(); ?>

		<?php get_theme_part( 'section/page', 'title' ) ?>

		<div class="main__intro">
			<?php the_content() ?>
		</div>

		<?php get_theme_part( 'section/page/study', 'types', array(
			'data' => array(
				'taxonomy' => 'event_study_cat',
				'class'    => 'study-types'
			)
		) ) ?>
		<?php get_theme_part( 'section/page/study', 'events' ) ?>

	<?php endwhile; ?>

<?php get_footer(); ?>