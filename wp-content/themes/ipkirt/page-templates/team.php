<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 
/**
 * Template Name: Наша команда
 */

get_theme_part( 'page', '', array(
	'data' => array(
		'class' => 'team',
		'html'  => get_theme_part( 'section/page/team', 'types', array(
			'return' => true
		) )
	)
) );