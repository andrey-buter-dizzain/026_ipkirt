<?php if ( ! defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 
/**
 * Template Name: Статьи
 */

$data = array(
	'taxonomy'  => 'category',
	'post_type' => 'post',
	'class'     => 'articles',
	'search'    => __( 'Поиск по статьям..' ),
);

$tmpls = array(
	'section/page/articles/search',
	'section/page/articles/tags',
	'section/page/articles/list',
);

$html = array();

foreach ( $tmpls as $path ) {
	$html[] = get_theme_part( $path, '', array(
		'data'   => $data,
		'return' => true
	) );
}

get_theme_part( 'page-mediateka', '', array(
	'data' => array(
		'html'  => implode( "\n", $html ),
		'class' => $data['class']
	)
) );