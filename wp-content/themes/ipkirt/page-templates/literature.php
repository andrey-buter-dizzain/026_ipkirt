<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 
/**
 * Template Name: Литература
 */

$data = array(
	'taxonomy'  => 'literature_cat',
	'post_type' => 'literature',
	'class'     => 'literature',
	'search'    => __( 'Поиск по списку литературы..' ),
);

$tmpls = array(
	'section/page/articles/search',
	'section/page/articles/tags',
	'section/page/articles/list',
);

$html = '';

foreach ( $tmpls as $path ) {
	$html[] = get_theme_part( $path, '', array(
		'data'   => $data,
		'return' => true
	) );
}

get_theme_part( 'page-mediateka', '', array(
	'data' => array(
		'html'  => implode( "\n", $html ),
		'class' => $data['class']
	)
) );