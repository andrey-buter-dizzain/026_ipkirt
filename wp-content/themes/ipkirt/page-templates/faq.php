<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 
/**
 * Template Name: FAQ
 */

get_theme_part( 'page-mediateka', '', array(
	'data' => array(
		'html'  => get_theme_part( 'section/page/faq', 'list', array(
			'return' => true
		) ),
		'class' => 'faq'
	)
) );