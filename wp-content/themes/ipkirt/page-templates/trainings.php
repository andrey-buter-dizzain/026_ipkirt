<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 
/**
 * Template Name: Тренинги
 */

get_theme_part( 'page', '', array(
	'data' => array(
		'class' => 'inner-elements inner-elements_trainings',
		'html'  => get_theme_part( 'section/page/tranings', 'list', array(
			'data'   => 'event_training',
			'return' => true
		) )
	)
) );