<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 
/**
 * Template Name:  СМИ о нас
 */

get_theme_part( 'page-mediateka', '', array(
	'data' => array(
		'html'  => get_theme_part( 'section/page/mass-media', 'list', array(
			'return' => true
		) ),
		'class' => 'msmd'
	)
) );