<?php if ( ! defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 
/**
 * Template Name: Контакты
 */

get_header(); ?>

	<?php get_theme_part( 'section', 'breadcrumbs' ) ?>

	<?php while ( have_posts() ) : the_post(); ?>

		<?php get_theme_part( 'section/page', 'title' ) ?>

		<?php get_theme_part( 'section/page/contacts', 'bar' ) ?>
		
		<div class="main__intro">
			<?php the_content() ?>
		</div>

		<?php get_theme_part( 'section/page/contacts', 'maps' ) ?>

	<?php endwhile; ?>

<?php get_footer(); 