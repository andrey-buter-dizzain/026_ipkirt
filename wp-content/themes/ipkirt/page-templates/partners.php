<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 
/**
 * Template Name: Партнеры
 */

get_theme_part( 'page-mediateka', '', array(
	'data' => array(
		'html'  => get_theme_part( 'section/page/partners', 'list', array(
			'return' => true
		) ),
		'class' => 'partners'
	)
) );