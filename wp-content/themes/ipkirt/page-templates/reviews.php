<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 
/**
 * Template Name: Отзывы
 */

ob_start();
comments_template();
$html = ob_get_clean();

get_theme_part( 'page-mediateka', '', array(
	'data' => array(
		'html'  => $html,
		'class' => 'reviews'
	)
) );