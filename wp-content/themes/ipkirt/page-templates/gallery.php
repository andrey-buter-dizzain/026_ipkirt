<?php if ( ! defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 
/**
 * Template Name: Фото и видео
 */

$data = array(
	'class' => 'gallery',
);

$tmpls = array(
	'section/page/gallery/filter',
	'section/page/gallery/list',
);

$html = '';

foreach ( $tmpls as $path ) {
	$html[] = get_theme_part( $path, '', array(
		'return' => true
	) );
}

get_theme_part( 'page-mediateka', '', array(
	'data' => array(
		'html'  => implode( "\n", $html ),
		'class' => $data['class']
	)
) );