<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 
/**
 * Template Name: Индивидуальный прием
 */

get_header(); ?>

	<?php get_theme_part( 'section', 'breadcrumbs' ) ?>

	<?php while ( have_posts() ) : the_post(); ?>

		<?php get_theme_part( 'section/page', 'title' ) ?>

		<div class="main__intro">
			<?php the_content() ?>
		</div>

		<?php get_theme_part( 'section/page/study', 'types', array(
			'data' => array(
				'taxonomy' => 'specialist_individual',
				'class'    => 'individual-types'
			)
		) ) ?>

	<?php endwhile; ?>

<?php get_footer(); ?>