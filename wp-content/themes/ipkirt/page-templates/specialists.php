<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 
/**
 * Template Name: Специалисты
 */

get_theme_part( 'page', '', array(
	'data' => array(
		'class' => 'specialists-list',
		'html'  => get_theme_part( 'section/page/specialists', 'types', array(
			'return' => true
		) )
	)
) );