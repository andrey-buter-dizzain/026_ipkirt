<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 
/**
 * Template Name: Новости
 */

get_theme_part( 'page', '', array(
	'data' => array(
		'class' => 'inner-elements inner-elements_news',
		'html'  => get_theme_part( 'section/page/news', 'list', array(
			'return' => true
		) )
	)
) );