<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

get_header(); ?>

	<?php get_theme_part( 'section', 'breadcrumbs' ) ?>
	
	<?php while ( have_posts() ) : the_post(); ?>

		<?php get_theme_part( 'section/page', 'title' ) ?>

		<div class="main__intro entry-content <?php echo get_post_type() ?>-content__entry">
			<?php the_content() ?>
		</div>

	<?php endwhile; ?>

<?php get_footer(); 