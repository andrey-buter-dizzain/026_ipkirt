<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 
/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package WordPress
 * @subpackage theme_name
 * @since theme_name 1.0
 */

get_header(); ?>

	<?php get_theme_part( 'section', 'breadcrumbs' ) ?>
	
	<?php while ( have_posts() ) : the_post(); ?>

		<?php get_theme_part( 'section/page', 'title' ) ?>

		<?php get_theme_part( 'content', 'specialist-single' ); ?>

		<?php comments_template( '/comments-specialist.php' ); ?>

	<?php endwhile; ?>

	<?php get_theme_part( 'section/page/specialist', 'related-posts' ) ?>

<?php get_footer(); ?>