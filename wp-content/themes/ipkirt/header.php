<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 
?><!DOCTYPE html>
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[!(IE 8)]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
	<?php do_action( 'theme_after_open_body' ) ?>

	<?php get_theme_part( 'section/svg', 'svg' ) ?>

	<div class="page">
		<?php get_theme_part( 'section', 'header' ) ?>

		<div class="main-wrap">
			<!--main-->
			<main role="main" class="main main-cell">
				<div class="main__inner">
					<div class="main__content">