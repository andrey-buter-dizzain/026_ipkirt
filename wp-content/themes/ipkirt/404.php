<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

get_header(); ?>

	<div class="page-header">
		<h1 class="page-title">
			<?php _e( 'Страница не найдена.', 'ipkirt' ) ?>
		</h1>
	</div>

<?php get_footer(); ?>