<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

get_header(); ?>

	<?php get_theme_part( 'section', 'breadcrumbs' ) ?>
	
	<?php get_theme_part( 'section/page', 'title' ) ?>

	<div class="main__inner-content main__inner-content_lg-sm main__inner-content_mb-lg main__inner-content_two-cols">
		<?php while ( have_posts() ) : the_post(); ?>

			<?php get_theme_part( 'content', 'single-event' ) ?>

		<?php endwhile; ?>

		<?php get_sidebar( 'event' ) ?>
	</div>

<?php get_footer(); ?>