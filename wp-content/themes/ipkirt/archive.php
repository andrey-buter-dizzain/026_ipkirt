<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

get_header(); ?>

	<?php get_theme_part( 'section', 'breadcrumbs' ) ?>

	<?php get_theme_part( 'section/page', 'title' ) ?>

	<div class="main__intro">
		<?php the_archive_description() ?>
	</div>

	<?php if ( have_posts() ) : ?>
		<section class="inner-elements inner-elements_tr-study">
			<ul class="inner-elements__list">
				<?php while ( have_posts() ) : the_post(); ?>
					<?php get_theme_part( 'content', 'event-list-item' ) ?>
				<?php endwhile ?>
			</ul>
		</section>

		<?php get_theme_part( 'section', 'pagination' ) ?>
	<?php endif ?>

<?php get_footer();