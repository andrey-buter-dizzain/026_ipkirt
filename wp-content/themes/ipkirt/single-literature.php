<?php if ( ! defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

get_header(); ?>
	<?php get_theme_part( 'section', 'breadcrumbs' ) ?>

	<div class="main__inner-content main__inner-content_two-cols">
		<?php get_sidebar( 'mediateka' ) ?>

		<div class="main__inner-col main__inner-col_section">
			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_theme_part( 'section/page', 'title' ) ?>

				<section class="book">
					<div class="book__col book__col_img">
						<?php if ( has_post_thumbnail() ): ?>
							<?php the_post_thumbnail( 'literatura' ) ?>
						<?php else: ?>
							<div class="book__img-holder">
								<svg aria-hidden="true" class="icon icon_book">
									<use xlink:href="#icon_book"></use>
								</svg>
							</div>
						<?php endif ?>
					</div>
					<div class="book__col book__col_info">
						<?php the_content() ?>
					</div>
				</section>
			<?php endwhile; ?>
		</div>
	</div>

<?php get_footer(); 