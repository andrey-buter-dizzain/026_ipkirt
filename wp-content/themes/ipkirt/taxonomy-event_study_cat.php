<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

$term         = get_queried_object();
$children_ids = get_term_children( get_queried_object_id(), $term->taxonomy );

if ( $children_ids AND ! is_wp_error( $children_ids ) ) {
	get_theme_part( "taxonomy-{$term->taxonomy}-parent", '', array(
		'data' => array(
			'children_ids' => $children_ids,
			'taxonomy'     => $term->taxonomy
		)
	) );
	die;
}

get_theme_part( 'archive' );
