<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

$blocks = get_theme_option( 'front_bloks' );

get_header(); ?>

	<?php if ( $blocks ): ?>
		<?php foreach ( $blocks as $key => $enabled ): ?>
			<?php if ( $enabled ): ?>
				<?php get_theme_part( 'section/front', $key ) ?>
			<?php endif ?>
		<?php endforeach ?>
	<?php endif ?>

<?php get_footer(); ?>