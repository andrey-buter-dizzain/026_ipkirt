<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

get_header(); ?>

	<?php get_theme_part( 'section', 'breadcrumbs' ) ?>

	<?php get_theme_part( 'section/page', 'title', array(
		'data' => array(
			'title' => __( 'Глобальный поиск' )
		)
	) ) ?>

	<section class="search-global">
		<div class="search-global__search">
			<?php get_search_form() ?>
		</div>
		<ul class="search-global__results">
			<?php while ( have_posts() ) : the_post(); ?>
				<?php get_theme_part( 'content', 'search-list-item' ) ?>
			<?php endwhile; ?>
		</ul>
		<?php get_theme_part( 'section', 'pagination' ) ?>
	</section>

<?php get_footer(); ?>