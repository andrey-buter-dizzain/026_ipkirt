<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

$page_id = get_theme_option( 'gallery_page_id' );

$data = array();

if ( $page_id ) {
	$page = get_post( $page_id );

	$post_content = $page->post_content;

	$data['data']['title'] = $page->post_title;
}

get_header(); ?>

	<?php get_theme_part( 'section', 'breadcrumbs' ) ?>

	<div class="main__inner-content main__inner-content_two-cols">
		<?php get_sidebar( 'mediateka' ) ?>

		<div class="main__inner-col main__inner-col_section">

			<?php get_theme_part( 'section/page', 'title', $data ) ?>

			<?php if ( $page_id ): ?>
				<div class="main__intro entry-content program-content__entry"> 
					<?php echo wpautop( $post_content ) ?>
				</div>
			<?php endif ?>
			
			<section class="gallery">
				<?php get_theme_part( 'section/page/gallery', 'filter' ) ?>

				<?php while( have_posts() ): the_post(); ?>
					<?php get_theme_part( 'content', "gallery-list-item" ) ?>
				<?php endwhile ?>

				<?php get_theme_part( 'section', 'pagination' ) ?>
			</section>
		</div>
	</div>

<?php get_footer();