<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

?>
<div class="field field_search">
	<form role="search" method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>">
		<input type="text" placeholder="<?php _e( 'Поиск..' ) ?>" name="s" class="field__line field__line_search" value="<?php echo get_search_query() ?>">
		<button type="submit" class="button button_search field__button field__button_search">
			<svg aria-hidden="true" class="icon icon_search">
				<use xlink:href="#icon_search"></use>
			</svg>
		</button>
	</form>
</div>