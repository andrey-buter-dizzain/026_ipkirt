<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

$data = get_theme_part_data();

$terms = get_terms( array(
	'taxonomy' => $data['taxonomy'],
	'include'  => $data['children_ids']
) );

get_header(); ?>

	<?php get_theme_part( 'section', 'breadcrumbs' ) ?>

	<?php get_theme_part( 'section/page', 'title' ) ?>

	<div class="main__intro">
		<?php the_archive_description() ?>
	</div>

	<section class="inner-elements inner-elements_therapy-study">
		<?php foreach ( $terms as $term ): ?>
			<div class="inner-elements__type">
				<h2 class="inner-elements__type-title">
					<a href="<?php echo get_term_link( $term ) ?>">
						<?php echo $term->name ?>
					</a>
				</h2>
				<?php get_theme_part( 'section/page/events', 'grid', array(
					'data' => array(
						'pagination' => false,
						'posts_per_page' => 3,
						'post_type' => 'event_study',
						'order'     => 'ASC',
						'tax_query' => array(
							array(
								'taxonomy' => $term->taxonomy,
								'field'    => 'term_id',
								'terms'    => $term->term_id,
							),
						)
					)
				) ) ?>
			</div>
		<?php endforeach ?>
	</section>

<?php get_footer();
