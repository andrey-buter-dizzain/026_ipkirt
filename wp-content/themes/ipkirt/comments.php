<?php if ( ! defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

/**
 * @used by 
 * - page-templates/reviews.php
 * - single-specialist.php
 */

?>
<?php if ( have_comments() ) : ?>
	<ul class="reviews__list">
		<?php wp_list_comments( array(
			'walker' => new Module_Reviews_Walker,
			'style'  => 'li',
		));?>
	</ul>

	<?php get_theme_part( 'section/comments', 'pagination' ); ?>
	
 <?php else : // this is displayed if there are no comments so far ?>

	<?php /*if ( comments_open() ) : ?>
		<!-- If comments are open, but there are no comments. -->

	 <?php else : // comments are closed ?>
		<!-- If comments are closed. -->
		<p class="nocomments"><?php _e( 'Comments are closed.' ); ?></p>

	<?php endif;*/ ?>
<?php endif; ?>

<?php // comment_form(); ?>
