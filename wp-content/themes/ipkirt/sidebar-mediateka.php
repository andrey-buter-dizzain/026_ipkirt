<?php if ( ! defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 


?>
<div class="main__inner-col main__inner-col_nav">
	<div class="aside-nav">
		<?php wp_nav_menu( array( 
			'theme_location' => 'mediateka',
			'container'      => false,
			'menu_class'     => 'sidebar-menu',
			'link_before'    => '<span>',
			'link_after'     => '</span>',
			'depth'          => 2
		) );  ?>		
	</div>
	<div class="aside-nav-mobile-wrap">
		<?php wp_nav_menu( array( 
			'theme_location' => 'mediateka',
			'container'      => false,
			'menu_class'     => 'sidebar-menu-mobile',
			'link_before'    => '<span>',
			'link_after'     => '</span>',
			'depth'          => 2,
		) );  ?>
	</div>
</div>