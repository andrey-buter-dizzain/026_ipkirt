<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

$term         = get_queried_object();
$children_ids = get_term_children( get_queried_object_id(), $term->taxonomy );

if ( $children_ids AND ! is_wp_error( $children_ids ) ) {
	get_theme_part( "taxonomy-{$term->taxonomy}-parent", '', array(
		'data' => array(
			'children_ids' => $children_ids,
			'taxonomy'     => $term->taxonomy
		)
	) );
	die;
}

get_header(); ?>

	<?php get_theme_part( 'section', 'breadcrumbs' ) ?>

	<?php get_theme_part( 'section/page', 'title' ) ?>

	<div class="main__intro">
		<?php the_archive_description() ?>
	</div>

	<?php if ( have_posts() ) : ?>
		<section class="therapists">
			<ul class="grid grid_flex therapists__list">
				<?php while ( have_posts() ) : the_post(); ?>
					<?php get_theme_part( 'content', 'specialist-individual-grid-item' ) ?>
				<?php endwhile ?>
			</ul>
		</section>

		<?php get_theme_part( 'section', 'pagination' ) ?>
	<?php endif ?>

<?php get_footer();
