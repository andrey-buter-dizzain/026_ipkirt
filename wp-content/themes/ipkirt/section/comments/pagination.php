<?php if ( ! defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 
/**
 * @used by 
 * - page-templates/reviews.php
 */

$per_page = absint( get_query_var( 'comments_per_page' ) );
$cpage    = absint( get_query_var( 'cpage', 1 ) );

$count = get_queried_object()->comment_count;

$pages = absint( ceil( $count / $per_page ) );

if ( 1 == $pages )
	return;

$labels = array(
	'prev' => __( '&lsaquo; Предыдущая', 'ipkirt' ),
	'next' => __( 'Следующая &rsaquo;', 'ipkirt' ),
);

$prev_disabled = $next_disabled = '';

if ( 1 == $cpage )
	$prev_disabled = 'pager__item_disabled';

if ( $pages == $cpage )
	$next_disabled = 'pager__item_disabled';

?>
<div class="reviews__pagination reviews__pagination_centered">
	<div class="pager">
		<div class="pager__item pager__item_prev <?php echo $prev_disabled ?>">
			<?php if ( $prev_disabled ): ?>
				<span class="page-numbers prev">
					<?php echo $labels['prev'] ?>
				</span>
			<?php else: ?>
				<a href="<?php echo get_comments_pagenum_link( ( $cpage - 1 ) ) ?>" class="page-numbers prev">
					<?php echo $labels['prev'] ?>
				</a>
			<?php endif ?>
		</div>

		<?php for ( $i = 1; $i <= $pages; $i++ ): 
			$current = ( $cpage == $i ) ? 'pager__item_current' : '';
		?>
			<div class="pager__item <?php echo $current ?>">
				<?php if ( $current ): ?>
					<span class="page-numbers">
						<?php echo $i ?>
					</span>
				<?php else: ?>
					<a href="<?php echo get_comments_pagenum_link( $i ) ?>" class="page-numbers">
						<?php echo $i ?>
					</a>
				<?php endif ?>
			</div>
		<?php endfor; ?>
		<div class="pager__item pager__item_next <?php echo $next_disabled ?>">
			<?php if ( $next_disabled ): ?>
				<span class="page-numbers next">
					<?php echo $labels['next'] ?>
				</span>
			<?php else: ?>
				<a href="<?php echo get_comments_pagenum_link( ( $cpage + 1 ) ) ?>" class="page-numbers next">
					<?php echo $labels['next'] ?>
				</a>
			<?php endif ?>
		</div>
	</div>
</div>