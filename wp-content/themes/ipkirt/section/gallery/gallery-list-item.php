<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

$attachments = get_theme_part_data();

?>
<ul class="gallery-line__list grid grid_flex grid_small">
	<?php foreach ( $attachments as $attachment ): 
		$video = trim( $attachment->post_content );
		$href  = $video ? $video : get_theme_image_src( $attachment->ID, $atts['size'] );
		$img   = get_theme_image_src( $attachment->ID, 'gallery-large' );
	?>
		<li class="gallery-line__item grid__item grid__item_1of5 grid__item_small">
			<a 
				href="<?php echo $href ?>" 
				style="background-image: url(<?php echo $img ?>);" 
				data-rjs="2" 
				<?php if ( empty( $video ) ): ?>
					rel="gr-line-custom" 
				<?php endif ?>
				class="gallery-line__item-link js-popup-gallery popup-gallery-img"
			>
				<?php if ( $video ): ?>
					<span class="gallery-custom__item-link-play"></span>
				<?php endif ?>
			</a>
		</li>
	<?php endforeach ?>
</ul>