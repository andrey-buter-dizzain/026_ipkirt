<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

$attachments = get_theme_part_data();

?>
<div class="mozaic-gal">
	<?php foreach ( $attachments as $key => $attachment ): 
		$size = 0 == $key ? 'large' : 'small';
		$src  = get_theme_image_src( $attachment->ID, "gallery-{$size}" );
	?>
		<div class="mozaic-gal__item">
			<div style="background-image: url(<?php echo $src ?>);" data-rjs="2" class="mozaic-gal__item-img"></div>
		</div>
	<?php endforeach ?>
</div>