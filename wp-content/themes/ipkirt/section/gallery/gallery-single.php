<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

$attachments = get_theme_part_data();

?>
<div class="gallery-custom grid grid_flex">
	<?php foreach ( $attachments as $attachment ): 
		$video = trim( $attachment->post_content );
		$href  = $video ? $video : get_theme_image_src( $attachment->ID, $atts['size'] );
		$img   = get_theme_image_src( $attachment->ID, 'gallery-single' );
	?>
		<div class="gallery-custom__item grid__item grid__item_1of4">
			<a 
				href="<?php echo $href ?>" 
				style="background-image: url(<?php echo $img ?>);" 
				data-rjs="2" 
				rel="gr-line-custom" 
				class="gallery-custom__item-link js-popup-gallery popup-gallery-img"
			>
				<?php if ( $video ): ?>
					<span class="gallery-custom__item-link-play"></span>
				<?php endif ?>
			</a>
		</div>
	<?php endforeach ?>
</div>