<?php if ( ! defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

$data = get_theme_part_data();

?>
<h2 class="main__title title title_bordered title_social">
	<?php if ( isset( $data['title'] ) AND $data['title'] ): ?>
		<?php echo $data['title'] ?>
	<?php elseif ( is_singular() ): ?>
		<?php the_title() ?>
	<?php else: ?>
		<?php theme_archive_title() ?>
	<?php endif ?>

	<?php get_theme_part( 'section', 'socials', array(
		'data' => array(
			'class' => 'social_insider'
		)
	) ) ?>
</h2>