<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

/**
 * @used by 
 * - page-templates/events.php
 * - taxonomy-event_study_cat-parent.php
 */

$data = get_theme_part_data();

$default = array(
	'paged' => get_query_var( 'paged' ),
	'pagination' => true
);

$args = wp_parse_args( $data, $default );

query_posts( $args );

if ( ! have_posts() )
	return wp_reset_query();

?>
<ul class="events__list grid grid_flex">
	<?php while ( have_posts() ): the_post(); ?>
		<?php get_theme_part( 'content', 'event-grid-item' ) ?>
	<?php endwhile ?>
</ul>

<?php if ( $args['pagination'] ): ?>
	<?php get_theme_part( 'section', 'pagination' ) ?>
<?php endif; ?>

<?php wp_reset_query();