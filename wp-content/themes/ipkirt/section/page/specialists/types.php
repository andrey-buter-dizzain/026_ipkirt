<?php if ( ! defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

$terms = get_terms( array(
	'taxonomy' => 'specialist_cat',
	'orderby'  => 'term_order'
) );

if ( empty( $terms ) OR is_wp_error( $terms ) )
	return;

?>
<?php foreach ( $terms as $term ): ?>
	<div class="specialists-list__item">
		<h3 class="specialists-list__item-title">
			<?php echo $term->name ?>
		</h3>
		<?php get_theme_part( 'section/page/specialists', 'list', array(
			'data' => array(
				'term' => $term
			)
		) ) ?>
	</div>
<?php endforeach ?>