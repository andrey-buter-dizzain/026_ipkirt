<?php if ( ! defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

$data = get_theme_part_data();

$term = $data['term'];

$args = array(
	'post_type' => 'specialist',
	'posts_per_page' => -1,
	'tax_query' => array(
		array(
			'taxonomy' => $term->taxonomy,
			'field'    => 'term_id',
			'terms'    => $term->term_id
		),
	),
);

$p = new WP_Query( $args );

if ( ! $p->have_posts() )
	return wp_reset_postdata();

?>
<ul class="grid grid_flex specialists-list__sub-list">
	<?php while( $p->have_posts() ): $p->the_post() ?>
		<?php get_theme_part( 'content', 'specialist-cat-grid-item' ) ?>
	<?php endwhile ?>
</ul>
<?php wp_reset_postdata();