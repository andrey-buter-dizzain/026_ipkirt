<?php if ( ! defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

/**
 * @used by
 * - page-templates/articles.php
 * - page-templates/literature.php
 */

$data  = get_theme_part_data();
$terms = get_terms( array(
	'taxonomy' => $data['taxonomy']
) );

if ( empty( $terms ) )
	return;

if ( is_wp_error( $terms ) )
	return;

?>
<div class="library__tag-cloud">
	<ul class="tag-cloud">
		<?php foreach ( $terms as $term ): 
			$class = theme_obj()->media_library()->is_term_selected( $term->term_id ) ? 'tag-cloud__item_selected' : '';

			$url = theme_obj()->media_library()->get_url_by_term_id( $term->term_id );
		?>
			<li class="tag-cloud__item <?php echo $class ?>">
				<a href="<?php echo $url ?>" class="tag-cloud__link">
					<?php echo $term->name ?>
				</a>
			</li>
		<?php endforeach ?>
	</ul>
</div>