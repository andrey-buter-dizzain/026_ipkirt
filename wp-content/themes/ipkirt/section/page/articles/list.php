<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

/**
 * @used by
 * - page-templates/articles.php
 * - page-templates/literature.php
 */

$data = get_theme_part_data();

$args = array(
	'post_type' => $data['post_type'],
	'paged'     => get_query_var( 'paged' ),
	's'         => isset( $_GET['ss'] ) ? $_GET['ss'] : '',
);


if ( isset( $data['posts_per_page'] ) )
	$args['posts_per_page'] = $data['posts_per_page'];

$meta_query = theme_obj()->media_library()->get_meta_query( $data['taxonomy'] );

if ( $meta_query )
	$args['tax_query'] = $meta_query;

query_posts( $args );

?>
<ul class="<?php echo $data['class'] ?>-list">
	<?php if ( have_posts() ): ?>
		<?php while( have_posts() ): the_post(); ?>
			<?php get_theme_part( 'content', "{$data['post_type']}-list-item" ) ?>
		<?php endwhile ?>
	<?php else: ?>
		<li class="articles__item">
			<?php _e( 'Ничего не найдено.' ) ?>
		</li>
	<?php endif ?>
</ul>

<?php get_theme_part( 'section', 'pagination' ) ?>

<?php wp_reset_query();