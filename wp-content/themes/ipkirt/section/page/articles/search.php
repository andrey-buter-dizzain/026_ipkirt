<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

/**
 * @used by
 * - page-templates/articles.php
 * - page-templates/literature.php
 */

$data = get_theme_part_data();

$library = theme_obj()->media_library();

?>
<div class="library__search">
	<form class="search" role="search" method="get" action="<?php echo $library->get_current_url(); ?>">
		<div class="field field_search field_search-alone">
			<input type="text" placeholder="<?php echo $data['search'] ?>" name="ss" class="field__line field__line_search field__line_search-alone" value="<?php echo $_GET['ss'] ?>">
			<button type="submit" class="button button_search field__button field__button_search">
				<svg aria-hidden="true" class="icon icon_search">
					<use xlink:href="#icon_search"></use>
				</svg>
			</button>
		</div>
		<?php if ( $library->get_tags() ): ?>
			<input type="hidden" name="<?php echo $library::ATTR ?>" value="<?php echo $library->get_inline_tags() ?>">
		<?php endif ?>
	</form>
</div>