<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

/**
 * @used by 
 * - page-templates/groups.php
 * - page-templates/trainings.php
 */

$post_type = get_theme_part_data();

$args = array(
	'post_type' => $post_type,
	'paged'     => get_query_var( 'paged' ),
	'order'     => 'ASC',
);

query_posts( $args );

if ( ! have_posts() )
	return wp_reset_query();

?>
<ul class="inner-elements__list">
	<?php while ( have_posts() ) : the_post(); ?>
		<?php get_theme_part( 'content', 'event-list-item' ) ?>
	<?php endwhile ?>
</ul>

<?php get_theme_part( 'section', 'pagination' ) ?>

<?php wp_reset_query();