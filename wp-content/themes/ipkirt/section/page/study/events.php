<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

/**
 * @used by 
 * - page-templates/study.php
 */

$count = absint( get_the_post_meta( 'page_study_count_type' ) );

$args = array(
	'post_type' => 'event_study',
	'posts_per_page' => $count ? $count : 5,
	'order' => 'ASC'
);

$p = new WP_Query( $args );

?>
<section class="coming-events">
	<h2 class="coming-events__title title">
		<?php _e( 'Ближайшие события' ) ?>
	</h2>
	<?php if ( ! $p->have_posts() ): ?>
		<p><?php _e( 'Ближайших событий нет' ) ?></p>
	<?php else: ?>
		<?php $p->the_post(); ?>
		<?php get_theme_part( 'content', 'event-study-first' ) ?>

		<ul class="coming-events__list grid grid_flex">
			<?php while( $p->have_posts() ): $p->the_post() ?>
				<?php get_theme_part( 'content', 'event-study-simple' ) ?>
			<?php endwhile ?>
		</ul>

		<?php 
			// по дизайну кнопка есть, 
			// но было решено убрать, 
			// т.к. нет страницы для этих событий 
		?>
		<?php /* ?>
		<div class="coming-events__buttons">
			<a href="#" class="button button_miw-200 button_more button_w-a coming-events__button">
				<?php _e( 'Все события' ) ?> &rsaquo;
			</a>
		</div>
		<?php */ ?>
	<?php endif ?>
</section>