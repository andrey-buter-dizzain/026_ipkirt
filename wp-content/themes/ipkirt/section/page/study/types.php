<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

/**
 * @used by 
 * - page-templates/events.php
 * - page-templates/individual.php
 */

$data = get_theme_part_data();

$terms = get_terms( array(
	'taxonomy'   => $data['taxonomy'],
	'hide_empty' => true,
	'parent'     => 0,
	'orderby'    => 'term_order',
) );

if ( is_wp_error( $terms ) )
	return;

if ( ! is_array( $terms ) )
	return;

?>
<section class="<?php echo $data['class'] ?>">
	<ul class="grid grid_flex <?php echo $data['class'] ?>__list">
		<?php foreach ( $terms as $term ): 
			$src = function_exists( 'z_taxonomy_image_url' ) ? z_taxonomy_image_url( $term->term_id, 'medium' ) : '';
		?>
			<li class="grid__item grid__item_1of3 <?php echo $data['class'] ?>__list-item grid__item_card">
				<div class="card card_wide-simple">
					<a class="card__img-wrap" href="<?php echo get_term_link( $term ) ?>">
						<div style="background-image: url(<?php echo $src ?>);" data-rjs="2" class="card__img">
						</div>
					</a>
					<div class="card__body">
						<h4 class="card__title">
							<a href="<?php echo get_term_link( $term ) ?>" class="card__title-link">
								<?php echo $term->name ?>
							</a>
						</h4>
						<div class="card__content">
							<?php echo wp_trim_words( $term->description, 37 ) ?>
							<a href="<?php echo get_term_link( $term ) ?>" class="card__content-link">
								<?php _e( 'Узнать больше &rsaquo;' ) ?>
							</a>
						</div>
					</div>
				</div>
			</li>
		<?php endforeach ?>
	</ul>
</section>