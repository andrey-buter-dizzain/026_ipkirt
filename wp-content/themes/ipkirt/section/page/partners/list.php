<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

/**
 * @used by 
 * - page-templates/partners.php
 */

$items = get_the_post_meta( 'page_partners_items' );

if ( ! $items )
	return;

?>
<ul class="grid grid_flex-table partners__list">
	<?php foreach ( $items as $item ): 
		$src = get_theme_option_image_src( $item['logo'], 'full' );
	?>
		<li class="grid__item grid__item_1of5 partners__item">
			<div class="partners__item-inner">
				<?php if ( $item['url'] ): ?>
					<a href="<?php echo $item['url'] ?>" class="partners__item-link" title="<?php echo $item['title'] ?>">
				<?php else: ?>
					<span class="partners__item-link">
				<?php endif ?>
						<img src="<?php echo $src ?>" data-rjs="2" class="partners__item-img" alt="<?php echo $item['title'] ?>">
				<?php if ( $item['url'] ): ?>
					</a>
				<?php else: ?>
					</span>
				<?php endif ?>
			</div>
		</li>
	<?php endforeach ?>
</ul>