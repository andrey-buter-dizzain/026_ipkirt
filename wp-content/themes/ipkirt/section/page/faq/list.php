<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

/**
 * @used by 
 * - page-templates/faq.php
 */

$items = get_the_post_meta( 'page_faq_items' );

if ( ! $items )
	return;

?>
<?php foreach ( $items as $item ): ?>
	<div class="faq__el">
		<div class="faq__el-head">
			<?php echo $item['title'] ?>
			<span class="faq__el-head-icon"></span>
		</div>
		<div class="faq__el-content">
			<?php echo wpautop( $item['text'] ) ?>
		</div>
	</div>
<?php endforeach ?>