<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

/**
 * @used by 
 * - page-templates/news.php
 */

$args = array(
	'post_type' => 'news',
	'paged'     => get_query_var( 'paged' ),
);

query_posts( $args );

if ( ! have_posts() )
	return wp_reset_postdata();

?>
<ul class="inner-elements__list">
	<?php while( have_posts() ): the_post(); ?>
		<?php get_theme_part( 'content', 'news-list-item' ) ?>
	<?php endwhile ?>
</ul>

<?php get_theme_part( 'section', 'pagination' ) ?>

<?php wp_reset_query();