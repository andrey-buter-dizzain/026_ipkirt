<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

/**
 * @used by 
 * - page-templates/mass-media.php
 */

$args = array(
	'post_type' => 'mass-media',
	'paged'     => get_query_var( 'paged' ),
);

query_posts( $args );

?>
<ul class="msmd__list">
	<?php if ( have_posts() ): ?>
		<?php while( have_posts() ): the_post(); ?>
			<?php get_theme_part( 'content', 'mass-media-list-item' ) ?>
		<?php endwhile ?>
	<?php else: ?>
		<li class="msmd__item">
			<?php _e( 'Ничего не найдено.' ) ?>
		</li>
	<?php endif ?>
</ul>

<?php get_theme_part( 'section', 'pagination' ) ?>

<?php wp_reset_query();