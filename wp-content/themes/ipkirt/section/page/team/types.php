<?php if ( ! defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

$terms = get_terms( array(
	'taxonomy' => 'specialist_team',
	'orderby'  => 'term_order'
) );

if ( empty( $terms ) OR is_wp_error( $terms ) )
	return;

?>
<?php foreach ( $terms as $term ): ?>
	<div class="team__item">
		<h3 class="team__item-title">
			<?php echo $term->name ?>
		</h3>
		<?php get_theme_part( 'section/page/team', 'list', array(
			'data' => array(
				'term' => $term
			)
		) ) ?>
	</div>
<?php endforeach ?>