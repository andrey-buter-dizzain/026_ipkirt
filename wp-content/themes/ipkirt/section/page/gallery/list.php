<?php if ( ! defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

/**
 * @used by
 * - page-templates/gallery.php
 */

$args = array(
	'post_type' => 'gallery',
	'paged'     => get_query_var( 'paged' ),
);

$year = wp_cache_get( 'theme_gallery_year' );

if ( $year ) {
	$args['year'] = $year;
}

query_posts( $args );

if ( ! have_posts() )
	return wp_reset_query();

?>
<?php while( have_posts() ): the_post(); ?>
	<?php get_theme_part( 'content', "gallery-list-item" ) ?>
<?php endwhile ?>

<?php get_theme_part( 'section', 'pagination' ) ?>

<?php 
wp_reset_query();
wp_cache_delete( 'theme_gallery_year' );