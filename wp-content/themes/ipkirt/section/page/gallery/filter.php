<?php if ( ! defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

$page_id = get_theme_option( 'gallery_page_id' );

if ( ! $page_id )
	return;

$permalink = get_permalink( $page_id );

$archives = trim( wp_get_archives( array(
	'type' => 'yearly',
	'post_type' => 'gallery',
	'format' => 'custom',
	'echo' => false
)));

preg_match_all( '/<a(.*)>([0-9]{4})<\/a>/', $archives, $matches );

$current_year = get_query_var( 'year' );

if ( empty( $current_year ) ) {
	wp_cache_set( 'theme_gallery_year', $matches[2][0] );
}

?>
<div class="gallery-filter">
	<h4 class="gallery-filter__title">
		<?php _e( 'Фильтр:' ) ?>
	</h4>
	<ul class="gallery-filter__list">
		<?php foreach ( $matches[2] as $key => $year ): 
			 $current = false;
			if ( $current_year ) {
				if ( $current_year == $year )
					$current = true; 
			} else
			if ( 0 == $key ) {
				$current = true; 
			}

			$class = $current ? 'gallery-filter__item_current' : '';

			$link = 0 == $key ? $permalink : "{$permalink}archive-{$year}/";
		?>
			<li class="gallery-filter__item <?php echo $class ?>">
				<?php if ( $current ): ?>
					<span>
						<?php echo $year ?>
					</span>
				<?php else: ?>
					<a href="<?php echo $link ?>">
						<?php echo $year ?>
					</a>
				<?php endif ?>
			</li>
		<?php endforeach ?>
	</ul>
</div>