<?php if ( ! defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

$ids = theme_obj()->related_posts()->get_post_ids( get_the_ID() );

if ( ! $ids )
	return;

$post_types = array(
	'event_single',
	'event_traning',
	'event_group',
	'event_study',
);

$args = array(
	'post_super_type' => 'event',
	'post_type' => $post_types,
	'post__in'  => $ids,
	'posts_per_page' => -1,
);

$p = new WP_Query( $args );

if ( ! $p->have_posts() )
	return wp_reset_postdata();

$name = get_the_post_meta( 'specialist_name' );

?>
<section class="specialist-programms">
	<?php if ( $name ): ?>
		<h2 class="specialist-programms__title">
			<?php _e( 'Программы' ) ?> <?php echo $name ?>
		</h2>
	<?php endif ?>
	<ul class="grid grid_flex specialist-programms__list">
		<?php while( $p->have_posts() ): $p->the_post(); ?>
			<?php get_theme_part( 'content', 'event-grid-item' ) ?>
		<?php endwhile ?>
	</ul>
</section>