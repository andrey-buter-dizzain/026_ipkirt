<?php if ( ! defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

/**
 * @used by
 * - page-templates/contacts.php
 */

$phones  = get_theme_option( 'phones' );
$address = get_theme_option( 'address' );
$email   = get_theme_option( 'email' );
$socials = get_theme_option( 'social-links' );

?>
<ul class="contacts-bar">
	<?php if ( $phones ): 
		$phones = array_filter( explode( "\n", $phones ) );
	?>
		<li class="contacts-bar__col">
			<h4 class="contacts-bar__col-title">
				<?php _e( 'Наши телефоны:' ) ?>
			</h4>
			<?php foreach ( $phones as $phone ): ?>
				<div class="contacts-bar__col-el">
					<a href="tel:<?php echo $phone ?>">
						<?php echo $phone ?>
					</a>
				</div>
			<?php endforeach ?>
		</li>
	<?php endif ?>
	<?php if ( $address ): ?>
		<li class="contacts-bar__col">
			<h4 class="contacts-bar__col-title">
				<?php _e( 'Наш адрес:' ) ?>
			</h4>
			<div class="contacts-bar__col-el">
				<a href="#" class="link_dotted">
					<?php echo $address ?>
				</a>
			</div>
		</li>
	<?php endif ?>
	<?php if ( $email ): ?>
		<li class="contacts-bar__col">
			<h4 class="contacts-bar__col-title">
				<?php _e( 'Наш email:' ) ?>
			</h4>
			<div class="contacts-bar__col-el">
				<a href="mailto:<?php echo $email ?>">
					<?php echo $email ?>
				</a>
			</div>
		</li>
	<?php endif ?>
	<?php if ( $socials ): ?>
		<li class="contacts-bar__col">
			<h4 class="contacts-bar__col-title">
				<?php _e( 'Мы в сетях:' ) ?>
			</h4>
			<div class="contacts-bar__col-el">
				<?php get_theme_part( 'section', 'socials-simple', array(
					'data' => array(
						'class' => 'social_contacts',
					)
				) ) ?>
			</div>
		</li>
	<?php endif ?>
</ul>