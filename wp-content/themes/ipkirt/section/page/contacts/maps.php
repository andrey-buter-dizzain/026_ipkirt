<?php if ( ! defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 
/**
 * @used by
 * - page-templates/contacts.php
 */

$items = get_the_post_meta( 'page_contacts_maps' );

if ( ! $items )
	return;

?>
<ul class="location">
	<?php foreach ( $items as $item ): ?>
		<li class="location__item">
			<?php if ( $item['title'] ): ?>
				<h3 class="location__title">
					<?php echo $item['title'] ?>
				</h3>
			<?php endif ?>
			<?php if ( $item['sub_title'] ): ?>
				<h4 class="location__subtitle">
					<?php echo $item['sub_title'] ?>
				</h4>
			<?php endif ?>
			<?php if ( $item['text'] ): ?>
				<div class="location__text">
					<?php echo wpautop( $item['text'] ) ?>
				</div>
			<?php endif ?>
			<?php if ( $item['link'] AND $item['image'] ): 
				$src = get_theme_option_image_src( $item['image'], 'full' );
			?>
				<a href="<?php echo $item['link'] ?>" style="background-image: url(<?php echo $src ?>);" data-rjs="2" class="location__map location__map_left" target="_blank"></a>
			<?php endif ?>
		</li>
	<?php endforeach ?>
</ul>