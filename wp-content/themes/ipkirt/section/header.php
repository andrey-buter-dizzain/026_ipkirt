<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

?>
<div class="header-wrap">
	<div class="header-cell">
		<!--header-->
		<header id="header" class="container header">
			<div class="container header__inner">
				<div class="header__inner-top">
					<div class="header__logo">
						<a id="site-title" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" title="<?php bloginfo( 'name' ); ?>">
							<svg aria-hidden="true" class="icon icon_logo"><use xlink:href="#icon_logo"></use></svg>
						</a>
					</div>

					<?php get_theme_part( 'section/header', 'actions' ) ?>
					
					<div class="header__contacts">
						<!--header-contacts-->
						<?php get_theme_part( 'section/header', 'contacts' ) ?>
						<!--header-contacts END-->
					</div>
				</div>
				<nav class="header__menu" role="navigation">
					<?php wp_nav_menu( array( 
						'theme_location' => 'primary',
						'container'      => false,
						'menu_class'     => 'menu',
						// 'link_before'    => '<span>',
						// 'link_after'     => '</span>',
						'depth'          => 2
					) ); ?>
				</nav>
			</div>
		</header>
		<!--header END-->
	</div>
</div>