<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

$title = get_theme_option( 'front_next_events_title' );
$posts = get_theme_option( 'front_next_events_posts' );

$args = array(
	// see inc/event/custom.php
	'post_super_type' => 'event',
	'post_type' => 'any',
	'post__in'  => wp_list_pluck( $posts, 'post_id' ),
	'posts_per_page' => -1,
	'orderby' => 'ASC'
);

$p = new WP_Query( $args );

$p = events_reorder_query( $p );

if ( ! $p->have_posts() )
	return wp_reset_postdata();

?>
<section class="coming-events">
	<?php if ( $title ): ?>
		<h2 class="coming-event__title title">
			<?php echo $title ?>
		</h2>
	<?php endif ?>
	<?php $p->the_post(); ?>
	<?php get_theme_part( 'content', 'event-study-first' ) ?>
	
	<ul class="coming-events__list grid grid_flex">
		<?php while( $p->have_posts() ): $p->the_post() ?>
			<?php get_theme_part( 'content', 'event-study-simple' ) ?>
		<?php endwhile ?>
	</ul>

	<?php /* ?>
		<div class="coming-events__buttons">
			<a href="#" class="button button_miw-200 button_more button_w-a coming-events__button">Все события &rsaquo;</a>
		</div>
	<?php */ ?>
</section>
<?php wp_reset_postdata();