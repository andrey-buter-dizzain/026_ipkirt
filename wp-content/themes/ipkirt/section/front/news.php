<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

$args = array(
	'post_type' => 'news',
	'posts_per_page' => 4
);

$p = new WP_Query( $args );

if ( ! $p->have_posts() )
	return wp_reset_postdata();

$news_id = get_theme_option( 'news_page_id' );

?>
<!--news-->
<section class="news">
	<h2 class="news__title title title_subscribe">
		<?php _e( 'Новости' ) ?>
		<a href="#subscribe" class="button news__title-button js-fancybox">
			<?php _e( 'Подписаться на рассылку' ) ?>
		</a>
	</h2>
	<ul class="grid news__list grid_flex">
		<?php while( $p->have_posts() ): $p->the_post(); ?>
			<?php get_theme_part( 'content', 'front-news' ) ?>
		<?php endwhile ?>
	</ul>
	<?php if ( $news_id ): ?>
		<div class="news__buttons">
			<a href="<?php echo get_permalink( $news_id ) ?>" class="button button_miw-200 button_more button_w-a news__button">
				<?php _e( 'Все новости &rsaquo;' ) ?>
			</a>
		</div>
	<?php endif ?>
</section>
<!--news END-->
<?php wp_reset_postdata();