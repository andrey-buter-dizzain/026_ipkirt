<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

$slides = get_theme_option( 'front_slider_posts' );

if ( ! $slides )
	return;

$args = array(
	// see inc/event/custom.php
	'post_super_type' => 'event',
	'post_type' => 'any',
	'post__in'  => wp_list_pluck( $slides, 'post_id' ),
	'posts_per_page' => -1
);

$p = new WP_Query( $args );

if ( ! $p->have_posts() )
	return wp_reset_postdata();

?>
<div class="main__slider">
	<div class="slider swiper-container">
		<div class="slider__wrapper swiper-wrapper">
			<?php while( $p->have_posts() ): $p->the_post(); ?>
				<?php get_theme_part( 'content', 'front-slide' ) ?>
			<?php endwhile ?>
		</div>
		<div class="slider__pagination swiper-pagination"></div>
	</div>
</div>
<?php wp_reset_postdata();