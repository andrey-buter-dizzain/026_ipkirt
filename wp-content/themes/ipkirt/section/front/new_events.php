<?php if ( ! defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

$title = get_theme_option( 'front_new_events_title' );
$posts = get_theme_option( 'front_new_events_posts' );

$args = array(
	// see inc/event/custom.php
	'post_super_type' => 'event',
	'post_type' => 'any',
	'post__in'  => wp_list_pluck( $posts, 'post_id' ),
	'posts_per_page' => -1,
	'orderby' => 'ASC'
);

$p = new WP_Query( $args );

$p = events_reorder_query( $p );

if ( ! $p->have_posts() )
	return wp_reset_postdata();

?>
<section class="recruiting">
	<?php if ( $title ): ?>
		<h2 class="recruiting__title title">
			<?php echo $title ?>
		</h2>
	<?php endif ?>
	<ul class="grid recruiting__list grid_flex">
		<?php while( $p->have_posts() ): $p->the_post(); ?>
			<?php get_theme_part( 'content', 'front-event-3' ) ?>
		<?php endwhile ?>
	</ul>

	<?php /* ?>
		<div class="recruiting__buttons">
			<a href="#" class="button button_miw-200 button_more button_w-a recruiting__button">Все мероприятия &rsaquo;</a>
		</div>
	<?php */ ?>
</section>
<?php wp_reset_postdata();