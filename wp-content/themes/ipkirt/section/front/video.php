<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

$title  = get_theme_option( 'front_video_title' );
$url    = get_theme_option( 'front_video_url' );
$poster = get_theme_option_image_src( 'front_video_poster', 'full' );

if ( ! $url )
	return;

?>
<section class="video-block">
	<?php if ( $title ): ?>
		<h2 class="video-block__title title">
			<?php echo $title ?>
		</h2>
	<?php endif ?>
	<div class="video-block__player">
		<video id="vid2" controls width="100%" height="200px" poster="<?php echo $poster ?>" data-setup='{ "techOrder": ["youtube"], "sources": [{ "type": "video/youtube", "src": "<?php echo $url ?>"}] }' class="video-js vjs-default-skin"></video>
	</div>

	<?php /* ?>
	<div class="video-block__buttons"><a href="#" class="button button_miw-200 button_more button_w-a video-block__button">Все видео &rsaquo;</a>
	</div>
	<?php */ ?>
</section>