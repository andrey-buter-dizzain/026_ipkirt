<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

$phones  = get_theme_option( 'phones' );
$address = get_theme_option( 'address' );

$phones = array_filter( explode( "\n", $phones ) );

$is_mobile = wp_cache_get( 'theme_header_actions_mobile' );

$contancts_page = get_theme_option( 'contacts_page_id' );

?>
<ul class="header-contacts">
	<li class="header-contacts__item">
		<div class="header-contact">
			<h6 class="header-contact__title">
				<?php _e( 'Наши телефоны' ) ?>
			</h6>
			<ul class="header-contact__inner">
				<?php foreach ( $phones as $phone ): ?>
					<li class="header-contact__inner-item">
						<?php if ( $is_mobile ): ?>
							<a href="tel:<?php echo $phone ?>">
						<?php endif ?>
							<?php echo $phone ?>

						<?php if ( $is_mobile ): ?>
							</a>
						<?php endif ?>
					</li>
				<?php endforeach ?>
			</ul>
		</div>
	</li>
	<li class="header-contacts__item">
		<div class="header-contact">
			<h6 class="header-contact__title">
				<?php _e( 'Наш адрес' ) ?>
			</h6>
			<ul class="header-contact__inner">
				<li class="header-contact__inner-item">
					<a href="<?php echo get_permalink( $contancts_page ) ?>" class="header-contact__link link_dotted">
						<?php echo $address ?>
					</a>
				</li>
			</ul>
		</div>
	</li>
</ul>