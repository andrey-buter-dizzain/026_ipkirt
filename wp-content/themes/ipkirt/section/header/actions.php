<?php if ( ! defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

$ask_qst_id = get_theme_option( 'form_question_page_id' );

?>
<div class="header__actions">
	<!--header-actions-->
	<ul class="header-actions">
		<?php if ( $ask_qst_id ): ?>
			<li class="header-actions__item header-actions__item_no-mobile header-actions__item_question">
				<a href="<?php echo get_permalink( $ask_qst_id ) ?>" class="button header-actions__button">
					<?php _e( 'Задать вопрос' ) ?>
				</a>
			</li>
		<?php endif ?>

		<li class="header-actions__item header-actions__item_menu header-actions__item_mobile-xs">
			<div class="button button_menu button_tab header-actions__button js-show-dropdown">
				<?php _e( 'Меню' ) ?>
				<svg aria-hidden="true" class="icon icon_cross">
					<use xlink:href="#icon_cross"></use>
				</svg>
				<svg aria-hidden="true" class="icon icon_menu">
					<use xlink:href="#icon_menu"></use>
				</svg>
			</div>
			<div class="header-actions__menu-mobile js-dropdown">
				<?php wp_nav_menu( array( 
					'theme_location' => 'primary',
					'container'      => false,
					'menu_class'     => 'menu-mobile',
					'depth'          => 2
				) ); ?>
			</div>
		</li>
		<li class="header-actions__item header-actions__item_search">
			<div class="button button_search button_tab header-actions__button js-show-dropdown">
				<svg aria-hidden="true" class="icon icon_search">
					<use xlink:href="#icon_search"></use>
				</svg>
				<svg aria-hidden="true" class="icon icon_cross">
					<use xlink:href="#icon_cross"></use>
				</svg>
			</div>
			<div class="header-actions__search js-dropdown">
				<!--header-search-->
				<div class="header-search">
					<?php get_theme_part( 'searchform-header' ) ?>
				</div>
				<!--header-search END-->
			</div>
		</li>
		<li class="header-actions__item header-actions__item_contacts header-actions__item_tablet">
			<div class="button button_contacts button_tab header-actions__button js-show-dropdown">
				<?php _e( 'Контакты' ) ?>
				<svg aria-hidden="true" class="icon icon_cross">
					<use xlink:href="#icon_cross"></use>
				</svg>
			</div>
			<div class="header-actions__contacts header-actions__contacts_mobile js-dropdown">
				<!--header-contacts-tablet-->
				<div class="header-contacts header-contacts_tablet">
					<ul class="header-contacts__list">
						<?php wp_cache_set( 'theme_header_actions_mobile', 1 ) ?>
						<?php get_theme_part( 'section/header', 'contacts' ) ?>
						<?php wp_cache_delete( 'theme_header_actions_mobile' ) ?>
					</ul>
					<?php if ( $ask_qst_id ): ?>
						<a href="<?php echo get_permalink( $ask_qst_id ) ?>" class="button header-contacts__button header-contacts__button_mobile">
							<?php _e( 'Задать вопрос' ) ?>
						</a>
					<?php endif ?>
				</div>
				<!--header-contacts-tablet-->
			</div>
		</li>
	</ul>
	<!--header-actions END-->
</div>