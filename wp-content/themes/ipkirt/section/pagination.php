<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

$max_num_pages = 1;

if ( isset( $GLOBALS['wp_query']->max_num_pages ) )
	$max_num_pages = $GLOBALS['wp_query']->max_num_pages;

$args = array(
	'prev_text' => __( '&lsaquo; Предыдущая', 'ipkirt' ),
	'next_text' => __( 'Следующая &rsaquo;', 'ipkirt' ),
	'type'      => 'array'
);

$links = paginate_links( $args );

if ( ! $links )
	return;

$paged = get_query_var( 'paged' ); 

$prev_class = $next_class = '';

$disabled_class = 'pager__item_disabled';

if ( $paged <= 1 ) {
	$prev = "<span class=\"page-numbers\">{$args['prev_text']}</span>";
	$prev_class = $disabled_class;
} else {
	$prev = array_shift( $links );
}

if ( $max_num_pages == $paged ) {
	$next = "<span class=\"page-numbers\">{$args['next_text']}</span>";
	$next_class = $disabled_class;
} else {
	$next = array_pop( $links );
}

?>
<div class="pager">
	<div class="pager__item pager__item_prev <?php echo $prev_class ?>">
		<?php echo $prev ?>
	</div>

	<?php foreach ( $links as $key => $link ): 
		$current = ( false !== strpos( $link, 'current' ) ) ? 'pager__item_current' : '';
	?>
		<div class="pager__item <?php echo $current ?>">
			<?php echo $link ?>
		</div>
	<?php endforeach ?>

	<div class="pager__item pager__item_next <?php echo $next_class ?>">
		<?php echo $next ?>
	</div>
</div>
