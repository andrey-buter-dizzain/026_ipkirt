<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

$data = get_theme_part_data();

$url = $data['url'] ? "url=\"{$data['url']}\"" : '';

$shortcode = "[Sassy_Social_Share $url]";

?>
<div class="social <?php echo $data['class'] ?>">
	<?php echo do_shortcode( $shortcode ) ?>
</div>