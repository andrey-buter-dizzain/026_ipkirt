<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

?>
<?php if ( function_exists( 'bcn_display_list' ) ): ?>
	<ul class="path">
		<?php bcn_display_list() ?>
	</ul>
<?php endif ?>