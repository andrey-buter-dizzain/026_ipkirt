<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

$socials = get_theme_option( 'social-links' );
$data = get_theme_part_data();

if ( ! $socials )
	return;

?>
<ul class="social <?php echo $data['class'] ?>">
	<?php foreach ( $socials as $item ): 
		if ( empty( $item['url'] ) OR empty( $item['icon'] ) )
			continue;
	?>
		<li class="social__item">
			<a href="<?php echo $item['url'] ?>" class="social__item-link social__item-link_<?php echo $item['icon'] ?>" target="_blank">
				<svg aria-hidden="true" class="icon icon_<?php echo $item['icon'] ?>">
					<use xlink:href="#icon_<?php echo $item['icon'] ?>"></use>
				</svg>
			</a>
		</li>
	<?php endforeach ?>
</ul>
	