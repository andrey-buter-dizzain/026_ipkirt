<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

$data = get_theme_part_data();

$classes = array();
$classes[] = is_special_page( 'sitempa_page' ) ? 'sitemap__entry' : '';

$classes[] = isset( $data['disable-entry-content'] ) ? '' : 'entry-content';

get_header(); ?>

	<?php get_theme_part( 'section', 'breadcrumbs' ) ?>

	<?php while ( have_posts() ) : the_post(); ?>

		<?php get_theme_part( 'section/page', 'title' ) ?>

		<div class="main__intro <?php echo implode( ' ', $classes ) ?>">
			<?php the_content() ?>
		</div>

	<?php endwhile; ?>

	<?php if ( isset( $data['html'] ) ): ?>
		<section class="<?php echo $data['class'] ?>">
			<?php echo $data['html'] ?>	
		</section>
	<?php endif ?>

<?php get_footer(); ?>