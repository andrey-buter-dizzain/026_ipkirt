<?php if ( ! defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

$src = get_theme_image_src( get_post_thumbnail_id(), 'full' );

$content = get_the_post_meta( 'specialist_content' );

$consult_url = get_specialist_register_link();

?>
<section class="specialist-info">
	<div class="specialist-info__col specialist-info__col_private">
		<div style="background-image: url(<?php echo $src ?>);" data-rjs="2" class="specialist-info__avatar specialist-info__avatar_mobile">
		</div>
		<div class="entry-content specialist-info__entry">
			<?php the_content() ?>
		</div>
		<div class="grid grid_aside-buttons specialist-info__actions">
			<?php if ( $consult_url ): ?>
				<div class="grid__item specialist-info__action">
					<a href="<?php echo $consult_url ?>" class="button button_miw-200 button_violet button_w-a specialist-info__button">
						<?php _e( 'Записаться на консультацию' ) ?>
					</a>
				</div>
			<?php endif ?>
			<?php if ( false ): ?>
				<div class="grid__item specialist-info__action">
					<a href="#" class="button button_w-a specialist-info__button">
						<?php _e( 'Оплатить' ) ?>
					</a>
				</div>
			<?php endif ?>
		</div>
	</div>
	<div class="specialist-info__col specialist-info__col_pro">
		<div style="background-image: url(<?php echo $src ?>);" data-rjs="2" class="specialist-info__avatar specialist-info__avatar_desktop">
		</div>
		<div class="entry-content specialist-info__entry">
			<?php echo wpautop( $content ) ?>
		</div>
	</div>
</section>