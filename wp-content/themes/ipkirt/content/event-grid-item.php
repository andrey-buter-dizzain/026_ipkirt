<?php if ( ! defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

$src = get_theme_image_src( get_post_thumbnail_id(), 'middle' );

$register_url = get_event_register_link();

?>
<li class="events__list-item grid__item grid__item_1of3 grid__item_card">
	<div class="card card_big-button card_wide-short">
		<a href="<?php the_permalink() ?>" style="background-image: url(<?php echo $src ?>);" data-rjs="2" class="card__img"></a>
		<div class="card__body">
			<?php get_theme_part( 'content/event', 'date' ) ?>

			<h4 class="card__title">
				<a href="<?php the_permalink() ?>" class="card__title-link">
					<?php the_title() ?>
				</a>
			</h4>

			<div class="card__content">
				<?php the_excerpt() ?>
			</div>
			<?php edit_post_link( __( 'Изменить', 'ipkirt' ), '<span class="edit-link">', '</span>' ); ?>
		</div>
		<?php if ( $register_url ): ?>
			<div class="card__buttons">
				<a href="<?php echo $register_url ?>" class="button button_white card__button">
					<?php _e( 'Записаться' ) ?>
				</a>
			</div>
		<?php endif ?>
	</div>
</li>