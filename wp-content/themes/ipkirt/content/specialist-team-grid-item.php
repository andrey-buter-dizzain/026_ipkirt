<?php if ( ! defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

$src = get_theme_image_src( get_post_thumbnail_id(), 'specialist-team-grid' );

$position = get_the_post_meta( 'specialist_position' );

?>
<li class="team__sub-list-item">
	<div class="card card_person card_two-col">
		<a href="<?php the_permalink() ?>" style="background-image: url(<?php echo $src ?>);" data-rjs="2" class="card__img">
		</a>
		<div class="card__body">
			<h3 class="card__title">
				<a href="<?php the_permalink() ?>" class="card__title-link">
					<?php the_title() ?>
				</a>
			</h3>
			<?php if ( $position ): ?>
				<div class="card__position">
					<?php echo $position ?>
				</div>
			<?php endif ?>
			<?php if ( has_excerpt() ): ?>
				<div class="card__content">
					<?php the_excerpt() ?>
				</div>
			<?php endif; ?>
			<?php edit_post_link( __( 'Изменить', 'ipkirt' ), '<span class="edit-link">', '</span>' ); ?>
		</div>
	</div>
</li>