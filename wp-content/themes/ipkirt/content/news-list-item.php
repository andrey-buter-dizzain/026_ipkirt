<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

$src = get_theme_image_src( get_post_thumbnail_id(), 'medium' );

$authors = theme_get_inline_authors( get_the_ID(), false );

?>
<li id="post-<?php the_ID(); ?>" class="inner-elements__item">
	<div class="card card_article-full card_two-col">
		<a href="<?php the_permalink() ?>" style="background-image: url(<?php echo $src ?>);" data-rjs="2" class="card__img"></a>
		<div class="card__body">
			<h3 class="card__title">
				<a href="<?php the_permalink() ?>" title="<?php the_title(); ?>" class="card__title-link">
					<?php the_title(); ?>
				</a>
			</h3>
			<?php if ( $authors ): ?>
				<div class="card__author">
					<?php echo $authors ?>, <?php the_time( 'Y' ) ?> г.
				</div>
			<?php endif ?>
			<div class="card__content">
				<?php the_excerpt() ?>
			</div>
			<?php edit_post_link( __( 'Изменить', 'ipkirt' ), '<span class="edit-link">', '</span>' ); ?>
		</div>
	</div>
</li>