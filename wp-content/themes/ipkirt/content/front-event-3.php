<?php if ( ! defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

$src = get_theme_image_src( get_post_thumbnail_id(), 'front-event' );

?>
<li class="grid__item grid__item_1of3 recruiting__list-item grid__item_card">
	<div class="card card_wide-short">
		<a href="<?php the_permalink() ?>" style="background-image: url(<?php echo $src ?>);" data-rjs="2" class="card__img">
		</a>
		<div class="card__body">
			<?php get_theme_part( 'content/event', 'date' ) ?>

			<?php get_theme_part( 'content/event', 'terms' ) ?>

			<h4 class="card__title">
				<a href="<?php the_permalink() ?>" class="card__title-link">
					<?php the_title() ?>
				</a>
			</h4>
			
			<?php get_theme_part( 'content/event', 'short-desc' ) ?>

			<?php edit_post_link( __( 'Изменить', 'ipkirt' ), '<span class="edit-link">', '</span>' ); ?>
		</div>
	</div>
</li>