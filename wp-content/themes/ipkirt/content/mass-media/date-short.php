<?php if ( ! defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); ?>
<div class="card__date">
	<div class="card-date card-date_short">
		<div class="card-date__month">
			<?php the_time( 'F' ) ?>
		</div>
		<div class="card-date__day">
			<?php the_time( 'j' ) ?>
		</div>
		<div class="card-date__year">
			<?php the_time( 'Y' ) ?>
		</div>
	</div>
</div>