<?php if ( ! defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

$src = get_theme_image_src( get_post_thumbnail_id(), 'front-slide' );

$post_type = get_post_type();

$terms = wp_get_post_terms( get_the_ID(), "{$post_type}_cat", array(
	'parent' => 0
) );

$authors = theme_get_inline_authors( get_the_ID(), true );

?>
<div class="slider__slide swiper-slide">
	<div class="slide">
		<a href="<?php the_permalink() ?>" style="background-image: url(<?php echo $src ?>);" data-rjs="2" class="slide__cell slide__cell_img">
		</a>
		<div class="slide__cell slide__cell_content">
			<?php if ( ! is_wp_error( $terms ) AND $terms ): ?>
				<div class="slide__header">
					<?php foreach ( $terms as $term ): ?>
						<a href="<?php echo get_term_link( $term ) ?>" class="h6 slide__category">
							<?php echo $term->name ?>
						</a>
					<?php endforeach ?>
				</div>
			<?php endif ?>
			<div class="slide__body">
				<h3 class="slide__title">
					<a href="<?php the_permalink() ?>">
						<?php the_title() ?>
					</a>
				</h3>
				<?php if ( $authors ): ?>
					<div class="h5 slide__names">
						<?php echo $authors ?>
					</div>
				<?php endif ?>
				<?php edit_post_link( __( 'Изменить', 'ipkirt' ), '<span class="edit-link">', '</span>' ); ?>
			</div>
		</div>
	</div>
</div>