<?php if ( ! defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 


?>
<li class="literature-list__item">
	<?php the_post_number() ?>. 
	<a href="<?php the_permalink() ?>" class="literature-list__link">
		<?php the_title() ?>
	</a>
	<?php edit_post_link( __( 'Изменить', 'ipkirt' ), '<span class="edit-link">', '</span>' ); ?>
</li>