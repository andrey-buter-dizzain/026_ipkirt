<?php if ( ! defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

$atts     = get_the_post_meta( 'event_atts' );
$discount = get_the_post_meta( 'event_discount_text' );

$reserved = theme_event_reserver_titles();

$register_url = get_event_register_link();

$form_question_id = get_theme_option( 'form_question_page_id' );

?>
<div class="program-info">
	<?php if ( $atts ): ?>
		<h3 class="program-info__title title title_widget">
			<?php _e( 'Информация' ) ?>
		</h3>
		<ul class="program-info__list">
			<?php foreach ( $atts as $attr ): 
				$title = $icon = $payment_anchor = '';

				if ( $attr['title_reserved'] ) {
					// get costant title and icon
					$data  = $reserved[ $attr['title_reserved'] ];
					$title = $data['title'];
					$icon  = $data['icon'];

					if ( 'price' == $attr['title_reserved'] ) {
						$id = get_the_ID();
						$payment_anchor = "#event-discount-data-{$id}";
					}

					if ( 'date' == $attr['title_reserved'] AND empty( $attr['value'] ) ) {
						$unix = get_unix_event_date();
						$day = date( 'j', $unix );
						$month = date_i18n( 'F', $unix );
						$year = date( 'Y', $unix );
						$attr['value'] = "$day $month $year";
					}
				}

				if ( $attr['title_custom'] )
					$title = $attr['title_custom'];

				if ( empty( $title ) )
					continue;

				if ( empty( $attr['value'] ) )
					continue;
			?>
				<li class="program-info__item">
					<?php if ( $icon ): ?>
						<div class="program-info__item-img">
							<svg aria-hidden="true" class="icon icon_<?php echo $icon ?>">
								<use xlink:href="#icon_<?php echo $icon ?>"></use>
							</svg>
						</div>
					<?php endif ?>
					<div class="program-info__item-label">
						<?php echo $title ?>:
					</div>
					<div class="program-info__item-value">
						<?php echo $attr['value'] ?>

						<?php if ( $payment_anchor AND $discount ): ?>
							<a href="<?php echo $payment_anchor ?>">
								<?php _e( '(узнать про скидки)' ) ?>
							</a>
						<?php endif ?>
					</div>
				</li>
			<?php endforeach ?>
		</ul>
	<?php endif ?>

	<div class="program-info__button-wrap">
		<div class="grid grid_aside-buttons">
			<div class="grid__row">
				<?php if ( $register_url ): ?>
					<div class="grid__item grid__item_1of2">
						<a class="button button_violet grid__button" href="<?php echo $register_url ?>">
							<?php _e( 'Записаться' ) ?>
						</a>
					</div>
				<?php endif ?>
				<?php if ( $form_question_id ): ?>
					<div class="grid__item grid__item_1of2">
						<a class="button grid__button" href="<?php echo get_permalink( $form_question_id ) ?>">
							<?php _e( 'Задать вопрос' ) ?>
							<?php //_e( 'Оплатить' ) ?>
						</a>
					</div>
				<?php endif ?>
			</div>
			<?php /* ?>
			<div class="grid__row">
				<div class="grid__item grid__item_fullwidth">
					<a href="#" class="button grid__button">
						<?php _e( 'Задать вопрос' ) ?>
					</a>
				</div>
			</div>
			<?php */ ?>
		</div>
	</div>
</div>