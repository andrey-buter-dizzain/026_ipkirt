<?php if ( ! defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

$articles = get_the_post_meta( 'event_articles' );

if ( ! $articles )
    return;

?>
<div class="program-sidebar__widget program-sidebar__widget_articles">
    <!--program-articles-->
    <div class="page-articles">
        <h3 class="page-articles__title title title_widget">
            <?php _e( 'Статьи по теме' ) ?>
        </h3>
        <ul class="page-articles__list">
            <?php foreach ( $articles as $data ): 
                $title = get_the_title( $data['post_id'] );
                $link  = get_permalink( $data['post_id'] );
            ?>
                <li class="page-articles__item">
                    <a href="<?php echo $link ?>">
                        <?php echo $title ?>&nbsp;&rsaquo;
                    </a>
                </li>
            <?php endforeach ?>
        </ul>
    </div>
</div>