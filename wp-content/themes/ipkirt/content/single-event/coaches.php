<?php if ( ! defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

global $post;

$authors = theme_get_authors();

if ( ! $authors )
    return;
    
$label = get_the_post_meta( 'event_coach_label' );

if ( ! $label )
    $label = 1 == count( $authors ) ? __( 'Преподаватель' ) : __( 'Преподаватели' );

?>
<div class="program-sidebar__widget program-sidebar__widget_teachers">
    <div class="program-teachers">
        <h3 class="program-teachers__title title title_widget">
            <?php echo $label ?>
        </h3>
        <ul class="program-teachers__list">
            <?php foreach ( $authors as $post ): 
                setup_postdata( $post ); 

                $src = get_theme_image_src( get_post_thumbnail_id(), 'specialist-event' );

                $position = get_the_post_meta( 'specialist_position' );
            ?>
                <li class="program-teachers__card">
                    <div class="card card_teacher">
                        <a href="<?php the_permalink() ?>" style="background-image: url(<?php echo $src ?>);" data-rjs="2" class="card__img"></a>
                        <div class="card__body">
                            <h3 class="card__title">
                                <a href="<?php the_permalink() ?>" class="card__title-link">
                                    <?php the_title() ?>
                                </a>
                            </h3>
                            <?php if ( $position ): ?>
                                <div class="card__position">
                                    <?php echo $position ?>
                                </div>
                            <?php endif ?>
                            <?php if ( has_excerpt() ): ?>
                                <div class="card__content">
                                    <?php the_excerpt() ?>
                                </div>
                            <?php endif; ?>
                            <?php edit_post_link( __( 'Изменить', 'ipkirt' ), '<span class="edit-link">', '</span>' ); ?>
                        </div>
                    </div>
                </li>
            <?php endforeach ?>
            <?php wp_reset_postdata(); ?>
        </ul>
    </div>
</div>