<?php if ( ! defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

$src = get_theme_image_src( get_post_thumbnail_id(), 'middle' );

$post_type = get_post_type();

$register_url = get_event_register_link();

$events = array(
	'event_single',
	'event_group',
	'event_traning',
	'event_study'
);

$is_event = in_array( get_post_type(), $events );
$is_news  = 'news' == $post_type;

$classes = array();
$classes[] = $is_event ? '' : 'card_no-calendar';
$classes[] = has_post_thumbnail() ? '' : 'card_no-image';

?>
<li class="search-global__item">
	<div class="card card_search card_two-col <?php echo implode( ' ', $classes ) ?>">
		<?php if ( has_post_thumbnail() ): ?>
			<a href="<?php the_permalink() ?>" style="background-image: url(<?php echo $src ?>);" data-rjs="2" class="card__img"></a>
		<?php endif ?>
		<div class="card__body">
			<?php if ( $is_event ): ?>
				<?php get_theme_part( 'content/event', 'date' ) ?>
			<?php endif ?>
	
			<?php get_theme_part( 'content/event', 'terms' ) ?>

			<h3 class="card__title">
				<a href="<?php the_permalink() ?>" class="card__title-link">
					<?php the_title() ?>
				</a>
			</h3>
			<?php if ( $is_news ): 
				$authors = theme_get_inline_authors( get_the_ID(), true );
			?>
				<?php if ( $authors ): ?>
					<div class="card__author">
						<?php echo $authors ?>
					</div>
				<?php endif ?>
			<?php endif ?>
			<div class="card__content">
				<?php the_excerpt() ?>
			</div>
			<?php if ( $register_url ): ?>
				<div class="card__buttons">
					<a href="<?php echo $register_url ?>" class="button button_miw-150 button_w-a button_white card__button">
						<?php _e( 'Записаться' ) ?>
					</a>
				</div>
			<?php endif ?>
		</div>
	</div>
</li>