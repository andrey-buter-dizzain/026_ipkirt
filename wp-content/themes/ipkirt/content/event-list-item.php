<?php if ( ! defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

$src = get_theme_image_src( get_post_thumbnail_id(), 'middle' );

$question_id = get_theme_option( 'form_question_page_id' );

$register_url = get_event_register_link();

?>
<li class="inner-elements__item">
	<div class="card card_inner card_two-col">
		<a href="<?php the_permalink() ?>" style="background-image: url(<?php echo $src ?>);" data-rjs="2" class="card__img"></a>
		<div class="card__body">
			<?php get_theme_part( 'content/event', 'date' ) ?>

			<?php get_theme_part( 'content/event', 'terms' ) ?>
			
			<h3 class="card__title">
				<a href="<?php the_permalink() ?>" class="card__title-link">
					<?php the_title() ?>
				</a>
			</h3>
			<div class="card__content">
				<?php the_excerpt() ?>
			</div>
			<?php edit_post_link( __( 'Изменить', 'ipkirt' ), '<span class="edit-link">', '</span>' ); ?>
			<div class="card__buttons">
				<?php if ( $register_url ): ?>
					<a href="<?php echo $register_url ?>" class="button button_miw-150 button_w-a button_white card__button">
						<?php _e( 'Записаться' ) ?>
					</a>
				<?php endif ?>

				<?php if ( $question_id AND in_array( get_post_type(), array( 'event_group', 'event_training' ) ) ): ?>
					<a href="<?php echo get_permalink( $question_id ) ?>" class="button button_miw-150 button_w-a button_white card__button">
						<?php _e( 'Задать вопрос' ) ?>
					</a>
				<?php endif ?>
			</div>
		</div>
	</div>
</li>