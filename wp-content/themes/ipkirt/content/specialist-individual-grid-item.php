<?php if ( ! defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

$src = get_theme_image_src( get_post_thumbnail_id(), 'specialist-individual-grid' );

$position = get_the_post_meta( 'specialist_position' );

?>
<li class="grid__item grid__item_1of4 grid__item_card grid__item_narrow therapists__list-item">
	<div class="card card_simple">
		<div class="card__img-wrap">
			<a href="<?php the_permalink() ?>" style="background-image: url(<?php echo $src ?>);" data-rjs="2" class="card__img"></a>
		</div>
		<div class="card__body">
			<h4 class="card__title">
				<a href="<?php the_permalink() ?>" class="card__title-link">
					<?php the_title() ?>
				</a>
			</h4>
			<?php if ( $position ): ?>
				<div class="card__position">
					<?php echo $position ?>
				</div>
			<?php endif ?>
			<?php edit_post_link( __( 'Изменить', 'ipkirt' ), '<span class="edit-link">', '</span>' ); ?>
		</div>
	</div>
</li>