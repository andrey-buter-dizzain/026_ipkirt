<?php if ( ! defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

$discount = get_the_post_meta( 'event_discount_text' );

?>
<div class="main__inner-col main__inner-col_content">
	<section class="program-content">
		<?php the_post_thumbnail( 'full', array( 'class' => 'program-content__image program-content__image_desktop' ) ) ?>
		<?php the_post_thumbnail( 'event-large', array( 'class' => 'program-content__image program-content__image_mobile' ) ) ?>
		<div class="program-content__add-info">
			<?php get_theme_part( 'content/single-event', 'program-info' ) ?>
		</div>
		<div class="entry-content program-content__entry">
			<?php the_content() ?>

			<?php // see content/single-event/program-info.php ?>
			<?php if ( $discount ): ?>
				<div id="event-discount-data-<?php the_ID() ?>">
					<?php echo wpautop( $discount ) ?>
				</div>
			<?php endif ?>
		</div>
	</section>
</div>