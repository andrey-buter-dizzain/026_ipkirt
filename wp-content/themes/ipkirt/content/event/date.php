<?php if ( ! defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

if ( ! get_the_post_meta( 'event_show_date' ) )
	return;

$unix = get_unix_event_date();

?>
<div class="card__date">
	<div class="card-date">
		<div class="card-date__month">
			<?php echo date_i18n( 'F', $unix ) ?>
		</div>
		<div class="card-date__day">
			<?php echo date( 'j', $unix ) ?>
		</div>
		<div class="card-date__year">
			<?php echo date( 'Y', $unix ) ?>
		</div>
		<div class="card-date__time">
			<?php echo theme_get_weekday_i18l( date( 'N', $unix ) ) ?>, <?php echo date( 'H:i', $unix ) ?>
		</div>
	</div>
</div>