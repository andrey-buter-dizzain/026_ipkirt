<?php if ( ! defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' );

$desc = get_the_post_meta( 'event_front_text' );

?>
<div class="card__content">
	<?php if ( $desc ): ?>
		<?php echo wpautop( $desc . ' <a href="'. get_permalink() .'" class="card__content-link">'. __( 'Узнать больше &rsaquo;' ) .'</a>' ) ?>
	<?php else: ?>
		<?php the_excerpt() ?>
	<?php endif ?>
</div>