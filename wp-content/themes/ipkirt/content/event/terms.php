<?php if ( ! defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' );

$post_type = get_post_type();

$terms = wp_get_post_terms( get_the_ID(), "{$post_type}_cat", array(
	'parent' => 0
) );

$page_id = get_theme_option( "{$post_type}_page_id" );

if ( empty( $page_id ) AND ( empty( $terms ) OR is_wp_error( $terms ) ) )
	return;

?>
<?php if ( $terms AND ! is_wp_error( $terms ) ): ?>
	<h6 class="card__category">
		<?php foreach ( $terms as $term ): ?>
			<a href="<?php echo get_term_link( $term ) ?>" class="card__category-link">
				<?php echo $term->name ?>
			</a>
		<?php endforeach ?>
	</h6>
<?php else: ?>
	<h6 class="card__category">
		<a href="<?php echo get_permalink( $page_id ) ?>" class="card__category-link">
			<?php echo get_the_title( $page_id ) ?>
		</a>
	</h6>
<?php endif ?>