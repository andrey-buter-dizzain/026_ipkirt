<?php if ( ! defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

$src = get_theme_image_src( get_post_thumbnail_id(), 'medium' );

wp_cache_set( 'theme_gallery_item', 1 );

global $post;

$shortcode = theme_get_shortcode_from_content( 'gallery', $post->post_content );

?>
<div class="gallery__item">
	<div class="gallery-line">
		<h3 class="gallery-line__title">
			<a href="<?php the_permalink() ?>">
				<?php the_title() ?>
			</a>
		</h3>
		<?php if ( $shortcode ): ?>
			<?php echo do_shortcode( $shortcode ) ?>
		<?php endif ?>
	</div>
</div>
<?php wp_cache_delete( 'theme_gallery_item' );