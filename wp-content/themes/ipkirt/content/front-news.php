<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

$src = get_theme_image_src( get_post_thumbnail_id(), 'thumbnail' );

$authors = theme_get_inline_authors( get_the_ID(), true );

?>
<li id="post-<?php the_ID(); ?>" class="grid__item grid__item_1of4 news__list-item grid__item_card">
	<div class="card card_simple-small">
		<a href="<?php the_permalink() ?>" style="background-image: url(<?php echo $src ?>);" data-rjs="2" class="card__img"></a>
		<div class="card__body">
			<div class="card__date">
				<?php the_time( 'j F Y' ) ?>
			</div>
			<h4 class="card__title">
				<a href="<?php the_permalink() ?>" class="card__title-link">
					<?php the_title(); ?>
				</a>
			</h4>
			<?php if ( $authors ): ?>
				<div class="card__author">
					<?php echo $authors ?>
				</div>
			<?php endif ?>
			<?php edit_post_link( __( 'Изменить', 'ipkirt' ), '<span class="edit-link">', '</span>' ); ?>
		</div>
	</div>
</li>