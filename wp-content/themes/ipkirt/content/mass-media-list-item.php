<?php if ( ! defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

$src = get_theme_image_src( get_post_thumbnail_id(), 'middle' );

$class = has_post_thumbnail() ? '' : 'card_no-img';

?>
<li class="msmd__item">
	<div class="card card_msmd card_two-col <?php echo $class ?>">
		<?php if ( empty( $class ) ): ?>
			<a href="<?php the_permalink() ?>" style="background-image: url(<?php echo $src ?>);" data-rjs="2" class="card__img"></a>
		<?php endif ?>

		<div class="card__body">
			<?php get_theme_part( 'content/mass-media', 'date-short' ) ?>

			<h3 class="card__title">
				<a href="<?php the_permalink() ?>" class="card__title-link">
					<?php the_title() ?>
				</a>
			</h3>

			<div class="card__content">
				<?php the_excerpt() ?>
			</div>
			<?php edit_post_link( __( 'Изменить', 'ipkirt' ), '<span class="edit-link">', '</span>' ); ?>
		</div>
	</div>
</li>