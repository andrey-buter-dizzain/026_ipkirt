<?php if ( ! defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

$src = get_theme_image_src( get_post_thumbnail_id(), 'specialist-grid' );

$position = get_the_post_meta( 'specialist_position' );

$consult_id = get_theme_option( 'form_consulting_page_id' );

?>
<li class="grid__item grid__item_1of3 grid__item_card specialists-list__sub-list-item">
	<div class="card card_big-button card_simple">
		<a href="<?php the_permalink() ?>" style="background-image: url(<?php echo $src ?>);" data-rjs="2" class="card__img"></a>
		<div class="card__body">
			<h4 class="card__title">
				<a href="<?php the_permalink() ?>" class="card__title-link">
					<?php the_title() ?>
				</a>
			</h4>
			<?php if ( $position ): ?>
				<div class="card__position">
					<?php echo $position ?>
				</div>
			<?php endif ?>
			<?php if ( has_excerpt() ): ?>
				<div class="card__content">
					<?php the_excerpt() ?>
				</div>
			<?php endif; ?>
			<?php edit_post_link( __( 'Изменить', 'ipkirt' ), '<span class="edit-link">', '</span>' ); ?>
		</div>
		<div class="card__buttons">
			<a href="<?php echo add_query_arg( 'post', get_the_ID(), get_permalink( $consult_id ) ) ?>" class="button button_white card__button">
				<?php _e( 'Записаться' ) ?>
			</a>
		</div>
	</div>
</li>