<?php if ( ! defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

/**
 * @used by 
 * - page-templates/reviews.php
 * - single-specialist.php
 */

if ( ! have_comments() )
	return;

?>
<section class="specialist-reviews">
	<h2 class="specialist-reviews__title">
		<?php _e( 'Отзывы' ) ?>
	</h2>
	<div class="reviews specialist-reviews__entry">
		<?php get_theme_part( 'comments' ); ?>
	</div>
</section>