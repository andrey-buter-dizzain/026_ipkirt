<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

$ask_qst_id = get_theme_option( 'form_question_page_id' );
$opinion_id = get_theme_option( 'opinion_page_id' );

$phones  = get_theme_option( 'phones' );
$email   = get_theme_option( 'email' );
$address = get_theme_option( 'address' );

$phones = array_filter( explode( "\n", $phones ) );

$socials = get_theme_option( 'social-links' );

$menu_1 = get_theme_option( 'footer_menu_1' );
$menu_2 = get_theme_option( 'footer_menu_2' );

$contancts_page = get_theme_option( 'contacts_page_id' );

?>
				</div>
			</div>
		</main>
	</div>
	<div class="footer-wrap">
		<div class="footer-cell">
			<!--footer-->
			<footer class="container footer">
				<div class="footer__inner">
					<div class="footer__body">
						<div class="footer__body-cell footer__body-cell_menu">
							<!--footer-menu-->
							<div class="footer-menu footer-menu_media">
								<div class="footer-menu__title">
									<?php _e( 'Медиатека' ) ?>
								</div>
								<?php for ( $i = 1; $i <= 2; $i++ ): 
									$menu = get_theme_option( "footer_menu_{$i}" );

									if ( ! $menu )
										continue;
								?>
									<?php wp_nav_menu( array( 
										'menu'       => $menu,
										'menu_class' => 'footer-menu__sub-list',
										'container'  => false,
										'depth'      => 1
									) ); ?>
								<?php endfor ?>
							</div>
							<?php if ( $socials ): ?>
								<div class="footer-menu footer-menu_social">
									<div class="footer-menu__title">
										<?php _e( 'Мы в сетях' ) ?>
									</div>
									<ul class="footer-menu__list">
										<?php foreach ( $socials as $item ): ?>
											<li class="footer-menu__item">
												<a href="<?php echo $item['url'] ?>" target="_blank" class="footer-menu__link">
													<?php echo $item['title'] ?>
												</a>
											</li>
										<?php endforeach ?>
									</ul>
								</div>
							<?php endif ?>
							<!--footer-menu END-->
						</div>
						<div class="footer__body-cell footer__body-cell_contacts">
							<!--footer-contacts-->
							<div class="footer-contacts">
								<div class="footer-contacts__title">
									<?php _e( 'Контакты' ) ?>
								</div>
								<ul class="footer-contacts__list">
									<?php if ( $phones ): ?>
										<li class="footer-contacts__item">
											<?php _e( 'Телефон:' ) ?> 
											<?php foreach ( $phones as $key => $phone ): ?>
												<?php if ( 0 < $key ): ?>
													,
												<?php endif ?>
												<a href="tel:<?php echo $phone ?>" class="footer-contacts__link">
													<?php echo $phone ?>
												</a>
											<?php endforeach ?>
										</li>
									<?php endif ?>
									<?php if ( $email ): ?>
										<li class="footer-contacts__item">
											<?php _e( 'Email:' ) ?> 
											<a href="mailto:<?php echo $email ?>" class="footer-contacts__link">
												<?php echo $email ?>
											</a>
										</li>
									<?php endif ?>
									<?php if ( $address ): ?>
										<li class="footer-contacts__item">
											<?php _e( 'Адрес:' ) ?> 
											<a href="<?php echo get_permalink( $contancts_page ) ?>" class="footer-contacts__link link_dotted">
												<?php echo $address ?>
											</a>
										</li>
									<?php endif ?>
								</ul>
							</div>
							<!--footer-contacts END-->
						</div>
						<div class="footer__body-cell footer__body-cell_actions">
							<?php if ( $ask_qst_id ): ?>
								<a href="<?php echo get_permalink( $ask_qst_id ) ?>" class="button button_white footer__button">
									<?php _e( 'Задать вопрос' ) ?>
								</a>
							<?php endif ?>
							<?php if ( $opinion_id ): ?>
								<a href="<?php echo get_permalink( $opinion_id ) ?>" class="button button_white footer__button">
									<?php _e( 'Написать отзыв' ) ?>
								</a>
							<?php endif ?>
						</div>
					</div>
					<div class="footer__bottom">
						<div class="footer__copyright">
							<?php wp_theme_copyright( 2017 ) ?> <?php theme_option( 'copyright' ) ?>
						</div>
						<div class="footer__author">
							<a target="_blank" href="www.behance.net/alika5527"><?php _e( 'Дизайн' ) ?> - Alika</a>
						</div>
					</div>
				</div>
			</footer>
			<!--footer END-->
		</div>
		<!--main END-->
	</div>
</div>

<?php echo do_shortcode( '[my_subscribers template="subscribe-front.tmpl"]' ) ?>

<?php wp_footer(); ?>

</body>
</html>