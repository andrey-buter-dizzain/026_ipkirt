<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

/**
 * Dizzain theme setup
 *
 * @package WordPress
 * @subpackage Dizzain
 */

if ( !defined( 'THEME_URI' ) )
	define( 'THEME_URI', get_stylesheet_directory_uri() .'/inc/assets' );

if ( !defined( 'THEME_INC_ABS' ) )
	define( 'THEME_INC_ABS', get_template_directory() .'/inc' );

if ( !defined( 'THEME_CACHE_GROUP' ) )
	define( 'THEME_CACHE_GROUP', 'ipkirt' );

if ( !defined( 'THEME_LANG_PREFIX' ) AND defined( 'ICL_LANGUAGE_CODE' ) )
	define( 'THEME_LANG_PREFIX', '_'. ICL_LANGUAGE_CODE );

/**
 * Set up theme defaults and registers support for various WordPress features.
 *
 */
add_action( 'after_setup_theme', 'theme_setup', 1 );
function theme_setup() {

	// Load up our additional theme scripts & styles
	require( THEME_INC_ABS .'/shared/lib/load-section.abstract.php' );
	require( THEME_INC_ABS .'/modules/theme-singleton/theme-singleton.php' );

	// Shared theme utilities (options|meta-boxes|utils...)
	require( THEME_INC_ABS .'/shared.php' );

	// General Dizzain Style Settings and functions ( include theme scripts and styles )
	require( THEME_INC_ABS .'/general.php' );

	require( THEME_INC_ABS .'/page.php' );
	require( THEME_INC_ABS .'/post.php' );
	require( THEME_INC_ABS .'/news.php' );
	require( THEME_INC_ABS .'/event.php' );
	require( THEME_INC_ABS .'/literature.php' );
	require( THEME_INC_ABS .'/mass-media.php' );
	require( THEME_INC_ABS .'/specialist.php' );
	require( THEME_INC_ABS .'/gallery.php' );

	require( THEME_INC_ABS .'/modules.php' );

	/**
	 * Setup vital theme settings
	 */

	// Add default posts and comments RSS feed links to <head>.
	// add_theme_support( 'automatic-feed-links' );
	add_post_type_support( 'page', 'excerpt' );

	remove_action( 'wp_head', 'feed_links_extra', 3 );
	remove_action( 'wp_head', 'feed_links', 2 );
	remove_action( 'wp_head', 'wp_generator' );
	remove_action( 'wp_head', 'wlwmanifest_link' );
	remove_action( 'wp_head', 'rsd_link' );
	remove_action( 'wp_head', 'wp_shortlink_wp_head' );

	// This theme uses wp_nav_menus() in one location.
	register_nav_menus( array(
		'primary'   => __( 'Главная навигация', 'ipkirt-admin' ),
		'mediateka' => __( 'Медиотека', 'ipkirt-admin' ),
	));

	// This theme uses a custom image size for featured images, displayed on "standard" posts.
	add_theme_support( 'post-thumbnails' );

	add_image_size( 'event-large',  570, 299, true );
	add_image_size( 'front-slide',  670, 400, true );
	add_image_size( 'specialist-grid',  368, 338, true );
	add_image_size( 'front-event',  368, 237, true );
	add_image_size( 'specialist-team-grid',  270, 303, true );
	add_image_size( 'specialist-individual-grid',  268, 290, true );
	add_image_size( 'specialist-event',  337, 308, true );
	add_image_size( 'literatura',  270, 360, true );
	add_image_size( 'gallery-single',  270, 190, true );
	add_image_size( 'gallery-large',  510, 510, true );
	add_image_size( 'gallery-small',  250, 250, true );
}

add_action( 'do_feed',      'redirect_feed_to_home', 1 );
add_action( 'do_feed_rdf',  'redirect_feed_to_home', 1 );
add_action( 'do_feed_rss',  'redirect_feed_to_home', 1 );
add_action( 'do_feed_rss2', 'redirect_feed_to_home', 1 );
add_action( 'do_feed_atom', 'redirect_feed_to_home', 1 );
function redirect_feed_to_home() {
    wp_redirect( home_url(), 301 );
}

// add_action( 'widgets_init', 'theme_widgets_init' );
function theme_widgets_init() {

	register_sidebar( array(
		'name'          => __( 'Shop Sidebar' ),
		'id'            => 'shop-sidebar',
		'before_widget' => '<aside id="%1$s" class="widget %2$s"><div class="widget-inn">',
		'after_widget'  => '</div></aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => __( 'Shop Sidebar 2' ),
		'id'            => 'shop-sidebar-2',
		'before_widget' => '<aside id="%1$s" class="widget %2$s"><div class="widget-inn">',
		'after_widget'  => '</div></aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}