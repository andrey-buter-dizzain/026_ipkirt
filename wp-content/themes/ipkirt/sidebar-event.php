<?php if ( ! defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

?>
<div class="main__inner-col main__inner-col_sidebar">
	<aside class="program-sidebar">
		<div class="program-sidebar__widget program-sidebar__widget_info">
			<?php get_theme_part( 'content/single-event', 'program-info' ) ?>
		</div>
		<?php get_theme_part( 'content/single-event', 'coaches' ) ?>
		<?php get_theme_part( 'content/single-event', 'articles' ) ?>
	</aside>
</div>