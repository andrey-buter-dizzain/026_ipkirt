<?php
/**
 * Register profile post type
 */

register_post_type( 'news',
	array(
		'labels'        => array(
			'name'               => __( 'Новости', 'ipkirt-admin' ),
			'singular_name'      => __( 'Новость', 'ipkirt-admin' ),
			'add_new'            => __( 'Добавить новость', 'ipkirt-admin' ),
			'add_new_item'       => __( 'Добавить новость', 'ipkirt-admin' ),
			'edit'               => __( 'Изменить', 'ipkirt-admin' ),
			'edit_item'          => __( 'Изменить новость', 'ipkirt-admin' ),
			'new_item'           => __( 'Добавить новость', 'ipkirt-admin' ),
			'view'               => __( 'Смотреть новости', 'ipkirt-admin' ),
			'view_item'          => __( 'Смотреть новость', 'ipkirt-admin' ),
			'search_items'       => __( 'Поиск новостей', 'ipkirt-admin' ),
			'not_found'          => __( 'Not found', 'ipkirt-admin' ),
			'not_found_in_trash' => __( 'Not found', 'ipkirt-admin' ),
		),
		'public'        => true,
		'menu_position' => 20,
		'hierarchical'  => false,
		'has_archive'   => false,
		'query_var'     => true,
		'supports'      => array( 
			'title', 
			'editor', 
			'thumbnail', 
		),
		'rewrite'       => array(
			'with_front' => true
		),
		'can_export'    => true,
		// 'publicly_queryable'  => false, // ???????????
		// 'exclude_from_search' => true
	)
);