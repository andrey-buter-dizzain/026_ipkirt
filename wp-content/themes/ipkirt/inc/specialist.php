<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 
/**
 * Page module
 */

class Inc_Specialist extends Inc_Load_Section
{
	public static $section = 'specialist';

	protected static $include_path;

	protected static $uri_path;

	public static $partials = array(
		'model',
		'meta',
		'custom',
		'related-posts'
	);
}
Inc_Specialist::load();