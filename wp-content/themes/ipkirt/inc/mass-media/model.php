<?php
/**
 * Register profile post type
 */

register_post_type( 'mass-media',
	array(
		'labels'        => array(
			'name'               => __( 'СМИ о нас', 'ipkirt-admin' ),
			'singular_name'      => __( 'СМИ', 'ipkirt-admin' ),
			'add_new'            => __( 'Добавить статью', 'ipkirt-admin' ),
			'add_new_item'       => __( 'Добавить статью', 'ipkirt-admin' ),
			'edit'               => __( 'Изменить', 'ipkirt-admin' ),
			'edit_item'          => __( 'Изменить статью', 'ipkirt-admin' ),
			'new_item'           => __( 'Добавить статью', 'ipkirt-admin' ),
			'view'               => __( 'Смотреть статьи', 'ipkirt-admin' ),
			'view_item'          => __( 'Смотреть статью', 'ipkirt-admin' ),
			'search_items'       => __( 'Поиск статей', 'ipkirt-admin' ),
			'not_found'          => __( 'Ничего не найдено.', 'ipkirt-admin' ),
			'not_found_in_trash' => __( 'Ничего не найдено.', 'ipkirt-admin' ),
		),
		'public'        => true,
		'menu_position' => 20,
		'hierarchical'  => false,
		'has_archive'   => false,
		'query_var'     => true,
		'supports'      => array( 
			'title', 
			'editor', 
			'thumbnail', 
			'author',
		),
		'rewrite'       => array(
			'with_front' => true
		),
		'can_export'    => true,
		// 'publicly_queryable'  => false, // ???????????
		// 'exclude_from_search' => true
	)
);