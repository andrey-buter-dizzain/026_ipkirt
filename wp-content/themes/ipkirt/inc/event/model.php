<?php if ( ! defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 
/**
 * Register profile post type
 */

$post_types = array(
	'event_group'    => array(
		'name'     => __( 'Клиентские группы', 'ipkirt-admin' ),
		'singular' => __( 'Клиентская группа', 'ipkirt-admin' ),
		'slug'     => 'groups',
	),
	'event_single'   => array(
		'name'     => __( 'Единичные события', 'ipkirt-admin' ),
		'singular' => __( 'Единичное события', 'ipkirt-admin' ),
		'slug'     => 'events',
	),
	'event_study'    => array(
		'name'     => __( 'Обучение', 'ipkirt-admin' ),
		'singular' => __( 'Событие обучения', 'ipkirt-admin' ),
		'slug'     => 'study',
	),
	'event_training' => array(
		'name'     => __( 'Тренинги', 'ipkirt-admin' ),
		'singular' => __( 'Тренинг', 'ipkirt-admin' ),
		'slug'     => 'trainings',
	),
);

foreach ( $post_types as $post_type => $item ) {
	register_post_type( $post_type,
		array(
			'labels'        => array(
				'name'               => $item['name'],
				'singular_name'      => $item['singular'],
				'add_new'            => __( 'Добавить событие', 'ipkirt-admin' ),
				'add_new_item'       => __( 'Добавить событие', 'ipkirt-admin' ),
				'edit'               => __( 'Изменить', 'ipkirt-admin' ),
				'edit_item'          => __( 'Изменить событие', 'ipkirt-admin' ),
				'new_item'           => __( 'Добавить событие', 'ipkirt-admin' ),
				'view'               => __( 'Смотреть события', 'ipkirt-admin' ),
				'view_item'          => __( 'Смотреть событие', 'ipkirt-admin' ),
				'search_items'       => __( 'Поиск новостей', 'ipkirt-admin' ),
				'not_found'          => __( 'Ничего не найдено.', 'ipkirt-admin' ),
				'not_found_in_trash' => __( 'Ничего не найдено.', 'ipkirt-admin' ),
			),
			'public'        => true,
			'menu_position' => 20,
			'hierarchical'  => false,
			'has_archive'   => false,
			'query_var'     => true,
			'supports'      => array( 
				'title', 
				'editor', 
				'thumbnail', 
				'excerpt',
			),
			'rewrite'       => array(
				'slug' => $item['slug']
			),
			'can_export'    => true,
			// 'publicly_queryable'  => false, // ???????????
			// 'exclude_from_search' => true
		)
	);
}

register_taxonomy(
	'event_study_cat',
	array( 'event_study' ),
	array(
		'hierarchical' => true,
		'labels'       => array(
			'name'              => __( 'Направления обучения', 'ipkirt-admin' ),
			'singular_name'     => __( 'Направление', 'ipkirt-admin' ),
			'search_items'      => __( 'Поиск направлений', 'ipkirt-admin' ),
			'all_items'         => __( 'Все направления', 'ipkirt-admin' ),
			'parent_item'       => __( 'Родительское направление', 'ipkirt-admin' ),
			'parent_item_colon' => __( 'Родительское направление:', 'ipkirt-admin' ),
			'edit_item'         => __( 'Изменить направление', 'ipkirt-admin' ), 
			'update_item'       => __( 'Обновить направление', 'ipkirt-admin' ),
			'add_new_item'      => __( 'Добавить новое направление', 'ipkirt-admin' ),
			'new_item_name'     => __( 'Название направления', 'ipkirt-admin' ),
			'menu_name'         => __( 'Направления обучения', 'ipkirt-admin' ),
		),
		'show_ui'     => true,
		'show_admin_column' => true,
		'query_var'   => true,
		'rewrite'     => array(
			'slug' => 'study-cources'
		),
		'update_count_callback' => '_update_generic_term_count',
	)
);
