<?php if ( ! defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

function get_event_register_link() {
	$post_id     = get_the_ID();
	$post_type   = get_post_type();
	$register_id = get_theme_option( "form_{$post_type}_page_id" );

	if ( ! $register_id )
		return;

	$args = array(
		'event' => get_the_ID()
	);

	$terms = wp_get_post_terms( get_the_ID(), "{$post_type}_cat", array(
		'parent' => 0
	) );

	if ( ! is_wp_error( $terms ) AND $terms ) {
		$args['type'] = $terms[0]->term_id;
	}

	return add_query_arg( $args, get_permalink( $register_id ) );
}

function get_unix_event_date( $post_id = null ) {
	if ( ! $post_id )
		$post_id = get_the_ID();

	$dates     = get_post_meta( $post_id, 'event_dates', true );
	$post_date = get_post_time( 'U', false, $post_id );
	$current   = current_time( 'timestamp' );

	if ( ! $dates )
		return $post_date;

	$dates = array_filter( array_column( $dates, 'date' ) );
	array_walk( $dates, function( &$date, $index, $frames ) {
		$date = strtotime( $date );

		if ( $frames['current'] > $date )
			$date = false;

		if ( $date > $frames['post_date'] )
			$date = false;
	}, compact( 'current', 'post_date' ) );

	$dates = array_filter( $dates );

	sort( $dates, SORT_NUMERIC );

	if ( $dates )
		return $dates[0];

	return $post_date;

}

if ( ! is_admin() ) {
	add_filter( 'get_term', 'event_fix_term_name', 10, 2 );
	add_filter( 'get_terms', 'event_fix_terms_name', 10, 2 );
}
function event_fix_term_name( $term, $taxonomy ) {
	if ( 'event_study_cat' != $taxonomy )
		return $term;

	return fix_event_study_cat_name( $term );
}

function fix_event_study_cat_name( $term ) {
	if ( ! is_object( $term ) )
		return $term;
	
	$term->name = preg_replace( '/#[0-9]+$/', '', $term->name );
	return $term;
}

function event_fix_terms_name( $terms, $taxonomy ) {
	if ( ! in_array( 'event_study_cat', $taxonomy ) )
		return $terms;

	return array_map( 'fix_event_study_cat_name', $terms );
}


function events_reorder_query( $query ) {
	$posts = $query->posts;
	$indexed = array();
	$ids = array();

	foreach ( $posts as $key => $post ) {
		$ids[ $post->ID ] = get_unix_event_date( $post->ID );
		$indexed[ $post->ID ] = $post;
	}

	$posts = array();

	$dates = $ids;

	sort( $dates, SORT_NUMERIC );
	
	foreach ( $dates as $date ) {
		$_ids = array_keys( $ids, $date );

		foreach ( $_ids as $id ) {
			unset( $ids[ $id ] );

			$posts[] = $indexed[ $id ];
		}
	}

	$query->posts = $posts;

	return $query;
}