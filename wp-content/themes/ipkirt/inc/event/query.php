<?php if ( ! defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

/**
 * 
 */
class Theme_Event_Handler
{
	protected $post_types = array(
		'event_group',
		'event_single',
		'event_study',
		'event_training',
	);

	function __construct()
	{
		if ( ! is_admin() )
			add_action( 'pre_get_posts', array( $this, 'future_events_query' ) );

		add_action( 'template_redirect', array( $this, 'event_template_redirect' ) );
	}

	function future_events_query( $query ) {
		if ( $this->is_event_query( $query ) ) {
			$timestamp = current_time( 'timestamp' );

			$query->set( 'post_status', array( 'future', 'publish' ) );
			$query->set( 'order', 'ASC' );
			$query->set( 'date_query', array( 
				array(
					'after' => array( 
						'year'  => absint( date( 'Y', $timestamp ) ), 
						'month' => absint( date( 'n', $timestamp ) ), 
						'day'   => absint( date( 'j', $timestamp ) - 1 )
					)
				)
			) );
		}
	}

	protected function is_event_query( $query )
	{
		// show singular events  every time
		if ( is_user_logged_in() AND $query->is_main_query() AND $query->is_singular() AND in_array( $query->get( 'post_type' ), $this->post_types ) )
			return false;

		// default events list
		if ( in_array( $query->get( 'post_type' ), $this->post_types ) )
			return true;

		// front page multiple events blocks
		if ( 'event' == $query->get( 'post_super_type' ) )
			return true;

		// taxonomy page
		if ( $query->is_main_query() AND $query->get( 'event_study_cat' ) )
			return true;

		if ( $query->is_main_query() AND $query->is_search() )
			return true;

		return false;
	}

	function event_template_redirect() {
		if ( ! is_singular( $this->post_types ) )
			return;

		get_theme_part( 'single-event' );
		die;
	}
}
new Theme_Event_Handler;
