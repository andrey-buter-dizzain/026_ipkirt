<?php if ( ! defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

/**
 * Handle every member aspect
 */

function theme_event_reserver_titles() {
	return array(
		'duration'     => array(
			'title' => __( 'Длительность', 'ipkirt-admin' ),
			'icon'  => 'sand-clocks',
		),
		'date'         => array(
			'title' => __( 'Дата проведения', 'ipkirt-admin' ),
			'icon'  => 'calendar',
		),
		'time'         => array(
			'title' => __( 'Время проведения', 'ipkirt-admin' ),
			'icon'  => 'clocks',
		),
		'places_count' => array(
			'title' => __( 'Осталось', 'ipkirt-admin' ),
			'icon'  => 'avatar',
		),
		'price'        => array(
			'title' => __( 'Стоимость', 'ipkirt-admin' ),
			'icon'  => 'ruble',
		),
	);
}

add_filter( 'populate_theme_meta_boxes', 'populate_event_meta_boxes' );
function populate_event_meta_boxes( $meta_boxes = array() ) {
	$post_types = array(
		'event_group',
		'event_single',
		'event_study',
		'event_training',
	);

	$prefix = "event_";

	$reserved_titles = theme_event_reserver_titles();

	foreach ( $reserved_titles as $key => $data ) {
		$reserved_titles[] = array(
			'value' => $key,
			'label' => $data['title']
		);

		unset( $reserved_titles[ $key ] );
	}

	array_unshift( $reserved_titles, array(
		'value' => '',
		'label' => __( '--', 'theme-text-domain' ),
	) );

	$meta_boxes[] = array(
		'id'       => "{$prefix}meta",
		'title'    => __( 'Дополнительная информация', 'ipkirt-admin' ),
		'pages'    => $post_types,
		'context'  => 'normal',
		'priority' => 'default',
		'fields'   => array(
			array(
				'label' => __( 'Даты', 'theme-text-domain' ),
				'id'    => "{$prefix}tab_date",
				'type'  => 'tab'
			),
			array(
				'label' => __( 'Отображать дату', 'ipkirt-admin' ),
				'id'    => "{$prefix}show_date",
				'type'  => 'on-off-simple',
				'settings' => array( 'std' => 1 ),
			),
			// see below theme_event_date_field_desc() to fix description
			array(
				'label'    => __( 'Промежуточные даты', 'ipkirt-admin' ),
				'id'       => "{$prefix}dates",
				'type'     => 'list-item',
				'class'    => 'custom-type-list-item',
				'settings' => array(
					array(
						'id'    => 'date',
						'type'  => 'date-time-picker',
						'label' => '',
						'class' => 'js-title-selector',
					)
				)
			),
			array(
				'label' => __( 'Параметры', 'theme-text-domain' ),
				'id'    => "{$prefix}tab_atts",
				'type'  => 'tab'
			),
			array(
				'label'    => __( 'Атрибуты события', 'ipkirt-admin' ),
				'id'       => "{$prefix}atts",
				'type'     => 'list-item',
				'class'    => 'custom-type-list-item',
				'settings' => array(
					array(
						'label'   => __( 'Или выбрерите зарезервированное название параметра', 'ipkirt-admin' ),
						'id'      => 'title_reserved',
						'type'    => 'select',
						'class'   => 'js-title-selector',
						'choices' => $reserved_titles
 					),
 					array(
						'label' => __( 'Свой параметр', 'ipkirt-admin' ),
						'id'    => 'title_custom',
						'type'  => 'text',
					),
					array(
						'label' => __( 'Значение', 'ipkirt-admin' ),
						'id'    => 'value',
						'type'  => 'text',
					),
					// array(
					// 	'label' => __( 'Иконка', 'ipkirt-admin' ),
					// 	'id'    => 'icon',
					// 	'type'  => 'text',
					// ),
				),
			),
			array(
				'label' => __( 'Информация про скидки', 'ipkirt-admin' ),
				'id'    => "{$prefix}discount_text",
				'type'  => 'textarea',
				'desc'  => __( 'Всегда отображается в низу контента', 'ipkirt-admin' ),
			),
			array(
				'label' => __( 'Преподаватели', 'ipkirt-admin' ),
				'id'    => "{$prefix}tab_coach",
				'type'  => 'tab'
			),
			array(
				'label' => __( 'Заглавие блока преподавателей.', 'ipkirt-admin' ),
				'id'    => "{$prefix}coach_label",
				'type'  => 'text',
				'desc'  => __( 'По умолчанию: Преподаватель' )
			),
			array(
				'label'    => __( 'Преподаватель/Ведущий', 'ipkirt-admin' ),
				'id'       => "post_authors",
				'type'     => 'list-item',
				'class'    => 'custom-type-list-item',
				'settings' => array(
					array(
						'id'        => 'specialist_id',
						'type'      => 'custom-post-type-select',
						'post_type' => 'specialist',
						'label'     => '',
						'class'     => 'js-title-selector',
					)
				)
			),
			array(
				'label' => __( 'Статьи по теме', 'ipkirt-admin' ),
				'id'    => "{$prefix}tab_articles",
				'type'  => 'tab'
			),
			array(
				'label'    => __( 'Выберите статьи по теме', 'ipkirt-admin' ),
				'id'       => "{$prefix}articles",
				'class'    => 'custom-type-list-item',
				'type'     => 'list-item',
				'settings' => array(
					array(
						'id'        => 'post_id',
						'type'      => 'custom-post-type-select',
						'post_type' => 'post',
						'class'     => 'js-title-selector',
					),
				),
			),
			array(
				'label' => __( 'Для главной страницы', 'theme-text-domain' ),
				'id'    => "{$prefix}tab_front",
				'type'  => 'tab'
			),
			array(
				'label' => __( 'Короткий текст под событием', 'ipkirt-admin' ),
				'id'    => "{$prefix}front_text",
				'type'  => 'textarea-simple',
				'rows'  => 2
			),
		),
	);

	return $meta_boxes;
}

add_filter( 'ot_list_item_title_label', 'theme_event_ot_list_item_title_label', 10, 2 );
function theme_event_ot_list_item_title_label( $label, $field_name ) {
	if ( 'event_atts' != $field_name )
		return $label;

	return __( 'Название параметра', 'ipkirt-admin' );
}

add_filter( 'ot_list_item_description', 'theme_event_date_field_desc', 10, 2 );
function theme_event_date_field_desc( $list_desc, $field_id ) {
	if ( 'event_dates' !== $field_id )
		return $list_desc;

	$date = get_the_time( 'Y-m-d' );

	return "Дата окончания события: <strong id=\"js-ot-post-date\">$date</strong>";
}