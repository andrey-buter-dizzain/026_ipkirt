<?php if ( ! defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

add_filter( 'post_type_link', 'theme_gallery_post_type_link', 20, 2 );
function theme_gallery_post_type_link( $post_link = '', $post = '' ) {
	if ( 'gallery' != $post->post_type )
		return $post_link;

	$year = get_the_time( 'Y', $post );

	return str_replace( '%year%', $year, $post_link );
}

add_filter( 'init', 'theme_gallery_rewrite_rules' );
function theme_gallery_rewrite_rules() {
	global $wp_rewrite;

	$page_id = get_theme_option( 'gallery_page_id' );

	if ( ! $page_id )
		return;

	$link = get_permalink( $page_id );

	$slug = str_replace( home_url( '/' ), '', $link );

	add_rewrite_tag( "%year%", '([0-9]{4})' );

	add_rewrite_rule( "{$slug}archive-([0-9]{4})/?$", "{$wp_rewrite->index}?post_type=gallery&year=\$matches[1]", 'top' );
	add_rewrite_rule( "{$slug}archive-([0-9]{4})/page/([0-9]{1,})/?$", "{$wp_rewrite->index}?post_type=gallery&year=\$matches[1]&paged=\$matches[2]", 'top' );
}