<?php if ( ! defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 
/**
 * Register profile post type
 */

register_post_type( 'gallery',
	array(
		'labels'        => array(
			'name'               => __( 'Галереи', 'ipkirt-admin' ),
			'singular_name'      => __( 'Галерея', 'ipkirt-admin' ),
			'add_new'            => __( 'Добавить галерею', 'ipkirt-admin' ),
			'add_new_item'       => __( 'Добавить галерею', 'ipkirt-admin' ),
			'edit'               => __( 'Изменить', 'ipkirt-admin' ),
			'edit_item'          => __( 'Изменить галерею', 'ipkirt-admin' ),
			'new_item'           => __( 'Добавить галерею', 'ipkirt-admin' ),
			'view'               => __( 'Смотреть галереи', 'ipkirt-admin' ),
			'view_item'          => __( 'Смотреть галерею', 'ipkirt-admin' ),
			'search_items'       => __( 'Поиск по галереями', 'ipkirt-admin' ),
			'not_found'          => __( 'Ничего не найдено.', 'ipkirt-admin' ),
			'not_found_in_trash' => __( 'Ничего не найдено.', 'ipkirt-admin' ),
		),
		'public'        => true,
		'menu_position' => 20,
		'hierarchical'  => false,
		'has_archive'   => true,
		'query_var'     => true,
		'supports'      => array( 
			'title', 
			'editor', 
		),
		'rewrite'       => array(
			// 'slug' => '/galleries/%year%'
			'slug' => 'galleries'
		),
		'can_export'    => true,
		// 'publicly_queryable'  => false, // ???????????
		// 'exclude_from_search' => true
	)
);