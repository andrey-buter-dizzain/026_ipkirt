<?php if ( ! defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

add_filter( 'post_gallery', 'theme_gallery_post_type_gallery', 10, 3 );
function theme_gallery_post_type_gallery( $html, $attr, $instance ) {
	$post = get_post();

	if ( 'gallery' != get_post_type( $post ) )
		return $html;

	$html = '';

	$atts = shortcode_atts( array(
		'order'      => 'ASC',
		'orderby'    => 'menu_order ID',
		'id'         => $post ? $post->ID : 0,
		'size'       => 'full',
		'include'    => '',
	), $attr, 'gallery' );

	if ( empty( $atts['include'] ) )
		return $html;

	$args = array( 
		'include'     => $atts['include'], 
		'post_status' => 'inherit', 
		'post_type'   => 'attachment', 
		'order'       => $atts['order'], 
		'orderby'     => $atts['orderby'],
		'post_mime_type' => 'image', 
	);

	$tmpl = 'gallery-single';

	if ( wp_cache_get( 'theme_gallery_item' ) ) {
		$args['posts_per_page'] = 5;
		$tmpl = 'gallery-list-item';
	}

	$attachments = get_posts( $args );

	if ( empty( $attachments ) )
		return '';

	return get_theme_part( 'section/gallery', $tmpl, array( 
		'return' => true,
		'data'   => $attachments,
	) );
}