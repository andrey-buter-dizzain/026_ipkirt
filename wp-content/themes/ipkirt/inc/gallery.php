<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 
/**
 * Page module
 */

class Inc_Gallery extends Inc_Load_Section
{
	public static $section = 'gallery';

	protected static $include_path;

	protected static $uri_path;

	public static $partials = array(
		'model',
		'custom',
		'permalinks'
	);
}
Inc_Gallery::load();