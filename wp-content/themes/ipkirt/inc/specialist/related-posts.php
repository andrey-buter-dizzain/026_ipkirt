<?php if ( ! defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

/**
 * 
 */
class Module_Related_Posts
{
	protected $meta_key = 'post_authors';

	const TRANSIENT_KEY = 'module_related_posts';

	function __construct()
	{
		add_action( 'save_post', array( $this, 'on_save_post' ) );
	}

	function on_save_post( $post_id )
	{
		$transient = self::TRANSIENT_KEY ."_{$post_id}";

		delete_transient( $transient );
	}

	public function get_post_ids( $post_id )
	{
		global $wpdb;

		$transient = self::TRANSIENT_KEY ."_{$post_id}";

		$result = get_transient( $transient );

		if ( ! empty( $result ) )
			return $result;

		$query = "SELECT post_id FROM {$wpdb->prefix}postmeta WHERE meta_key = '{$this->meta_key}' AND meta_value LIKE '%\"{$post_id}\"%'";

		$result = $wpdb->get_col( $query );

		set_transient( $transient, $result, 0 );

		return $result;
	}
}
theme_obj()->init_object( 'related_posts', 'Module_Related_Posts' );