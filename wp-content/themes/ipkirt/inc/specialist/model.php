<?php
/**
 * Register profile post type
 */

register_post_type( 'specialist',
	array(
		'labels'        => array(
			'name'               => __( 'Специалисты', 'ipkirt-admin' ),
			'singular_name'      => __( 'Специалист', 'ipkirt-admin' ),
			'add_new'            => __( 'Добавить специалиста', 'ipkirt-admin' ),
			'add_new_item'       => __( 'Добавить специалиста', 'ipkirt-admin' ),
			'edit'               => __( 'Изменить', 'ipkirt-admin' ),
			'edit_item'          => __( 'Изменить специалиста', 'ipkirt-admin' ),
			'new_item'           => __( 'Добавить специалиста', 'ipkirt-admin' ),
			'view'               => __( 'Смотреть специалистов', 'ipkirt-admin' ),
			'view_item'          => __( 'Смотреть специалиста', 'ipkirt-admin' ),
			'search_items'       => __( 'Поиск специалистов', 'ipkirt-admin' ),
			'not_found'          => __( 'Ничего не найденою', 'ipkirt-admin' ),
			'not_found_in_trash' => __( 'Ничего не найденою', 'ipkirt-admin' ),
		),
		'public'        => true,
		'menu_position' => 20,
		'hierarchical'  => false,
		'has_archive'   => false,
		'query_var'     => true,
		'supports'      => array( 
			'title', 
			'editor', 
			'thumbnail', 
			'excerpt',
			'comments'
		),
		'rewrite'       => array(
			'with_front' => true
		),
		'can_export'    => true,
		// 'publicly_queryable'  => false, // ???????????
		// 'exclude_from_search' => true
	)
);

register_taxonomy(
	'specialist_team',
	array( 'specialist' ),
	array(
		'hierarchical' => true,
		'labels'       => array(
			'name'              => __( 'Команда', 'ipkirt-admin' ),
			'singular_name'     => __( 'Роль', 'ipkirt-admin' ),
			'search_items'      => __( 'Поиск по команде', 'ipkirt-admin' ),
			'all_items'         => __( 'Все роли', 'ipkirt-admin' ),
			'parent_item'       => __( 'Родительская роль', 'ipkirt-admin' ),
			'parent_item_colon' => __( 'Родительская роль:', 'ipkirt-admin' ),
			'edit_item'         => __( 'Изменить роль', 'ipkirt-admin' ), 
			'update_item'       => __( 'Обновить роль', 'ipkirt-admin' ),
			'add_new_item'      => __( 'Добавить новую роль', 'ipkirt-admin' ),
			'new_item_name'     => __( 'Название роли', 'ipkirt-admin' ),
			'menu_name'         => __( 'Команда', 'ipkirt-admin' ),
		),
		'show_ui'     => true,
		'show_admin_column' => true,
		'query_var'   => true,
		'rewrite'     => true,
		'public'      => false,
		'update_count_callback' => '_update_generic_term_count',
	)
);

register_taxonomy(
	'specialist_individual',
	array( 'specialist' ),
	array(
		'hierarchical' => true,
		'labels'       => array(
			'name'              => __( 'Категории индивидуальных специалистов', 'ipkirt-admin' ),
			'singular_name'     => __( 'Категория индивидуальных специалистов', 'ipkirt-admin' ),
			'search_items'      => __( 'Поиск по категориям', 'ipkirt-admin' ),
			'all_items'         => __( 'Все категории', 'ipkirt-admin' ),
			'parent_item'       => __( 'Родительская категория', 'ipkirt-admin' ),
			'parent_item_colon' => __( 'Родительская категория:', 'ipkirt-admin' ),
			'edit_item'         => __( 'Изменить категорию', 'ipkirt-admin' ), 
			'update_item'       => __( 'Обновить категорию', 'ipkirt-admin' ),
			'add_new_item'      => __( 'Добавить новую категорию', 'ipkirt-admin' ),
			'new_item_name'     => __( 'Название категории', 'ipkirt-admin' ),
			'menu_name'         => __( 'Категории индивидуальных специалистов', 'ipkirt-admin' ),
		),
		'show_ui'     => true,
		'show_admin_column' => true,
		'query_var'   => true,
		'rewrite'     => true,
		'update_count_callback' => '_update_generic_term_count',
	)
);

register_taxonomy(
	'specialist_cat',
	array( 'specialist' ),
	array(
		'hierarchical' => true,
		'labels'       => array(
			'name'              => __( 'Категории всех специалистов', 'ipkirt-admin' ),
			'singular_name'     => __( 'Категория всех специалистов', 'ipkirt-admin' ),
			'search_items'      => __( 'Поиск по категориям', 'ipkirt-admin' ),
			'all_items'         => __( 'Все категории', 'ipkirt-admin' ),
			'parent_item'       => __( 'Родительская категория', 'ipkirt-admin' ),
			'parent_item_colon' => __( 'Родительская категория:', 'ipkirt-admin' ),
			'edit_item'         => __( 'Изменить категорию', 'ipkirt-admin' ), 
			'update_item'       => __( 'Обновить категорию', 'ipkirt-admin' ),
			'add_new_item'      => __( 'Добавить новую категорию', 'ipkirt-admin' ),
			'new_item_name'     => __( 'Название категории', 'ipkirt-admin' ),
			'menu_name'         => __( 'Категории всех специалистов', 'ipkirt-admin' ),
		),
		'show_ui'     => true,
		'show_admin_column' => true,
		'query_var'   => true,
		'public'      => false,
		'update_count_callback' => '_update_generic_term_count',
	)
);