<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

/**
 * Handle every member aspect
 */

add_filter( 'populate_theme_meta_boxes', 'populate_specialist_meta_boxes' );
function populate_specialist_meta_boxes( $meta_boxes = array() ) {
	$post_type = 'specialist';

	$prefix = "{$post_type}_";

	$meta_boxes[] = array(
		'id'       => "{$prefix}meta",
		'title'    => __( 'Дополнительная информация', 'ipkirt-admin' ),
		'pages'    => array( $post_type ),
		'context'  => 'normal',
		'priority' => 'high',
		'fields'   => array(
			array(
				'label'   => __( 'Вторая часть контента', 'ipkirt-admin' ),
				'id'      => "{$prefix}content",
				'type'    => 'textarea',
			),
			array(
				'label'   => __( 'Имя и Фамилия в Родительном падеже', 'ipkirt-admin' ),
				'id'      => "{$prefix}name",
				'type'    => 'text',
			),
			array(
				'label'   => __( 'Поизиция', 'ipkirt-admin' ),
				'id'      => "{$prefix}position",
				'type'    => 'text',
			),
		),
	);

	return $meta_boxes;
}
