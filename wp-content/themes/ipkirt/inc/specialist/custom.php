<?php if ( ! defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

if ( ! is_admin() )
	add_action( 'pre_get_posts', 'theme_specialist_pre_get_posts' );
function theme_specialist_pre_get_posts( $query ) {
	if ( ! $query->is_main_query() )
		return;

	if ( $query->get( 'specialist_individual' ) ) {
		$query->set( 'posts_per_page', -1 );
	}
}

add_filter( 'wp_insert_post_data', 'default_comments_on' );
function default_comments_on( $data ) {
    if ( $data['post_type'] == 'specialist' ) {
        $data['comment_status'] = 'open';
    }

    return $data;
}

function theme_get_authors( $post_id = null ) {
	if ( empty( $post_id ) )
		$post_id = get_the_ID();

	$authors = get_post_meta( $post_id, 'post_authors', true );

	if ( empty( $authors ) )
		return;

	$authors = array_filter( wp_list_pluck( $authors, 'specialist_id' ) );

	if ( empty( $authors ) )
		return;

	return get_posts( array(
		'post_type' => 'specialist',
		'post__in'  => $authors,
		'orderby'   => 'post__in'
	) );
}

function theme_get_inline_authors( $post_id = null, $label = false ) {
	if ( empty( $post_id ) )
		$post_id = get_the_ID();

	return get_post_meta( $post_id, 'post_authors_string', true );
}

function _theme_get_inline_authors( $post_id = null, $label = false ) {
	$posts = theme_get_authors( $post_id );

	if ( empty( $posts ) )
		return;

	$posts = wp_list_pluck( $posts, 'post_title' );

	$inline = implode( ', ', $posts );

	if ( false === $label )
		return $inline;

	$label = 1 == count( $posts ) ? __( 'Автор:' ) : __( 'Авторы:' );

	return "$label $inline";
}

function get_specialist_register_link() {
	$post_id     = get_the_ID();
	$post_type   = get_post_type();
	$register_id = get_theme_option( 'form_consulting_page_id' );

	if ( ! $register_id )
		return;

	$args = array(
		'spec' => get_the_ID()
	);

	$terms = wp_get_post_terms( get_the_ID(), 'specialist_individual', array(
		'parent' => 0
	) );

	if ( ! is_wp_error( $terms ) AND $terms ) {
		$args['type'] = $terms[0]->term_id;
	}

	return add_query_arg( $args, get_permalink( $register_id ) );
}