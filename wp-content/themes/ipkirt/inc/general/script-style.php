<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

/**
 * Localize theme scripts in any place before do wp_footer
 *
 * @param array $data - add to global var
 * OR
 * @param $key
 * @param $data
 */
function theme_localize_script( $data = array() ) {
	$args = func_get_args();

	$group = '';

	if ( 2 == count( $args ) ) {
		$group = $args[0];
		$data  = $args[1];
	}

	Theme_Scripts_Styles::add_localize_script( $data, $group );
}


/**
 * Include Theme Styles and Scripts
 *
 * @package WordPress
 */

class Theme_Scripts_Styles
{
	protected $defer = array(
		'enabled' => false,
		'exclude' => array(
			// 'smartling'
		)
	);

	protected $folders = array(
		'js'  => 'scripts',
		'css' => 'stylesheets',
	);

	protected $is_localhost = false;

	protected $min = '.min';

	protected $theme_minimized = array(
		// 'common.js',
		// 'style.css',
	);

	protected static $localize_script_data = array(
		'cache_key' => 'theme_localize_script',
		'name'      => 'themeData',
		'handler'   => 'empty-script-js'
	);

	protected static $cache_key = 'theme_localize_script';

	function enqueue_scripts()
	{
		$this->enqueue_script( 'jquery-ui-core' );
		$this->enqueue_script( 'jquery-ui-widget' );
		$this->enqueue_script( 'scripts-js', 'scripts.js', array( 'jquery' ), true );
		$this->enqueue_script( 'andrew-js', 'andrew.js', array( 'jquery', 'scripts-js' ), true );

		$this->enqueue_script( 'empty-script-js', 'empty-script.js', array( 'jquery' ), true );

		if ( is_localhost() ) {
			// wp_enqueue_script( 'live-reload', '//192.168.1.100:35729/livereload.js', false, '1', false );
			// wp_enqueue_script( 'live-reload', '//localhost:35729/livereload.js', false, '1', false );
		}

		// wp_enqueue_script( 'nrec-typekit', 'https://use.typekit.net/uwf2hys.js', array(), '1.0' );
		// wp_add_inline_script( 'nrec-typekit', 'try{Typekit.load({ async: true });}catch(e){}' );
	}

	function enqueue_styles()
	{
		// wp_enqueue_style( 'google_fonts', '//fonts.googleapis.com/css?family=Playfair+Display:400,400italic' );
		// wp_enqueue_style( 'fast_fonts', 'https://fast.fonts.net/t/1.css?apiType=css&projectid=2f07c857-4965-4132-b11c-bcde839fc526' );
		
		$this->enqueue_style( 'theme-styles', 'style.css' );
	}

	function head_scripts()
	{
		$path = $this->uri( 'images/favicon' );	
		
		$code = get_theme_option( 'code_inside_head' );
	?>
		<link rel="shortcut icon" type="image/x-icon" href="<?php echo $path ?>/favicon.ico">

		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="imagetoolbar" content="no">
		<meta name="msthemecompatible" content="no">
		<meta name="cleartype" content="on">
		<meta name="HandheldFriendly" content="True">
		<meta name="format-detection" content="telephone=no">
		<meta name="format-detection" content="address=no">
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">

		<meta name="apple-mobile-web-app-title" content="ИПКиРТ">
		<meta name="application-name" content="ИПКиРТ">

		<?php if ( $code ): ?>
			<?php echo $code ?>
		<?php endif; ?>
	<?php 
	}

	function footer_scripts()
	{
		$code = get_theme_option( 'code_footer' );

		?>
			<?php if ( $code ): ?>
				<?php echo $code ?>
			<?php endif; ?>
		<?php 
	}

	function scripts_in_body()
	{
		if ( is_localhost() )
			return;

		$code = get_theme_option( 'code_in_body' );
		?>
			<?php if ( $code ): ?>
				<?php echo $code ?>
			<?php endif; ?>
		<?php 
	}

	/**
	 * Plugin Name: Enqueue jQuery in Footer
	 * Version:     0.0.1
	 * Plugin URI:  http://wpgrafie.de/836/
	 * Description: Prints jQuery in footer on front-end.
	 * Author:      Dominik Schilling
	 * Author URI:  http://wpgrafie.de/
	 */
	function enqueue_jquery_in_footer()
	{
		if ( is_admin() )
			return;

		wp_deregister_script( 'jquery' );
		wp_deregister_script( 'jquery-migrate' );

		// wp_enqueue_script( 'underscore' );

		// Load the copy of jQuery that comes with WordPress  
		// The last parameter set to TRUE states that it should be loaded  
		// in the footer.  
		wp_enqueue_script( 'jquery', '/wp-includes/js/jquery/jquery.js', false, '1.11.0', true );
		wp_enqueue_script( 'jquery-migrate', '/wp-includes/js/jquery/jquery-migrate.min.js', false, '1.2.1', true );
	}

	function admin_enqueue_scripts( $hook ) 
	{
		// $this->enqueue_style( 'custom_wp_admin_css', 'admin/admin_style.css' );
	}

	protected function uri( $file )
	{
		$uri = THEME_URI;

		$path = $this->get_file_path( $file );

		return "$uri/$path";
	}

	protected function get_file_path( $file )
	{
		// check external urls
		// if ( preg_match( '/^(http|https|\/\/)/', $file, $matches ) ) {
		// 	var_dump($matches);
		// 	return $file;
		// }


		$type = pathinfo( $file, PATHINFO_EXTENSION );

		if ( !$type )
			return $file;

		$is_min = in_array( $file, $this->theme_minimized );

		$file = str_replace( ".{$type}", '', $file );

		if ( is_localhost() OR $this->is_localhost ) {
			if ( 'js' == $type ) {
				$abs  = THEME_INC_ABS .'/assets';

				$path = "src/.js-cache/{$file}.es6.{$type}";

				if ( file_exists( "$abs/$path" ) )
					return $path;
			}
		}

		if ( $is_min )
			$file .= $this->min;

		return "{$this->folders[ $type ]}/{$file}.{$type}";
	}

	protected function get_file_version( $file )
	{
		$abs  = THEME_INC_ABS .'/assets';

		$path = $this->get_file_path( $file );

		if ( ! file_exists( $path ) )
			return;

		return filemtime( "$abs/$path" );
	}

	protected function enqueue_script( $handle, $src = '', $deps = array(), $in_footer = false )
	{
		if ( empty( $src ) OR $this->is_external( $src ) ) {
			$version = 1;
		} else {
			$version = $this->get_file_version( $src );
			$src     = $this->uri( $src );
		}

		wp_enqueue_script( $handle, $src, $deps, $version, $in_footer );
	}

	protected function enqueue_style( $handle, $src = '', $deps = array(), $media = 'all' )
	{
		if ( $this->is_external( $src ) ) {
			$version = 1;
		} else {
			$version = $this->get_file_version( $src );
			$src     = $this->uri( $src );
		}

		wp_enqueue_style( $handle, $src, $deps, $version, $media );
	}

	protected function is_external( $uri )
	{
		$external = array(
			'http',
			'//'
		);

		foreach ( $external as $start ) {
			if ( 0 === strpos( $uri, $start ) )
				return true;
		}

		return false;
	}

	public static function add_localize_script( $js_data = array(), $group = '' )
	{
		$data = self::$localize_script_data;

		$cache = wp_cache_get( $data['cache_key'] );

		if ( ! $cache )
			$cache = array();

		if ( $group )
			$cache[ $group ] = $js_data;
		else
			$cache = wp_parse_args( $js_data, $cache );

		wp_cache_set( $data['cache_key'], $cache );
	}

	public static function print_extra_localize_script( $name, $data )
	{
		$key = __FUNCTION__ . '-'. rand( 0, 100 );
		
		wp_enqueue_script( $key, 'test.js' );
		wp_scripts()->localize( $key, $name, $data );
		wp_scripts()->print_extra_script( $key );
		wp_dequeue_script( $key );
	}

	public static function print_extra_inline_script( $path )
	{
		$key = __FUNCTION__ . '-'. rand( 0, 100 );

		$path = THEME_INC_ABS ."/assets/{$this->folders}/{$path}";

		ob_start();
		include $path;
		$script = ob_get_clean();

		wp_enqueue_script( $key, 'test.js' );
		wp_scripts()->add_inline_script( $key, $script );
		wp_scripts()->print_inline_script( $key );
		wp_dequeue_script( $key );
	}

	function do_theme_localize_script()
	{
		$data = self::$localize_script_data;

		$cache = wp_cache_get( $data['cache_key'] );

		if ( ! $cache )
			return;

		wp_scripts()->localize( $data['handler'], $data['name'], $cache );
		wp_scripts()->print_extra_script( $data['handler'] );
		wp_dequeue_script( $data['handler'] );

		wp_cache_delete( $data['cache_key'] );
	}

	function add_defer_attribute( $tag, $handle )
	{
		if ( false === $this->defer['enabled'] )
			return $tag;

		if ( in_array( $handle, $this->defer['exclude'] ) )
			return $tag;

		return str_replace( ' src', ' defer src', $tag );
	}

	function __construct()
	{
		if ( is_localhost() )
			$this->min = '';

		if ( is_admin() ) {
			add_action( 'admin_enqueue_scripts', array( $this, 'admin_enqueue_scripts' ) );
			return;
		}

		// add_action( 'init', array( $this, 'enqueue_jquery_in_footer' ) );
		
		add_action( 'wp_footer', array( $this, 'do_theme_localize_script' ), 19 );

		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts' ), 100 );
		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_styles'  ), 100 );


		add_action( 'wp_head',   array( $this, 'head_scripts'   ) );
		add_action( 'wp_footer', array( $this, 'footer_scripts' ), 100 );

		add_action( 'theme_after_open_body', array( $this, 'scripts_in_body' ) );

		add_filter( 'script_loader_tag', array( $this, 'add_defer_attribute' ), 10, 2 );
		
		if ( ! is_admin() )
			self::add_localize_script( array( 'ajaxUrl' => admin_url( 'admin-ajax.php' ) ) );
	}
}
new Theme_Scripts_Styles;

