<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

add_action( 'pre_get_posts', 'theme_search_query' );
function theme_search_query( $query ) {
	if ( ! $query->is_main_query() )
		return;

	if ( ! $query->is_search() )
		return;

	$query->set( 'post_type', array(
		'event_single',
		'event_group',
		'event_traning',
		'event_study',
		'news'
	) );
}