<?php if ( ! defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

// add_filter( 'bcn_template_tags', 'yyy', 10, 3 );
function yyy( $replacements, $type, $id ) {
	var_dump($replacements, $type, $id);
	$replacements['%xxx%'] = '789';
	return $replacements;
	var_dump($replacements);
	die;
}

/**
 * 
 */
class Theme_Breadcrumbs
{
	protected $replacements;
	protected $types;
	protected $done = array();

	function __construct()
	{
		$this->opt = get_option( 'bcn_options' );

		add_filter( 'bcn_template_tags', array( $this, 'bcn_template_tags' ), 10, 3 );
	}

	function bcn_template_tags( $replacements, $types, $id )
	{
		$this->replacements = $replacements;
		$this->types        = $types;

		$this->add_custom_parent_page( 'specialist_individual' );
		$this->add_custom_parent_page( 'post-post', 'articles_page_id' );
		$this->add_specialist_parent_page();
		$this->add_articles_parent_page();

		return $this->replacements;
	}

	protected function add_articles_parent_page()
	{
		$type = 'post-post';

		if ( ! in_array( $type, $this->types ) )
			return;
	}

	protected function add_specialist_parent_page()
	{
		$type = 'post-specialist';

		if ( ! in_array( $type, $this->types ) )
			return;

		if ( 2 != $this->replacements['%position%'] ) {
			$this->replacements['%custom_parent_page%'] = '';
			return;
		}

		$post = get_queried_object();

		$taxonomies = array(
			'specialist_cat'  => 'specialists_page_id',
			'specialist_team' => 'team_page_id'
		);

		foreach ( $taxonomies as $taxonomy => $option_key ) {
			$terms = wp_get_post_terms( $post->ID, $taxonomy );
			
			if ( is_wp_error( $terms ) OR empty( $terms ) )
				continue;

			$this->set_page_data( $taxonomy, $option_key );
			break;
		}

		if ( ! isset( $this->done[ $taxonomy ] ) )
			$this->replacements['%custom_parent_page%'] = '';
	}

	protected function add_custom_parent_page( $type, $option_key = '' )
	{
		if ( ! in_array( $type, $this->types ) )
			return;


		if ( isset( $this->done[ $type ] ) ) {
			$this->replacements['%custom_parent_page%'] = '';
			return;
		}

		if ( empty( $option_key ) )
			$option_key = "{$type}_page_id";

		$this->set_page_data( $type, $option_key );
	}

	protected function set_page_data( $type, $option_key )
	{
		$page_id = get_theme_option( $option_key );

		if ( ! $page_id )
			return;

		$this->done[ $type ] = 1;

		$parents = get_post_ancestors( $page_id );

		$title = get_the_title( $page_id );
		$link  = get_permalink( $page_id );

		$this->replacements['%custom_parent_page%'] = $this->get_template( $title, $link );
	}

	protected function get_template( $title, $link ) 
	{
		return '
			<span property="itemListElement" typeof="ListItem">
				<a property="item" typeof="WebPage" title="'. $title .'" href="'. $link .'">
					<span property="name">'. $title .'</span>
				</a>
			</span>'. $this->opt['hseparator'];
	}
}
new Theme_Breadcrumbs;