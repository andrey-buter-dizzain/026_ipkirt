<?php 

add_filter( 'nav_menu_css_class', 'theme_nav_menu_css_class', 10, 4 );
function theme_nav_menu_css_class( $classes, $item, $args, $depth ) {
	if ( 'primary' !== $args->theme_location )
		return $classes;
	
	$classes[] = 0 == $depth ? 'menu__item' : 'submenu__item';

	return $classes;
}