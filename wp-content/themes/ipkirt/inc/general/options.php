<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 
/**
 * Populate product meta boxes
 *
 * @package WordPress
 * @subpackage ipkirt-admin
 */

add_filter( 'populate_theme_options', 'populate_theme_general_options' );
function populate_theme_general_options( $settings = array() ) {
	$id = __FUNCTION__;

	$settings['sections'][] = array(
		'title'   => __( 'Общее', 'ipkirt-admin' ),
		'id'      => $id
	);

	$settings['settings'][] = array(
		'id'      => 'phones',
		'label'   => __( 'Наши телефоны', 'ipkirt-admin' ),
		'type'    => 'textarea-simple',
		'rows'    => 3,
		'desc'    => __( 'Каждый телефон пишите с новой строки.', 'ipkirt-admin' ),
		'section' => $id,
	);

	$settings['settings'][] = array(
		'id'      => 'address',
		'label'   => __( 'Наш адрес', 'ipkirt-admin' ),
		'type'    => 'text',
		'section' => $id,
	);

	$settings['settings'][] = array(
		'id'      => 'email',
		'label'   => __( 'Email', 'ipkirt-admin' ),
		'type'    => 'text',
		'section' => $id,
	);

	$settings['settings'][] = array(
		'id'      => 'social-links',
		'label'   => __( 'Соц. сети', 'ipkirt-admin' ),
		'type'     => 'list-item',
		'settings' => array(
			array(
				'label' => __( 'Ссылка', 'ipkirt-admin' ),
				'id'    => 'url',
				'type'  => 'text',
			),
			array(
				'label' => __( 'Иконка', 'ipkirt-admin' ),
				'id'    => 'icon',
				'type'  => 'select',
				'choices'     => array( 
					array(
						'value' => '',
						'label' => __( '-- Выбрать --', 'ipkirt-admin' ),
					),
					array(
						'value' => 'vk',
						'label' => __( 'Вконтакте', 'ipkirt-admin' ),
					),
					array(
						'value' => 'fb',
						'label' => __( 'Facebook', 'ipkirt-admin' ),
					),
				),
			),
		),
		'section' => $id,
	);

	return $settings;
}

add_filter( 'populate_theme_options', 'populate_theme_front_page_options' );
function populate_theme_front_page_options( $settings = array() ) {
	$id = __FUNCTION__;

	$settings['sections'][] = array(
		'title'   => __( 'Главная страница', 'ipkirt-admin' ),
		'id'      => $id
	);

	$settings['settings'][] = array(
		'id'      => 'front_bloks',
		'label'   => __( 'Блоки', 'ipkirt-admin' ),
		'type'    => 'on-off-multiple',
		'choices' => array(
			array(
				'label' => 'Слайдер',
				'value' => 'slider',
			),
			array(
				'label' => 'Идет набор',
				'value' => 'new_events',
			),
			array(
				'label' => 'Ближайшие события',
				'value' => 'next_events',
			),
			array(
				'label' => 'Видео',
				'value' => 'video',
			),
			array(
				'label' => 'Новости',
				'value' => 'news',
			),
		),
		'class'   => 'theme-sortable-option',
		'section' => $id,
	);

	// $settings['settings'][] = array(
	// 	'id'      => 'front_slider_posts2',
	// 	'label'   => __( 'Посты в Слайдере', 'ipkirt-admin' ),
	// 	'type'    => 'page-checkbox',
	// 	'class'   => 'theme-sortable-option',
	// 	'section' => $id,
	// );

	$settings['settings'][] = array(
		'id'       => 'front_slider_posts',
		'label'    => __( 'Посты в Слайдере', 'ipkirt-admin' ),
		'type'     => 'list-item',
		'class'    => 'custom-type-list-item',
		'settings' => array(
			array(
				'id'        => 'post_id',
				'type'      => 'custom-post-type-select',
				'post_type' => 'event_single,event_study,event_training,event_group',
				'class'     => 'js-title-selector',
			),
		),
		'section' => $id,
	);

	$settings['settings'][] = array(
		'id'      => 'front_new_events_title',
		'label'   => __( 'Название блока "Идет набор"', 'ipkirt-admin' ),
		'type'    => 'text',
		'std'     => __( 'Идет набоор' ),
		'section' => $id,
	);

	$settings['settings'][] = array(
		'id'       => 'front_new_events_posts',
		'label'    => __( 'Посты событий в блоке "Идет набор"', 'ipkirt-admin' ),
		'type'     => 'list-item',
		'class'    => 'custom-type-list-item',
		'settings' => array(
			array(
				'id'        => 'post_id',
				'type'      => 'custom-post-type-select',
				'post_type' => 'event_single,event_study,event_training,event_group',
				'class'     => 'js-title-selector',
			),
		),
		'section' => $id,
	);

	$settings['settings'][] = array(
		'id'      => 'front_next_events_title',
		'label'   => __( 'Название блока "Ближайшие события"', 'ipkirt-admin' ),
		'type'    => 'text',
		'std'     => __( 'Ближайшие события' ),
		'section' => $id,
	);

	$settings['settings'][] = array(
		'id'       => 'front_next_events_posts',
		'label'    => __( 'Посты событий в блоке "Ближайшие события"', 'ipkirt-admin' ),
		'type'     => 'list-item',
		'class'    => 'custom-type-list-item',
		'settings' => array(
			array(
				'id'        => 'post_id',
				'type'      => 'custom-post-type-select',
				'post_type' => 'event_single,event_study,event_training,event_group',
				'class'     => 'js-title-selector',
			),
		),
		'section' => $id,
	);

	$settings['settings'][] = array(
		'id'      => 'front_video_title',
		'label'   => __( 'Название блока "Об институте"', 'ipkirt-admin' ),
		'type'    => 'text',
		'std'     => __( 'Об институте' ),
		'section' => $id,
	);

	$settings['settings'][] = array(
		'id'      => 'front_video_url',
		'label'   => __( 'URL на youtube видео', 'ipkirt-admin' ),
		'type'    => 'text',
		'section' => $id,
	);

	$settings['settings'][] = array(
		'id'      => 'front_video_poster',
		'label'   => __( 'Фото заставка видео', 'ipkirt-admin' ),
		'type'    => 'upload',
		'class'   => 'ot-upload-attachment-id',
		'section' => $id,
	);

	$settings['settings'][] = array(
		'id'      => 'front_news_title',
		'label'   => __( 'Название блока "Новости"', 'ipkirt-admin' ),
		'type'    => 'text',
		'std'     => __( 'Новости' ),
		'section' => $id,
	);

	$settings['settings'][] = array(
		'id'      => 'front_news_per_page',
		'label'   => __( 'Количество новстей', 'ipkirt-admin' ),
		'type'    => 'text',
		'std'     => 4,
		'section' => $id,
	);

	return $settings;
}

add_filter( 'populate_theme_options', 'populate_theme_footer_options' );
function populate_theme_footer_options( $settings = array() ) {
	$id = __FUNCTION__;

	$settings['sections'][] = array(
		'title'   => __( 'Footer', 'ipkirt-admin' ),
		'id'      => $id
	);

	$settings['settings'][] = array(
		'id'       => 'footer_menu_1',
		'label'    => __( 'Меню Медиатека', 'ipkirt-admin' ),
		'type'     => 'taxonomy-select',
		'taxonomy' => 'nav_menu',
		'wpmu'     => true,
		'section'  => $id,
	);

	$settings['settings'][] = array(
		'id'       => 'footer_menu_2',
		'label'    => __( 'Меню Общее', 'ipkirt-admin' ),
		'type'     => 'taxonomy-select',
		'taxonomy' => 'nav_menu',
		'wpmu'     => true,
		'section'  => $id,
	);

	$settings['settings'][] = array(
		'id'      => 'copyright',
		'label'   => __( 'Copyright &copy;', 'ipkirt-admin' ),
		'type'    => 'text',
		'wpmu'    => true,
		'section' => $id,
	);

	return $settings;
}

add_filter( 'populate_theme_options', 'populate_theme_code_options' );
function populate_theme_code_options( $settings = array() ) {
	$id = __FUNCTION__;

	$settings['sections'][] = array(
		'title'   => __( 'Код', 'ipkirt-admin' ),
		'id'      => $id
	);

	$settings['settings'][] = array(
		'id'      => 'code_inside_head',
		'label'   => __( 'Код внутри тэга head', 'ipkirt-admin' ),
		'type'    => 'javascript',
		'section' => $id,
	);

	$settings['settings'][] = array(
		'id'      => 'code_in_body',
		'label'   => __( 'Код после открытия тэга body', 'ipkirt-admin' ),
		'type'    => 'javascript',
		'section' => $id,
	);

	$settings['settings'][] = array(
		'id'      => 'code_footer',
		'label'   => __( 'Код перед закрытием тэга body', 'ipkirt-admin' ),
		'type'    => 'javascript',
		'section' => $id,
	);

	return $settings;
}


add_filter( 'populate_theme_options', 'populate_theme_ids_options', 100 );
function populate_theme_ids_options( $settings = array() ) {
	$id = 'ids';

	$settings['sections'][] = array(
		'title' => __( 'ID\'s', 'ipkirt-admin' ),
		'id'    => $id
	);

	$settings['settings'][] = array(
		'id'       => 'opinion_page_id',
		'label'    => __( '(Страница) Оставить отзыв', 'ipkirt-admin' ),
		'type'     => 'page-select',
		'wpmu'     => true,
		'section'  => $id,
	);

	$settings['settings'][] = array(
		'id'       => 'news_page_id',
		'label'    => __( '(Страница) Новости', 'ipkirt-admin' ),
		'type'     => 'page-select',
		'wpmu'     => true,
		'section'  => $id,
	);

	$settings['settings'][] = array(
		'id'       => 'event_single_page_id',
		'label'    => __( '(Страница) События', 'ipkirt-admin' ),
		'type'     => 'page-select',
		'wpmu'     => true,
		'section'  => $id,
	);

	$settings['settings'][] = array(
		'id'       => 'event_study_page_id',
		'label'    => __( '(Страница) Обучение', 'ipkirt-admin' ),
		'type'     => 'page-select',
		'wpmu'     => true,
		'section'  => $id,
	);

	$settings['settings'][] = array(
		'id'       => 'event_training_page_id',
		'label'    => __( '(Страница) Тренинги', 'ipkirt-admin' ),
		'type'     => 'page-select',
		'wpmu'     => true,
		'section'  => $id,
	);

	$settings['settings'][] = array(
		'id'       => 'event_group_page_id',
		'label'    => __( '(Страница) Группы', 'ipkirt-admin' ),
		'type'     => 'page-select',
		'wpmu'     => true,
		'section'  => $id,
	);

	$settings['settings'][] = array(
		'id'       => 'sitemap_page_id',
		'label'    => __( '(Страница) Карта сайта', 'ipkirt-admin' ),
		'type'     => 'page-select',
		'wpmu'     => true,
		'section'  => $id,
	);

	$settings['settings'][] = array(
		'id'       => 'gallery_page_id',
		'label'    => __( '(Страница) Фото и видео', 'ipkirt-admin' ),
		'type'     => 'page-select',
		'wpmu'     => true,
		'section'  => $id,
	);

	$settings['settings'][] = array(
		'id'       => 'form_event_single_page_id',
		'label'    => __( '(Страница) Записаться на событие', 'ipkirt-admin' ),
		'type'     => 'page-select',
		'wpmu'     => true,
		'section'  => $id,
	);

	$settings['settings'][] = array(
		'id'       => 'form_event_study_page_id',
		'label'    => __( '(Страница) Записаться на обучение', 'ipkirt-admin' ),
		'type'     => 'page-select',
		'wpmu'     => true,
		'section'  => $id,
	);

	$settings['settings'][] = array(
		'id'       => 'form_event_group_page_id',
		'label'    => __( '(Страница) Записаться на группу', 'ipkirt-admin' ),
		'type'     => 'page-select',
		'wpmu'     => true,
		'section'  => $id,
	);

	$settings['settings'][] = array(
		'id'       => 'form_event_training_page_id',
		'label'    => __( '(Страница) Записаться на тренинг', 'ipkirt-admin' ),
		'type'     => 'page-select',
		'wpmu'     => true,
		'section'  => $id,
	);

	$settings['settings'][] = array(
		'id'       => 'form_consulting_page_id',
		'label'    => __( '(Страница) Записаться на консультацию', 'ipkirt-admin' ),
		'type'     => 'page-select',
		'wpmu'     => true,
		'section'  => $id,
	);

	$settings['settings'][] = array(
		'id'       => 'form_question_page_id',
		'label'    => __( '(Страница) Задать вопрос', 'ipkirt-admin' ),
		'type'     => 'page-select',
		'wpmu'     => true,
		'section'  => $id,
	);

	$settings['settings'][] = array(
		'id'       => 'specialist_individual_page_id',
		'label'    => __( '(Страница) Индивидуальный прием', 'ipkirt-admin' ),
		'type'     => 'page-select',
		'wpmu'     => true,
		'section'  => $id,
	);

	$settings['settings'][] = array(
		'id'       => 'specialists_page_id',
		'label'    => __( '(Страница) Специалисты', 'ipkirt-admin' ),
		'type'     => 'page-select',
		'wpmu'     => true,
		'section'  => $id,
	);

	$settings['settings'][] = array(
		'id'       => 'team_page_id',
		'label'    => __( '(Страница) Наша команда', 'ipkirt-admin' ),
		'type'     => 'page-select',
		'wpmu'     => true,
		'section'  => $id,
	);

	$settings['settings'][] = array(
		'id'       => 'articles_page_id',
		'label'    => __( '(Страница) Статьи', 'ipkirt-admin' ),
		'type'     => 'page-select',
		'wpmu'     => true,
		'section'  => $id,
	);

	$settings['settings'][] = array(
		'id'       => 'contacts_page_id',
		'label'    => __( '(Страница) Контакты', 'ipkirt-admin' ),
		'type'     => 'page-select',
		'wpmu'     => true,
		'section'  => $id,
	);

	return $settings;
}