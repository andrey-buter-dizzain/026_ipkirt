<?php if ( ! defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

function theme_get_weekday_i18l( $n, $type = 'short' ) {
	$days_short = array(
		1 => 'пн',
		2 => 'вт',
		3 => 'ср',
		4 => 'чт',
		5 => 'пт',
		6 => 'сб',
		7 => 'вск',
	);

	return $days_short[ $n ];
}

add_filter( 'post_gallery', 'theme_default_post_gallery', 10, 3 );
function theme_default_post_gallery( $html, $attr, $instance ) {
	$post = get_post();

	if ( 'gallery' == get_post_type( $post ) )
		return $html;

	$atts = shortcode_atts( array(
		'order'      => 'ASC',
		'orderby'    => 'menu_order ID',
		'id'         => $post ? $post->ID : 0,
		'size'       => 'full',
		'include'    => '',
	), $attr, 'gallery' );

	if ( empty( $atts['include'] ) )
		return $html;

	$args = array( 
		'include'     => $atts['include'], 
		'post_status' => 'inherit', 
		'post_type'   => 'attachment', 
		'order'       => $atts['order'], 
		'orderby'     => $atts['orderby'],
		'post_mime_type' => 'image', 
	);

	$attachments = get_posts( $args );

	if ( empty( $attachments ) )
		return '';

	return get_theme_part( 'section/gallery', 'default', array( 
		'return' => true,
		'data'   => $attachments,
	) );
}