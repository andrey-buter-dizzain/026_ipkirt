<?php
/**
 * Register profile post type
 */

register_post_type( 'literature',
	array(
		'labels'        => array(
			'name'               => __( 'Литература', 'ipkirt-admin' ),
			'singular_name'      => __( 'Литература', 'ipkirt-admin' ),
			'add_new'            => __( 'Добавить литературу', 'ipkirt-admin' ),
			'add_new_item'       => __( 'Добавить литературу', 'ipkirt-admin' ),
			'edit'               => __( 'Изменить', 'ipkirt-admin' ),
			'edit_item'          => __( 'Изменить литературу', 'ipkirt-admin' ),
			'new_item'           => __( 'Добавить литературу', 'ipkirt-admin' ),
			'view'               => __( 'Смотреть новости', 'ipkirt-admin' ),
			'view_item'          => __( 'Смотреть литературу', 'ipkirt-admin' ),
			'search_items'       => __( 'Поиск литературы', 'ipkirt-admin' ),
			'not_found'          => __( 'Ничего не найдено', 'ipkirt-admin' ),
			'not_found_in_trash' => __( 'Ничего не найдено', 'ipkirt-admin' ),
		),
		'public'        => true,
		'menu_position' => 20,
		'hierarchical'  => false,
		'has_archive'   => false,
		'query_var'     => true,
		'supports'      => array( 
			'title', 
			'editor', 
			'thumbnail', 
		),
		'rewrite'       => array(
			'slug' => 'library'
		),
		'can_export'    => true,
		// 'publicly_queryable'  => false, // ???????????
		// 'exclude_from_search' => true
	)
);

register_taxonomy(
	'literature_cat',
	array( 'literature' ),
	array(
		'hierarchical' => true,
		'labels'       => array(
			'name'              => __( 'Категория', 'ipkirt-admin' ),
			'singular_name'     => __( 'Категория', 'ipkirt-admin' ),
			'search_items'      => __( 'Поиск категорий', 'ipkirt-admin' ),
			'all_items'         => __( 'Все категории', 'ipkirt-admin' ),
			'parent_item'       => __( 'Родительская категория', 'ipkirt-admin' ),
			'parent_item_colon' => __( 'Родительская категория:', 'ipkirt-admin' ),
			'edit_item'         => __( 'Изменить категорию', 'ipkirt-admin' ), 
			'update_item'       => __( 'Обновить категорию', 'ipkirt-admin' ),
			'add_new_item'      => __( 'Добавить новую категорию', 'ipkirt-admin' ),
			'new_item_name'     => __( 'Название категории', 'ipkirt-admin' ),
			'menu_name'         => __( 'Категория литературы', 'ipkirt-admin' ),
		),
		'show_ui'     => true,
		'show_admin_column' => true,
		'query_var'   => true,
		'public'      => false,
		'update_count_callback' => '_update_generic_term_count',
	)
);