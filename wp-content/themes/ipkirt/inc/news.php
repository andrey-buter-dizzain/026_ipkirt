<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 
/**
 * Page module
 */

class Inc_News extends Inc_Load_Section
{
	public static $section = 'news';

	protected static $include_path;

	protected static $uri_path;

	public static $partials = array(
		'model',
		'meta',
	);
}
Inc_News::load();