<?php if ( ! defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 
/**
 * Page module
 */

class Inc_Literature extends Inc_Load_Section
{
	public static $section = 'literature';

	protected static $include_path;

	protected static $uri_path;

	public static $partials = array(
		'model',
		// 'meta',
	);
}
Inc_Literature::load();