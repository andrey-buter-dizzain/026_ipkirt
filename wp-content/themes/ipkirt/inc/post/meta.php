<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

add_filter( 'populate_theme_meta_boxes', 'populate_post_meta_boxes' );
function populate_post_meta_boxes( $meta_boxes = array() ) {
	$post_type = 'post';

	$prefix = "{$post_type}_";

	$meta_boxes[] = array(
		'id'       => "{$prefix}meta",
		'title'    => __( 'Дополнительная информация', 'ipkirt-admin' ),
		'pages'    => array(
			'post',
			'news',
		),
		'context'  => 'normal',
		'priority' => 'default',
		'fields'   => array(
			array(
				'label'    => __( 'Автор', 'ipkirt-admin' ),
				'id'       => "{$prefix}authors_string",
				'type'     => 'text',
			),
		),
	);

	return $meta_boxes;
}


// add_filter( 'populate_theme_meta_boxes', 'populate_post_specialist_meta_boxes' );
// function populate_post_specialist_meta_boxes( $meta_boxes = array() ) {
// 	$post_type = 'post';

// 	$prefix = "{$post_type}_";

// 	$meta_boxes[] = array(
// 		'id'       => "{$prefix}meta",
// 		'title'    => __( 'Дополнительная информация', 'ipkirt-admin' ),
// 		'pages'    => array(
// 			'post',
// 			'news',
// 		),
// 		'context'  => 'normal',
// 		'priority' => 'default',
// 		'fields'   => array(
// 			array(
// 				'label'    => __( 'Автор/Ведущий', 'ipkirt-admin' ),
// 				'id'       => "{$prefix}authors",
// 				'type'     => 'list-item',
// 				'class'    => 'custom-type-list-item',
// 				'settings' => array(
// 					array(
// 						'id'        => 'specialist_id',
// 						'type'      => 'custom-post-type-select',
// 						'post_type' => 'specialist',
// 						'label'     => '',
// 						'class'     => 'js-title-selector',
// 					)
// 				)
// 			),
// 		),
// 	);

// 	return $meta_boxes;
// }