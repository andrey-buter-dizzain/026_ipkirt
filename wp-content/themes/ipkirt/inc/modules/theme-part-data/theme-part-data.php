<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

if ( !defined( 'THEME_PART_ABSPATH' ) )
	define( 'THEME_PART_ABSPATH', dirname( __FILE__ ) );

require_once( THEME_PART_ABSPATH .'/inc/names-cache.class.php' );
require_once( THEME_PART_ABSPATH .'/inc/data-cache.class.php' );
require_once( THEME_PART_ABSPATH .'/inc/public.php' );