<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

/**
 * Theme_Part_Data
 */
class Theme_Part_Data
{	
	protected static $instance;

	/**
	 * Chache group key
	 * @var string
	 */
	protected $cache_group = 'theme-part';

	protected function __construct()
	{
		// Set template data
		add_action( 'theme_part_before', array( $this, 'set_template_data' ), 10, 3 );

		// Delete template data
		add_action( 'theme_part_after', array( $this, 'delete_template_data' ) );
	}

	public static function get_instance()
	{
		if ( !self::$instance )
			self::$instance = new self();

		return self::$instance;
	}

	/**
	 * Set theme-part data
	 */
	public function set_template_data( $slug, $name, $data )
	{
		if ( !$data )
			return;

		$name = theme_part_set_new_name();

		if ( is_object( $data ) ) {
			$data = array(
				'data'               => $data,
				"_{$name}_is_object" => true
			);
		}

		$this->set_data( $name, $data );
	}

	/**
	 * Delete theme_part data
	 */
	public function delete_template_data()
	{
		$name = theme_part_get_current_name();

		theme_part_delete_current_name();

		$this->delete_data( $name );
	}

	/**
	 * Get template data if it's set in get_theme_part()
	 */
	public function get_template_data()
	{
		$name = theme_part_get_current_name();

		$data = $this->get_data( $name );

		if ( isset( $data["_{$name}_is_object"] ) )
			$data = $data['object'];

		return $data ? $data : array();
	}

	/**
	 * Set cache data
	 */
	protected function set_data( $key, $data )
	{
		wp_cache_set( $key, $data, $this->cache_group );
	}

	/**
	 * Delete cache data
	 */
	protected function delete_data( $key )
	{
		wp_cache_delete( $key, $this->cache_group );
	}

	/**
	 * Get cache data
	 */
	protected function get_data( $key )
	{
		return wp_cache_get( $key, $this->cache_group );
	}
}

Theme_Part_Data::get_instance();