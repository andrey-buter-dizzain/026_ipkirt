<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

/**
 * 
 */
class Theme_Part_Names
{
	/**
	 * Cache group key
	 * @var string
	 */
	protected static $cache_group = 'theme-part-names';

	/**
	 * Cahche names keys
	 * @var string
	 */
	protected static $cache_key = 'names';

	/**
	 * Default name
	 * @var string
	 */
	protected static $name = 'data';

	/**
	 * Theme-part set new data name
	 */
	public static function set_new_name()
	{
		$names = self::get_saved_names();

		$key = self::get_last_key( $names );

		if ( !$key ) {
			$names = array();
			$key   = -1;
		}			

		$key++;

		$names[$key] = "data-{$key}";

		self::save_names( $names );

		return $names[$key];
	}

	/**
	 * Get theme-part current data name
	 */
	public static function get_current_name() 
	{
		$names = self::get_saved_names();

		if ( !$names )
			return self::$name;

		$key = self::get_last_key( $names );

		return $names[$key];
	} 

	/**
	 * Delete theme part current (last) data name
	 */
	public static function delete_current_name()
	{
		$names = self::get_saved_names();

		if ( !$names )
			return false;

		$key = self::get_last_key( $names );

		unset( $names[$key] );

		self::save_names( $names );

		return true;
	}

	/**
	 * Get last el of array
	 */
	protected static function get_last_key( $array = array() )
	{
		if ( !$array )
			return null;

		return key( array_slice( $array, -1, 1, TRUE ) );
	}

	protected static function get_saved_names()
	{
		return wp_cache_get( self::$cache_key, self::$cache_group );
	}

	protected static function save_names( $data )
	{
		wp_cache_set( self::$cache_key, $data, self::$cache_group );
	}
}