jQuery(document).ready( function($) {
	/**
	 * See http://joxi.ru/5mdkJGXCkW19vr
	 */
	var parentSelector = '.custom-type-list-item-wrap';
	var titleSelector = '.js-title-selector';
	var timer;

	$(document).on('change', parentSelector + ' ' + titleSelector, function(e) {
		editTitle(e.currentTarget);
	});

	$(window).on('load', function() {
		$(titleSelector).each( function(i, el) {
			setTitle( el );
		});
	})

	// @see OT_UI.edit_title
	function editTitle(el) {
		var tagName = el.tagName.toLowerCase();

		if ( 'select' == tagName ) {
			setTitle( el );
		} 

		editTitleByInput( el );
	}

	function setTitle( el ) {
		$(el).parents('.option-tree-setting').children('.open').text( getValue( el ) );
	}

	function getValue(el) {
		if ( 'select' == el.tagName.toLowerCase() ) {
			return el.options[ el.selectedIndex ].text;
		}

		return el.value;
	}

	// @see OT_UI.edit_title
	function editTitleByInput( el ) {
		if ( timer )
			clearTimeout(timer);

		timer = setTimeout( function() {
			setTitle( el );
		}, 100);
	}

	$('#timestampdiv').each( function() {
		var $parent = $(this);
		var $save = $parent.find('a.save-timestamp');
		var $date = $('#js-ot-post-date');

		$save.on('click', function(e) {
			var month = $parent.find('#mm').val();
			var year  = $parent.find('#aa').val();
			var day   = $parent.find('#jj').val();
			
			$date.text( year + '-' + month + '-' + day );
		});
	});	

});
