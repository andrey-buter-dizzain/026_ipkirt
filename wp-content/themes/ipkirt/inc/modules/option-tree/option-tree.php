<?php 

class Inc_Module_Option_Tree_Addon extends Inc_Load_Section
{
	public static $section;

	protected static $include_path;

	protected static $uri_path;

	public static $partials = array(
		'checkbox-simple',
		'hidden',
		'on-off-simple',
		'on-off-multiple',
		// 'custom-list-item',
		'sortable-options',
		'custom-post-type-select-2',
		'list-item-simple',
	);

	public static function on_load() 
	{
		add_action( 'admin_enqueue_scripts', __CLASS__ .'::admin_enqueue_scripts' );
	}

	public static function admin_enqueue_scripts()
	{
		$module = basename( __DIR__ );

		$path = THEME_MODULE_URI . "/{$module}/assets";

		wp_enqueue_style( 'theme_ot_styles', "$path/ot-admin.css", array( 'ot-admin-css' ) );
		wp_enqueue_script( 'theme_ot_scripts', "$path/ot-admin.js", array( 'jquery', 'ot-admin-js' ) );
	}

	/**
	 * Preload function
	 */
	public static function pre_load() 
	{
		if ( !is_admin() )
			return;

		$module = basename( __DIR__ );

		static::$section = 'modules/'. $module;

		static::$section = static::$section . '/fields';
	}

	/**
	 * Get partial function
	 */
	protected static function get_partial( $partial = '' )
	{
		return $partial .'/'. $partial;
	}
}
Inc_Module_Option_Tree_Addon::load();