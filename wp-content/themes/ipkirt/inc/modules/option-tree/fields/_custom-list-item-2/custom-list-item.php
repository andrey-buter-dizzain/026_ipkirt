<?php if ( ! defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' );

function ot_type_custom_list_item( $args = array() ) {
		
	/* turns arguments array into variables */
	extract( $args );
	
	/* verify a description */
	$has_desc = $field_desc ? true : false;

	// Default
	$sortable = true;

	// Check if the list can be sorted
	if ( ! empty( $field_class ) ) {
		$classes = explode( ' ', $field_class );
		if ( in_array( 'not-sortable', $classes ) ) {
			$sortable = false;
			str_replace( 'not-sortable', '', $field_class );
		}
	}

	$desc_class = $has_desc ? 'has-desc' : 'no-desc';
	$sortable_class = $sortable ? 'option-tree-sortable' : '';

	$esc_field_id = esc_attr( $field_id );

	$list_desc = $sortable ? __( 'You can re-order with drag & drop, the order will update after saving.', 'option-tree' ) : '';

?>
	<div class="format-setting type-list-item <?php echo $desc_class ?>">

		<?php if ( $has_desc ): ?>
			<div class="description">
				<?php echo htmlspecialchars_decode( $field_desc ) ?>
			</div>
		<?php endif ?>

		<div class="format-setting-inner">

			<input type="hidden" name="<?php echo "{$esc_field_id}_settings_array" ?>" id="<?php echo "{$esc_field_id}_settings_array" ?>" value="<?php echo ot_encode( serialize( $field_settings ) ) ?>" />
			<?php 
				/** 
				 * settings pages have array wrappers like 'option_tree'.
				 * So we need that value to create a proper array to save to.
				 * This is only for NON metabox settings.
				 */
				if ( ! isset( $get_option ) )
					$get_option = '';
			?>
				
			<ul class="option-tree-setting-wrap <?php echo $sortable_class ?>" data-name="<?php echo $esc_field_id ?>" data-id="<?php echo esc_attr( $post_id ) ?>" data-get-option="<?php echo esc_attr( $get_option ) ?>" data-type="<?php echo esc_attr( $type ) ?>">
				<?php if ( is_array( $field_value ) && ! empty( $field_value ) ): ?>
					<?php foreach ( $field_value as $key => $list_item ): ?>
						<li class="ui-state-default list-list-item">
							<?php ot_list_item_view( $field_id, $key, $list_item, $post_id, $get_option, $field_settings, $type ); ?>
						</li>
					<?php endforeach ?>
				<?php endif ?>
			</ul>
			
			<a href="javascript:void(0);" class="option-tree-list-item-add option-tree-ui-button button button-primary right hug-right" title="<?php _e( 'Add New', 'option-tree' ) ?>">
				<?php _e( 'Add New', 'option-tree' ) ?>
			</a>';
			
			
			<div class="list-item-description">
				<?php echo apply_filters( 'ot_list_item_description', $list_desc, $field_id ) ?>
			</div>
		
		</div>

	</div>
	<?php 
}