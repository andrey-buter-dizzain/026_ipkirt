<?php 


/**
* On/Off option type
*
* See @ot_display_by_type to see the full list of available arguments.
*
* @param     array     The options arguments
* @return    string    The gallery metabox markup.
*
* @access    public
* @since     2.2.0
*/

function ot_type_on_off_multiple( $args = array() ) {
	/* turns arguments array into variables */
	extract( $args );

	/* verify a description */
	$has_desc = $field_desc ? true : false;

	// Onle on create new post set default value
	if ( !isset( $_GET['post'] ) AND isset( $field_settings['std'] ) AND $field_settings['std'] )
		$field_value = $field_settings['std'];

	$triggers = array(
		array(
			'value'   => 1,
			'label'   => __( 'On', 'option-tree' ),
		),
		array(
			'value'   => '',
			'label'   => __( 'Off', 'option-tree' ),
		)
	);

	if ( false !== strpos( $args['field_class'], 'theme-sortable-option' ) AND $field_value ) {
		$order = array_keys( $field_value );
		$new_choices = array();

		foreach ( $field_choices as $key => $choice ) {
			$new = array_search( $choice['value'], $order );

			$new_choices[ $new ] = $choice;
		}

		ksort( $new_choices );

		$field_choices = $new_choices;
	}
?>
	<div class="format-setting type-radio <?php echo ( $has_desc ? 'has-desc' : 'no-desc' ) ?>">
		<?php if ( $has_desc ): ?>
			<div class="description">
				<?php echo htmlspecialchars_decode( $field_desc ) ?>
			</div>
		<?php endif ?>

		<div class="format-setting-inner">
			<?php foreach ( $field_choices as $choice ): 
			?>
				<div style="margin: 5px 0">
					<div class="on-off-switch" style="display: inline-block; vertical-align: middle;">
						<?php foreach ( $triggers as $key => $trigger ): 
							$id = esc_attr( "{$field_id}-{$choice['value']}-{$key}" );
							$name = esc_attr( "{$field_name}[{$choice['value']}]" );
							$value = ( is_array( $field_value ) AND isset( $field_value[ $choice['value'] ] ) ) ? $field_value[ $choice['value'] ] : '';
						?>
							<input 
								type="radio" 
								name="<?php echo $name ?>" 
								id="<?php echo $id ?>" 
								value="<?php echo esc_attr( $trigger['value'] ) ?>" 
								<?php checked( $value, $trigger['value'] ) ?> 
								class="radio option-tree-ui-radio <?php echo esc_attr( $field_class ) ?>" 
							/>
							<label for="<?php echo $id ?>" onclick="">
								<?php echo esc_attr( $trigger['label'] ) ?>
							</label>
						<?php endforeach ?>
						<span class="slide-button"></span>
					</div>
					<div style="display: inline-block; vertical-align: middle; margin-left: 5px">
						<?php echo $choice['label'] ?>
					</div>
				</div>
			<?php endforeach ?>
		</div>
	</div>
<?php 

}