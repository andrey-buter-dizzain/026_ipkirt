<?php 
/**
 * Based on Google Fonts option type.
 */

function ot_type_list_item_simple( $args = array() ) {
	extract( $args );

	/* verify a description */
	$has_desc = $field_desc ? true : false;
	$has_desc_class = ( $has_desc ? 'has-desc' : 'no-desc' );

	if ( empty( $field_value ) )
		$field_value = array();

	$default = ot_type_list_item_simple_filter_args( $args, array() );

?>
	<div class="format-setting type-google-font <?php echo $has_desc_class ?>">
		<?php if ( $has_desc ): ?>
			<div class="description">
				<?php echo htmlspecialchars_decode( $field_desc ) ?>
			</div>
		<?php endif ?>

		<div class="format-setting-inner">
			<?php foreach ( (array) $field_value as $key => $values ): 
				$fields = ot_type_list_item_simple_filter_args( $args, $values, $key );
			?>
				<div class="type-google-font-group">
					<div class="option-tree-google-font-family">
						<?php foreach ( $fields as $field ): ?>
							<?php echo ot_display_by_type( $field ) ?>
						<?php endforeach ?>
						<a href="javascript:void(0);" class="js-remove-google-font option-tree-ui-button button button-secondary light" title="<?php _e( 'Remove Item', 'option-tree' ) ?>">
							<span class="icon ot-icon-minus-circle"/>
							<?php _e( 'Remove Item', 'option-tree' ) ?>
						</a>
					</div>
				</div>
			<?php endforeach ?>
			<div class="type-google-font-group-clone">
				<div class="option-tree-google-font-family">
					<?php foreach ( $default as $field ): ?>
						<?php echo ot_display_by_type( $field ) ?>
					<?php endforeach ?>
					<a href="javascript:void(0);" class="js-remove-google-font option-tree-ui-button button button-secondary light" title="<?php _e( 'Remove Item', 'option-tree' ) ?>">
						<span class="icon ot-icon-minus-circle"/>
						<?php _e( 'Remove Item', 'option-tree' ) ?>
					</a>
				</div>
			</div>
			<a href="javascript:void(0);" class="js-add-google-font option-tree-ui-button button button-primary right hug-right" title="<?php _e( 'Add New', 'option-tree' ) ?>">
				<?php _e( 'Add New', 'option-tree' ) ?>
			</a>
		</div>
	</div>
<?php 
}

function ot_type_list_item_simple_filter_args( $args, $values, $key = 0 ) {
	$common = $args;

	$common['field_settings'] = array();
	$common['field_value']    = array();
	$common['field_desc']     = '';
	$common['field_std']      = '';

	$output = array();

	foreach ( $args['field_settings'] as $key => $field ) {
		if ( ! isset( $field['type'] ) OR empty( $field['type'] ) )
			continue;

		if ( ! isset( $field['id'] ) OR empty( $field['id'] ) )
			continue;

		$field_value = isset( $values[ $field['id'] ] ) ? $values[ $field['id'] ] : '';

		if ( isset( $field['std'] ) )
			$field_value = ot_filter_std_value( $field_value, $field['std'] );

		foreach ( $field as $key => $value ) {
			if ( in_array( $key, array( 'type', 'post_id', 'get_option' ) ) )
				continue;

			$field["field_{$key}"] = $value;
			unset( $field[ $key ] );
		}

		$field = wp_parse_args( $field, $common );
		$field['field_value'] = $field_value;
		$field['field_name']  = "{$args['field_name']}[][{$field['field_id']}]";
		$field['field_id']    = "{$args['field_id']}_{$field['field_id']}_{$key}";

		$output[] = $field;
	}

	return $output;
}

add_filter( 'ot_validate_setting', 'thtme_ot_validate_setting', 10, 3 );
function thtme_ot_validate_setting( $input, $type, $field_id ) {
	$type = str_replace( '-', '_', $type );
	
	if ( 'list_item_simple' != $type )
		return $input;

	// remove last item,
	// because last value saved by hidden clone fields part
	// search class 'type-google-font-group-clone'
	array_pop( $input );

	return $input;
}