<?php
// Prevent loading this file directly
defined( 'ABSPATH' ) || exit;

/**
 * Text option type.
 *
 * See @ot_display_by_type to see the full list of available arguments.
 *
 * @param     array     An array of arguments.
 * @return    string
 *
 * @access    public
 * @since     2.0
 */
if ( ! function_exists( 'ot_type_hidden' ) ) {

	function ot_type_hidden( $args = array() ) {
		// Try to determine field type by its arguments
		if (!empty($args['field_settings']))
			$args['type'] = 'list-item';
			/*
		elseif (!empty($args['field_choices']) AND is_array($args['field_value']))
			$args['type'] = 'checkbox';
		elseif (!empty($args['field_choices']))
			$args['type'] = 'select';
			*/
		elseif (is_array($args['field_value']) AND !empty($args['field_value']))
		{
			// Set field choises from field values
			$args['field_choices'] = array();
			foreach ($args['field_value'] as $key => $value) {
				$args['field_choices'][$key] = array(
					'label' => $key,
					'value' => $value
					);
			}
			$args['type'] = 'checkbox';
		}
		else
			$args['type'] = 'textarea-simple';
		
		echo '<div class="hide-parent" style="width:0;height:0">';
		ot_display_by_type($args);
		echo '</div>';
	}
}

/**
 * Hide all hidden fields
 */
add_action('admin_footer-appearance_page_ot-theme-options', 'ot_hide_hidden_fields');
function ot_hide_hidden_fields() {
	?><script type="text/javascript">
		jQuery(document).ready(function($) {
			$('div.format-setting-wrap:has(> .hide-parent)').hide();
		})
	</script><?php
}