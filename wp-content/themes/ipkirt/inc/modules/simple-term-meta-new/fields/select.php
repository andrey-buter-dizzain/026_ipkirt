<?php 
/**
 * Select filed template
 *
 * Simple term meta
 */
?>
<select name="<?php echo $name ?>" id="<?php echo $id ?>">
	<?php foreach ( $choices as $key => $option ): ?>
		<option value="<?php echo $key ?>" <?php selected( $value, $key ) ?>><?php echo $option ?></option>
	<?php endforeach ?>
</select>