<?php 

add_filter( 'simple_term_meta_fields_new', 'add_exhibition_cat_term_meta' );
function add_exhibition_cat_term_meta( $taxes_meta ) {
	$tax = 'exhibition_cat';

	$taxes_meta[] = array(
		'taxonomy' => $tax,
		'fields'   => array(
			array(
				'label'   => __( 'Gallery Guide Location Description' ,'theme-admin' ),
				'id'      => 'art_guide_location_desc',
				'type'    => 'textarea',
				'choices' => array(),
				'desc'  => __( 'Used for OLD category for redirect to NEW category.', 'theme-admin' )
			),
		)
	);

	return $taxes_meta;
}