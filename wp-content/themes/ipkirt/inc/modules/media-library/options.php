<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 
/**
 * Populate product meta boxes
 *
 * @package WordPress
 * @subpackage ipkirt-admin
 */

add_filter( 'populate_theme_options', 'populate_module_media_library_options' );
function populate_module_media_library_options( $settings = array() ) {
	$id = __FUNCTION__;

	$settings['sections'][] = array(
		'title'   => __( 'Медиотека', 'ipkirt-admin' ),
		'id'      => $id
	);

	$settings['settings'][] = array(
		'id'       => 'media_library_parent_id',
		'label'    => __( 'Родительская страница Медиатеки', 'ipkirt-admin' ),
		'type'     => 'page-select',
		'wpmu'     => true,
		'section'  => $id,
	);

	return $settings;
}