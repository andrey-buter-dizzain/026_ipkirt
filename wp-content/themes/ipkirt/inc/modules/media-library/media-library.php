<?php if ( ! defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

require_once dirname( __FILE__ ) .'/options.php';

/**
 * 
 */
class Module_Media_Library
{
	protected $menu_location = '';

	protected $tags = array();

	const SEP  = ',';
	const ATTR = 'tags';

	protected $current_url = '';

	protected $post_types = array(
		'literature',
		'post',
		'mass-media'
	);

	function __construct()
	{
		add_action( 'template_redirect', array( $this, 'check_media_library' ) );
	}

	function check_media_library()
	{
		if ( ! $this->is_media_library() )
			return;

		$this->current_url = current_url();
		$this->init_tags();

		$tmpl = '';

		if ( is_page() ) {
			if ( is_page_template( 'default' ) )
				$tmpl = 'page';
		}
		else
		if ( is_singular( 'literature' ) )
			$tmpl = ''; // load single-literature.php
		else
		if ( is_singular() )
			$tmpl = 'single';

		if ( ! $tmpl )
			return;

		get_theme_part( "{$tmpl}-mediateka" );
		die;
	}

	public function is_media_library()
	{
		$parent_id = absint( get_theme_option( 'media_library_parent_id' ) );

		if ( ! $parent_id )
			return false;

		if ( is_page( $parent_id ) )
			return true;

		if ( is_page() AND $GLOBALS['post']->post_parent === $parent_id )
			return true;

		if ( is_singular( $this->post_types ) )
			return true;

		return false;
	}

	protected function init_tags()
	{
		if ( ! isset( $_GET[ self::ATTR ] ) )
			return;

		if ( empty( $_GET[ self::ATTR ] ) )
			return;

		$this->tags = explode( self::SEP, $_GET[ self::ATTR ] );
	}

	public function get_meta_query( $taxonomy )
	{
		if ( empty( $this->tags ) )
			return array();

		return array(
			array(
				'taxonomy' => $taxonomy,
				'field'    => 'term_id',
				'terms'    => $this->tags,
			),
		);
	}

	public function get_url_by_term_id( $term_id )
	{
		$tags = $this->tags;

		if ( $this->is_term_selected( $term_id ) ) {
			$key = array_search( $term_id, $tags );
			unset( $tags[ $key ] );
		} else {
			$tags[] = $term_id;
		}

		return add_query_arg( array(
			self::ATTR => empty( $tags ) ? false : implode( self::SEP, $tags ),
		), $this->current_url );
	}

	public function is_term_selected( $term_id )
	{
		return in_array( $term_id, $this->tags );
	}

	public function get_current_url()
	{
		return $this->current_url;
	}

	public function get_tags()
	{
		return $this->tags;
	}

	public function get_inline_tags()
	{
		return implode( self::SEP, $this->tags );
	}

	// function check_media_library() 
	// {
	// 	$menu_items = $this->get_menu_items();

	// 	if ( ! $menu_items )
	// 		return;

	// 	if ( $this-> ) {
	// 		# code...
	// 	}
	// }

	// protected function get_menu_items()
	// {
	// 	$locations = get_nav_menu_locations();

	// 	if ( ! isset( $locations[ $this->menu_location ] ) )
	// 		return;

	// 	$menu = wp_get_nav_menu_object( $locations[ $this->menu_location ] );

	// 	return wp_get_nav_menu_items( $menu->term_id, array( 'update_post_term_cache' => false ) );
	// }
}
theme_obj()->init_object( 'media_library', 'Module_Media_Library' );