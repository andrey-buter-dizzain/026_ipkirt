<?php if ( ! defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

add_filter( 'module_cf7_dynamic_taxonomies', 'module_cf7_dynamic_taxonomies', 10, 2 );
function module_cf7_dynamic_taxonomies( $choices = array(), $args = array() ) {
	if ( ! isset( $args['taxonomy'] ) )
		return $choices;

	$default = isset( $args['default'] ) ? $args['default'] : '---';

	$terms = get_terms( array(
		'taxonomy' => $args['taxonomy'],
		'orderby'  => 'term_order',
		'parent'   => 0
	) );

	if ( empty( $terms ) AND is_wp_error( $terms ) )
		return $choices;

	$choices = array(
		$default => '',
	);

	foreach ( $terms as $term ) {
		$choices[ $term->name ] = $term->term_id;
	}

	return $choices;
}

add_filter( 'module_cf7_dynamic_posts', 'module_cf7_dynamic_posts', 10, 2 );
function module_cf7_dynamic_posts( $choices = array(), $args = array() ) {
	if ( ! isset( $args['post_type'] ) )
		return $choices;

	$default = isset( $args['default'] ) ? $args['default'] : '---';

	$atts = array(
		'posts_per_page' => -1, 
		'post_type' => $args['post_type']
	);

	if ( isset( $args['taxonomy'] ) ) {
		if ( isset( $args['term_id'] ) ) {
			$terms = array( $args['term_id'] );
		} else {
			$terms = get_terms( array(
				'taxonomy' => $args['taxonomy'],
				'fields'   => 'id=>name'
			) );
			
			$terms = array_keys( $terms );
		}

		if ( $terms ) {
			$atts['tax_query'] = array(
				array(
					'taxonomy' => $args['taxonomy'],
					'field'    => 'term_id',
					'terms'    => $terms,
				),
			);
		}
	}

	$posts = get_posts( $atts );

	if ( ! $posts )
		return $choices;

	$choices = wp_list_pluck( $posts, 'ID', 'post_title' );

	$choices = array_merge( array(
		$default => '',
	), $choices );

	return $choices;
}

add_filter( 'module_cf7_dynamic_related_posts', 'module_cf7_dynamic_related_posts', 10, 2 );
function module_cf7_dynamic_related_posts( $choices = array(), $args = array() ) {
	$terms = get_terms( array(
		'taxonomy' => $args['taxonomy'],
	) );

	$all = module_cf7_dynamic_posts( array(), $args );

	$output = array( 
		0 => $all 
	);

	foreach ( $terms as $term ) {
		$args['term_id'] = $term->term_id;
		$output[ $term->term_id ] = module_cf7_dynamic_posts( array(), $args );
	}

	wp_localize_script( 'scripts-js', 'moduleDynamicSelect', $output );

	if ( isset( $_GET['type'] ) AND absint( $_GET['type'] ) )
		return $output[ $_GET['type'] ];

	return $output[0];
}