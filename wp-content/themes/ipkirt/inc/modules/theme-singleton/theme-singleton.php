<?php if ( ! defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

function theme_obj() {
	return Theme_Object::inst();
}

/**
 * 
 */
class Theme_Object
{
	public static $instance;

	protected $objects = array();

	protected function __construct() {}

	public static function inst()
	{
		if ( ! self::$instance )
			self::$instance = new self();

		return self::$instance;
	}

	public function init_object( $key, $class_name )
	{
		if ( is_string( $class_name ) ) {
			if ( ! class_exists( $class_name ) )
				return;

			$class_name = new $class_name;
		}

		$this->objects[ $key ] = $class_name;
	}

	public function __call( $name, $arguments = array() ) 
	{
		if ( ! isset( $this->objects[ $name ] ) )
			return;

		return $this->objects[ $name ];
	}
}