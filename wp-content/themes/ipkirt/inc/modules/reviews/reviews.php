<?php if ( ! defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

add_action( 'wpcf7_before_send_mail', 'module_reviews_before_send_email' );
function module_reviews_before_send_email( $contact_form ) {
	$form_id = get_theme_option( 'module_reviews_form' );

	if ( ! $form_id )
		return;

	if ( $form_id != $contact_form->id() )
		return;

	if ( isset( $_POST['specialists'] ) AND $_POST['specialists'] ) {
		$post_id = absint( $_POST['specialists'] );
	} else {
		$post_id = get_theme_option( 'module_reviews_page_id' );
	}

	$data = array(
		'comment_post_ID'      => $post_id,
		'comment_author'       => esc_attr( "{$_POST['first-name']} {$_POST['last-name']}" ),
		'comment_author_email' => esc_attr( $_POST['email'] ),
		'comment_content'      => esc_textarea( $_POST['comments'] ),
		'comment_author_IP'    => $_SERVER['REMOTE_ADDR'],
		'comment_agent'        => $_SERVER['HTTP_USER_AGENT'],
		'comment_date'         => current_time( 'mysql' ),
		'comment_approved'     => 0,
	);

	wp_insert_comment( $data );
}


add_filter( 'populate_theme_options', 'module_reviews_options' );
function module_reviews_options( $settings = array() ) {
	$id = __FUNCTION__;

	$settings['sections'][] = array(
		'title'   => __( 'Отзывы', 'ipkirt-admin' ),
		'id'      => $id
	);

	$settings['settings'][] = array(
		'id'        => 'module_reviews_form',
		'label'     => __( 'Форма отзыва', 'ipkirt-admin' ),
		'type'      => 'custom-post-type-select',
		'post_type' => 'wpcf7_contact_form',
		'wpmu'      => true,
		'section'   => $id,
	);

	$settings['settings'][] = array(
		'id'        => 'module_reviews_page_id',
		'label'     => __( 'Страница отзывов', 'ipkirt-admin' ),
		'type'      => 'page-select',
		'wpmu'      => true,
		'section'   => $id,
	);

	return $settings;
}

add_filter( 'comments_template_query_args', 'module_reviews_comments_template_query_args' );
function module_reviews_comments_template_query_args( $comment_args ) {
	$comment_args['order'] = 'DESC';
	return $comment_args;
}

/**
* 
*/
class Module_Reviews_Walker extends Walker_Comment
{
	protected function comment( $comment, $depth, $args ) {
		if ( 'div' == $args['style'] ) {
			$tag = 'div';
			$add_below = 'comment';
		} else {
			$tag = 'li';
			$add_below = 'div-comment';
		}

		$classes = array(
			'reviews__item'
		);
		$classes[] = $this->has_children ? 'parent' : '';
	?>
		<<?php echo $tag; ?> <?php comment_class( $classes, $comment ); ?> id="comment-<?php comment_ID(); ?>">
			<?php if ( 'div' != $args['style'] ) : ?>
				<div id="div-comment-<?php comment_ID(); ?>" class="comment-body reviews__item-body">
			<?php endif; ?>
					<div class="reviews__item-content">
						<?php comment_text( $comment, array_merge( $args, array( 
							'add_below' => $add_below, 
							'depth' => $depth, 
							'max_depth' => $args['max_depth'] 
						) ) ); ?>
						<div class="reviews__item-content-angle"></div>
					</div>

					<div class="reviews__item-author comment-author vcard">
						<?php if ( 0 != $args['avatar_size'] ) echo get_avatar( $comment, $args['avatar_size'] ); ?>
						<?php
							/* translators: %s: comment author link */
							printf( __( '%s <span class="says">says:</span>' ),
								sprintf( '<cite class="fn">%s</cite>', get_comment_author_link( $comment ) )
							);
						?>
					</div>
					<?php if ( '0' == $comment->comment_approved ) : ?>
						<em class="comment-awaiting-moderation"><?php _e( 'Your comment is awaiting moderation.' ) ?></em>
						<br />
					<?php endif; ?>

					<div class="reviews__item-date comment-meta commentmetadata">
						<a href="<?php echo esc_url( get_comment_link( $comment, $args ) ); ?>">
							<?php echo get_comment_date( 'j F Y', $comment ); ?>
							<?php
								/* translators: 1: comment date, 2: comment time */
								//printf( __( '%1$s at %2$s' ), get_comment_date( 'j F Y', $comment ),  get_comment_time() ); ?>
						</a>
						<?php edit_comment_link( __( '(Edit)' ), '&nbsp;&nbsp;', '' ); ?>
					</div>

					<?php /*comment_reply_link( array_merge( $args, array(
						'add_below' => $add_below,
						'depth'     => $depth,
						'max_depth' => $args['max_depth'],
						'before'    => '<div class="reply">',
						'after'     => '</div>'
					) ) );*/ ?>

			<?php if ( 'div' != $args['style'] ) : ?>
				</div>
			<?php endif; ?>
<?php
	}
}