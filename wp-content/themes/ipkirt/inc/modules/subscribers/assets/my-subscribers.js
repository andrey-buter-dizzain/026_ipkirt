jQuery(document).ready(function($) {

	$('.js-my-subscribers-from').map( function() {
		$main = $(this);

		$main.find( 'form' ).on( 'submit', function(e){
			e.preventDefault();
			
			var $form  = $(this);
			var data   = $form.serialize();

			var $emailField = $form.find('.form-email'),
				$nameField  = $form.find('.form-name');
			
			data = data + '&action=my_subscribers&subscribe_submit=1';

			$.ajax({
				url: themeData.ajaxUrl,
				type: 'post',
				dataType: 'json',
				data: data,
				success: function(data) {
					doIfErrors(  data.errors, $form );
					doIfSuccess( data.success, $form );
				}
			});
		});

		function doIfSuccess( success, $form ) {
			if ( undefined === success ) 
				return;

			$main.find( '.success-message' ).removeClass('hidden');
			$main.find( '.hide-on-success' ).addClass('hidden');

			$form.find( 'input[type=text]' ).val( '' );
		}

		function doIfErrors( errors, $form ) {
			if ( undefined === errors ) 
				return;

			var $emailField = $form.find('.form-email'),
				$nameField  = $form.find('.form-name');

			errorField( errors.email, $emailField, 'email' );
			errorField( errors.name, $nameField, 'name' );
		}

		function errorField( error, field, key ) {
			var fieldErrorClass = '.error-message-' + key;

			if ( undefined === error ) {
				field.removeClass( 'error' );
				$main.find( fieldErrorClass ).addClass( 'hidden' );
			} else {
				field.addClass( 'error' );

				if ( 'email' == key ) {
					$main.find( fieldErrorClass ).addClass( 'hidden' );

					if ( 'empty' == error )
						fieldErrorClass = fieldErrorClass + '-empty';
					else if( 'novalid' == error )
						fieldErrorClass = fieldErrorClass + '-novalid';
					else
						fieldErrorClass = fieldErrorClass + '-dublicate';
				} 

				$main.find( fieldErrorClass ).removeClass( 'hidden' );
			}
		}
	});

}); // End of jQuery shell function