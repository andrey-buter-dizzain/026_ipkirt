jQuery(document).ready(function($) {

	$('.js-my-subscribers-from').map( function() {
		$main = $(this);

		$main.find('form').on('submit', function(e){
			e.preventDefault();
			
			var $form  = $(this);
			var data   = $form.serialize();

			var $emailField = $form.find('.form-email'),
				$nameField  = $form.find('.form-name');
			
			data = data + '&action=my_subscribers&subscribe_submit=1';

			$.ajax({
				url: ajaxUrl,
				type: 'post',
				dataType: 'json',
				data: data,
				success: function(data) {
					doIfErrors(  data.errors );
					doIfSuccess( data.success );
				}
			});
		});

		function doIfSuccess( success ) {
			if ( undefined === success ) 
				return;

			$parent = $main.parent();

			console.log($parent)

			$main.find( '.success-message' ).removeClass('hidden');
			$main.find( '.hide-on-success' ).addClass('hidden');
		}

		function doIfErrors( errors, $form ) {
			if ( undefined === errors ) 
				return;

			var $emailField = $form.find('.form-email'),
				$nameField  = $form.find('.form-name');

			errorField( errors.email, $emailField, 'email' );
			errorField( errors.name, $nameField, 'name' );
		}

		function errorField( error, field, key ) {
			if ( undefined === error ) {
				field.removeClass('error').find('.message').addClass('hidden');
			} else {
				field.addClass('error');

				if ( 'email' == key ) {
					field.find('.message').addClass('hidden');

					if ( 'empty' == error )
						field.find('.message-empty').removeClass('hidden');
					else if( 'novalid' == error )
						field.find('.message-novalid').removeClass('hidden');
					else
						field.find('.message-dublicate').removeClass('hidden');
				} else {
					field.find('.message').removeClass('hidden');	
				};
			}
		}
	});

}); // End of jQuery shell function