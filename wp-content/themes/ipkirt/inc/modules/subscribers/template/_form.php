<?php if ( ! defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

$data = get_theme_part_data();

$errors = array(
	'email'     => 'Введите правильный электронный адрес.',
	'dublicate' => 'Ваш электронный адрес уже записан в базу.',
);

?>
<div class="my-subscribe-form">
	<div class="subscribe-message">
		<?php if ( isset( $data['error'] ) ): ?>
			<div class="error"><?php echo $errors[$data['error']] ?></div>
		<?php endif ?>
		<?php if ( isset( $data['success'] ) ): ?>
			<div class="success">Спасибо. Ваш электронный адрес добавлен в базу.</div>
		<?php endif ?>
	</div>
	<form action="" method="post">
		<input type="text" name="subscribers[email]" class="email" placeholder="Ваш адрес электронной почты"><?php 
		 ?><input type="submit" value="Узнать первой!" name="subscribe_submit" class="btn"><?php 
		 ?><input type="hidden" name="subscribe_term_id" value="<?php echo $data['term_id'] ?>">
	</form>
</div>