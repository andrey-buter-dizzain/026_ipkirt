<?php if ( ! defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

$data = mysubscr_get_cache();

// $errors = array(
// 	'email'     => 'Введите правильный электронный адрес.',
// 	'dublicate' => 'Ваш электронный адрес уже записан в базу.',
// );

?>
<div class="archive-top-block archive-top-pillows">
	<div><img src="<?php echo home_url() ?>/wp-content/uploads/2014/02/pillows-apsent.jpg" alt=""></div>

	<div class="text-block">
		<div class="success-message hidden">
			<p class="large success">Поздравляем!</p>
			<p class="middle success">Через несколько дней вы первыми узнаете о новых поступлениях.</p>
			<p class="small">&nbsp;</p>
		</div>
		<div class="hide-on-success">
			<p class="large">К сожалению, у нас закончились все подушки для беременных ;(</p>
			<p class="middle">Но уже совсем скоро появится новая коллекция!</p>

			<p class="small error-message-email">&nbsp;</p>
			<p class="small error error-message-email error-message-email-novalid hidden">Введите правильный электронный адрес.</p>
			<p class="small error-message-email error-message-email-empty hidden">Оставьте свой электронный адрес и узнайте об этом первой.</p>
		</div>

		<?php /* ?>
		<p class="small <?php echo $class ?>"><?php echo $message ?></p>		
		<?php */ ?>

		<div class="my-subscribe-form"><?php 
			 ?><form action="" method="post"><?php 
				 ?><input type="text" name="subscribers[email]" class="email" placeholder="Ваш адрес электронной почты"><?php 
				 ?><input id="js-ga-event-my-subscribe" type="submit" value="Узнать первой!" name="subscribers[submit]" class="btn"><?php 
				 ?><input type="hidden" name="subscribers[term_id]" value="<?php echo $data['term_id'] ?>"><?php 
			 ?></form><?php 
		 ?></div>
	</div>
</div>