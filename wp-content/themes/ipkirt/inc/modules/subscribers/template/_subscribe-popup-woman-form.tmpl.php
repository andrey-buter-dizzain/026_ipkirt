<?php if ( ! defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

?>
<div class="subscribe-form">
	<div class="success-message hidden">
		<span class="medium success"><span class="yellow-bg">Поздравляем!</span></span><br>
		<span class="small success"><span class="yellow-bg">Вы первыми узнаете о новых поступлениях.</span></span>
	</div>
	<form action="" method="post" class="hide-on-success">
		<div class="form-item form-name">
			<input type="text" name="subscribers[name]" class="name" placeholder="Имя">
			<div class="message hidden error-message-name">
				Заполните свое имя
			</div>
		</div>
		<div class="form-item form-email">
			<input type="text" name="subscribers[email]" class="email" placeholder="Электронная почта">
			<div class="message message-empty hidden error-message-email error-message-email-empty">
				Заполните свой электронный адрес
			</div>
			<div class="message message-novalid hidden error-message-email error-message-email-novalid">
				Введите правильный электронный адрес
			</div>
		</div>
		<div class="form-submit">
			<input type="submit" value="Узнать первой!" class="btn">
			<input type="hidden" name="subscribers[required]" value="email,name">
		</div>
	</form>
</div>