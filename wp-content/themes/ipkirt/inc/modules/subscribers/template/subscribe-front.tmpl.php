<?php if ( ! defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

$data = mysubscr_get_cache();

$errors = array(
	'empty'     => 'Введите электронный адрес.',
	'email'     => 'Введите правильный электронный адрес.',
	// 'dublicate' => 'Ваш электронный адрес уже записан в базу.',
);

?>
<div style="display:none">
	<div id="subscribe" class="popup">
		<a href="#" class="js-popup-close popup__close">
			<svg class="icon icon_cross">
				<use xlink:href="#icon_cross"></use>
			</svg>
		</a>
		<div class="popup__inner popup__inner_subscribe">
			<h3 class="popup__title">
				<?php _e( 'Подписаться на рассылку' ) ?>
			</h3>
			<div class="js-my-subscribers-from">
				<div class="text-block">
					<div class="success-message hidden">
						<p class="middle success">Спасибо. Вы подписались на нашу рассылку.</p>
					</div>
					<div class="hide-on-success">
						<p class="small error-message-email">&nbsp;</p>
						<p class="small error error-message-email error-message-email-novalid hidden">
							<?php echo $errors['email'] ?>
						</p>
						<p class="small error-message-email error-message-email-empty hidden">
							<?php echo $errors['empty'] ?>
						</p>
					</div>

					<div class="my-subscribe-form hide-on-success"><?php 
						 ?><form action="" method="post"><?php 
							 ?><input type="text" name="subscribers[email]" class="email" placeholder="Ваш адрес электронной почты"><?php 
							 ?><input id="js-ga-event-my-subscribe" type="submit" value="Подписаться" name="subscribers[submit]" class="btn"><?php 
							 ?><input type="hidden" name="subscribers[term_id]" value="<?php echo $data['term_id'] ?>"><?php 
						 ?></form><?php 
					 ?></div>
				</div>
			</div>
		</div>
	</div>
</div>