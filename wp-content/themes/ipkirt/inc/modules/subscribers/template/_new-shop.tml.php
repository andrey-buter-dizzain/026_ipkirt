<?php if ( ! defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

$data = mysubscr_get_cache_data();

$errors = array(
	'email'     => 'Введите правильный электронный адрес.',
	'dublicate' => 'Ваш электронный адрес уже записан в базу.',
);

?>
<div id="js-my-subscribers-from" class="new-shop-subscribe">
	<form id="js-new-shop-form" action="" method="post" class="">
		<div class="form-item form-name">
			<input type="text" name="subscribe_name" class="name" placeholder="Имя">
			<span class="message hidden">А как вас зовут?</span>
		</div>
		<div class="form-item form-email">
			<input type="text" name="subscribe_email" class="email" placeholder="Email">
			<span class="message message-empty hidden">Мы никому не скажем ваш email, честное слово!</span>
			<span class="message message-novalid hidden">Укажите правильный адрес электронной почты.</span>
			<span class="message message-dublicate hidden">Ваш электронной адрес уже записан.</span>
		</div>
		<div class="form-item">
			<input id="js-ga-event-my-subscribe" type="submit" value="Узнать первой!" name="subscribe_submit" class="btn">
			<input type="hidden" name="subscribe_term_id" value="0">
		</div>
	</form>
	<div class="success-message hidden">
		<span class="yellow-bg">Спасибо, что оставили заявку. Вы узнаеет первой об открытии магазина!</span>
	</div>
</div>