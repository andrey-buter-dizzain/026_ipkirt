<?php 

function mysubscr_get_admin_url( $args = array() ) {
	$url = 'my-subscribers';

	if ( empty( $args ) )
		return $url;

	$url = 'admin.php?page=' . $url;

	return add_query_arg( $args, $url );
}

/**
 * Use shortcode [my_subscribers term_id=14 template="pillows.tml"]
 */
add_shortcode( 'my_subscribers', 'my_subscribers_shortcode' );
function my_subscribers_shortcode( $args ) {
	$default = array(
		'term_id'  => 0,
		'template' => 'form',
		'ajax'     => true
	);

	// $response = $this->validate_form();
	// if ( $response )
	// 	$args = array_merge( $args, $response );

	$args = wp_parse_args( $args, $default );

	$template = $args['template'];

	$output = get_theme_part( MY_SUBSCRIBERS_PATH .'/template', $template, array(
		'return' => true,
		'data'   => $args
	) );

	// if enabled ajax
	if ( 'true' === $args['ajax'] OR 1 == $args['ajax'] ) {
		add_action( 'wp_footer', 'mysybscr_enqueue_scripts', 100 );

		$output = '<div class="js-my-subscribers-from">'. $output .'</div>';
	}

	return $output;
}

function mysybscr_enqueue_scripts() {
	echo '<script type="text/javascript" src="'. MY_SUBSCRIBERS_URI .'/assets/my-subscribers.js"></script>';
}


/**
 * Set cache
 */
function mysubscr_set_cache( $data, $key = 'template-data' ) {
	wp_cache_set( $key, $data, 'my-subscribers' );
}

/**
 * Cache get
 */
function mysubscr_get_cache( $default = null, $key = 'template-data' ) {
	$data = wp_cache_get( $key, 'my-subscribers' );

	return $data ? $data : $default;
}

/**
 * Delete cache
 */
function mysubscr_delete_cache( $key = 'template-data' ) {
	wp_cache_delete( $key, 'my-subscribers' );
}