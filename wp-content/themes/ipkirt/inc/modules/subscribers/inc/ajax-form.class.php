<?php 

if ( !defined( 'DOING_AJAX' ) )
	return;

if ( true !== DOING_AJAX )
	return;

/**
 * 
 */
class Module_Subscribe_Ajax_Form
{	
	protected $default = array(
		'required' => array(
			'email',
		),
		'term_id'  => 0,
		'ip'       => '',
	);

	protected $data = array();

	/**
	 * Is email dublicate
	 */
	protected $is_dublicate = null;

	function __construct()
	{
		add_action( 'wp_ajax_my_subscribers', array( $this, 'ajax_submit_form' ) );
		add_action( 'wp_ajax_nopriv_my_subscribers', array( $this, 'ajax_submit_form' ) );
	}

	function ajax_submit_form() 
	{
		if ( ! $this->set_data() )
			return;

		$response = $this->validate_form();

		echo json_encode( $response );

		exit;
	}

	protected function set_data()
	{
		if ( ! isset( $_POST['subscribers'] ) )
			return false;

		$data = $_POST['subscribers'];

		if ( empty( $data ) )
			$data = array();

		$data = $this->set_requireds( $data );

		$data = wp_parse_args( $data, $this->default );

		$data['ip'] = $_SERVER['REMOTE_ADDR'];		

		$this->data = $data;

		return true;
	}

	protected function set_requireds( $data )
	{
		$required = explode( ',', $data['required'] );

		$required = array_filter( $required, 'trim' );
		$required = wp_parse_args( $this->default['required'], $required );

		$data['required'] = array_flip( $required );

		return $data;
	}

	protected function get_data( $key = '', $default = null )
	{
		$data = $this->data;

		if ( !$key )
			return $data;

		return isset( $data[$key] ) ? $data[$key] : $default;
	}

	protected function is_required( $field_key )
	{
		$required = $this->get_data( 'required' );

		return isset( $required[$field_key] );
	}

	protected function validate_form()
	{
		$errors = array();

		$data = $this->get_data();

		if ( $this->is_required( 'email' ) ) {
			if ( empty( $data['email'] ) ) 
				$errors['email'] = 'empty';
			elseif ( !is_email( $data['email'] ) )
				$errors['email'] = 'novalid';
				// return array( 'error' => 'email' );
				// return array( 'error' => 'Введите правильный электронный адрес.' );
		}

		if ( $this->is_required( 'name' ) AND empty( $data['name'] ) )
			$errors['name'] = '';

		// если не пройдена певричная валидация на правильность и заполненность полей
		if ( !empty( $errors ) )
			return array( 'errors' => $errors );
/*
		if ( $this->is_email_dublicate( $data ) )
			$errors['email'] = 'dublicate';
			// return array( 'errors' => 'dublicate' );
			// return array( 'error' => 'Ваш электронный адрес уже записан в базу.' );

		// если не пройдена вторичная валидация
		if ( !empty( $errors ) )
			return array( 'errors' => $errors );
*/
		$this->save_subscriber( $data );

		return array( 'success' => '' );
	}

	protected function save_subscriber( $data ) 
	{
		global $wpdb;

		if ( $this->is_email_dublicate( $data ) )
			return;

		$fields = array(
			'id'      => 0,
			'email'   => '',
			'name'    => '',
			'ip'      => '',
			'term_id' => 0
		);

		$data = wp_parse_args( $data, $fields );

		extract( $data );
		$wpdb->get_results( "INSERT INTO $wpdb->my_subscribers( email, name, ip, term_id ) values( '$email', '$name', '$ip', '$term_id' )" );
	}

	/**
	 * Check is current email dublicate
	 */
	protected function is_email_dublicate( $data )
	{
		global $wpdb;

		if ( null !== $this->is_dublicate )
			return $this->is_dublicate;

		extract( $data );

		$count = $wpdb->get_var( "SELECT count(*) FROM $wpdb->my_subscribers where email = '$email'" );

		$this->is_dublicate = absint( $count ) >= 1;

		return $this->is_dublicate;
	}
}

new Module_Subscribe_Ajax_Form;