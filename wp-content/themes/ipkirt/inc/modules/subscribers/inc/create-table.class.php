<?php 

/**
 * 
 */
class Module_Subscribe_Table
{	
	function __construct()
	{
		global $wpdb;

		$wpdb->my_subscribers = $wpdb->prefix . 'my_subscribers';

		$this->create_table();
	}

	protected function create_table()
	{
		global $wpdb;
		$wpdb->show_errors();

		$sql = "CREATE TABLE IF NOT EXISTS 
				$wpdb->my_subscribers( 
					id int(50) primary key auto_increment not null,
				    email varchar(50) not null,
				    name varchar(50) not null,
				    ip varchar(50) not null,
				    term_id int(50) not null
				)";

		$wpdb->get_results( $sql );
	}
}
new Module_Subscribe_Table;