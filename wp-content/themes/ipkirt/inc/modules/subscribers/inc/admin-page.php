<?php 

/**
 * 
 */
class Module_Subscribe_Admin_Page
{
	function __construct()
	{
		add_action( 'admin_menu', array( $this, 'admin_menu_init' ) );
	}

	function admin_menu_init()
	{
		$admin_url = mysubscr_get_admin_url();

		add_menu_page( 
			'Manage Subscribers', 
			'Subscribes', 
			'manage_options', 
			$admin_url, 
			array( $this, 'content' ) 
		);
	}

	function content() 
	{	
		$this->delete_subscriber();
		$data = $this->get_page_data();

		get_theme_part( MY_SUBSCRIBERS_PATH .'/template/admin', 'page-admin', array(
			'data' => $data
		) );
	}

	protected function delete_subscriber()
	{
		if ( !isset( $_POST['subscribe_del'] ) )
			return;

		global $wpdb;

		$del = array();

		foreach ( $_POST as $key => $value ) {
			if ( strpos( $key, 'delete' ) === false )
				continue;

			preg_match( '/[0-9]+/', $key, $matches );

			$del[] = "'" . $matches[0] . "'";
		}

		if ( empty( $del ) ) {
			$_POST['error'] = '';
			return;
		}

		$ids = implode( ',', $del );
		$wpdb->get_results( "DELETE FROM $wpdb->my_subscribers where id IN ( $ids )" );

		$_POST['success'] = '';
	}

	protected function get_page_data()
	{
		$count = $this->get_subscribers_count();
		$list  = $this->get_subscribers_list();
		$pages = $this->get_arg_pages();

		return array(
			'count' => $count,
			'pages' => $pages,
			'list'  => $list
		);
	}

	protected function get_subscribers_count()
	{
		global $wpdb;
		return $wpdb->get_var( "SELECT COUNT(*) FROM $wpdb->my_subscribers" );
	}

	protected function get_subscribers_list()
	{
		global $wpdb;

		// $per_page = 10;
		// $pages    = $this->get_arg_pages();
		// $start    = $pages * $per_page;

		return $wpdb->get_results( "SELECT * FROM $wpdb->my_subscribers" );
	}

	protected function get_arg_pages()
	{
		return isset( $_GET['pages'] ) ? ( absint( $_GET['pages'] ) - 1 ) : 0;
	}
}

new Module_Subscribe_Admin_Page;