<?php 

/**
 * Use shortcode [my_subscribers term_id=14 template="pillows.tml"]
 */


$folder = basename( dirname( __FILE__ ) );

if ( ! defined( 'MY_SUBSCRIBERS_PATH' ) )
	define( 'MY_SUBSCRIBERS_PATH', THEME_MODULE_PATH .'/'. $folder );

if ( ! defined( 'MY_SUBSCRIBERS_ABSPATH' ) )
	define( 'MY_SUBSCRIBERS_ABSPATH', THEME_MODULE_ABS .'/'. $folder );

if ( ! defined( 'MY_SUBSCRIBERS_URI' ) )
	define( 'MY_SUBSCRIBERS_URI', THEME_MODULE_URI .'/'. $folder );


require_once( MY_SUBSCRIBERS_ABSPATH .'/inc/create-table.class.php' );
require_once( MY_SUBSCRIBERS_ABSPATH .'/inc/public.php' );

if ( !is_admin() )
	return;

require_once( MY_SUBSCRIBERS_ABSPATH .'/inc/ajax-form.class.php' );
require_once( MY_SUBSCRIBERS_ABSPATH .'/inc/admin-page.php' );