<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 
/**
 * Page module
 */

class Inc_Mass_Media extends Inc_Load_Section
{
	public static $section = 'mass-media';

	protected static $include_path;

	protected static $uri_path;

	public static $partials = array(
		'model',
		// 'meta',
	);
}
Inc_Mass_Media::load();