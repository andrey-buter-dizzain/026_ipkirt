<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 
/**
 * Page module
 */

class Inc_Page extends Inc_Load_Section
{
	public static $section = 'page';

	protected static $include_path;

	protected static $uri_path;

	public static $partials = array(
		'meta-faq',
		'meta-partners',
		"meta-contacts",
		"meta-study",
	);
}
Inc_Page::load();