<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

/**
 * Handle every member aspect
 */

add_filter( 'populate_theme_meta_boxes', 'populate_page_meta_boxes' );
function populate_page_meta_boxes( $meta_boxes = array() ) {
	$post_type = 'page';

	$prefix = "{$post_type}_";

	$meta_boxes[] = array(
		'id'       => "{$prefix}section",
		'title'    => __( 'Page Meta', 'nrec-admin' ),
		'pages'    => array( $post_type ),
		'context'  => 'normal',
		'priority' => 'default',
		'fields'   => array(
			array(
				'label'   => __( 'Page Title', 'nrec-admin' ),
				'id'      => "{$prefix}title",
				'type'    => 'text',
			),
			array(
				'label'   => __( 'Page Title Caption', 'nrec-admin' ),
				'id'      => "{$prefix}title_caption",
				'type'    => 'textarea-simple',
				'rows'    => 2
			),
		),
	);

	return $meta_boxes;
}