<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

/**
 * Handle every member aspect
 */

add_filter( 'populate_theme_meta_boxes', 'populate_study_meta_boxes' );
function populate_study_meta_boxes( $meta_boxes = array() ) {
	$post_type = 'page';

	$prefix = "{$post_type}_study_";

	$meta_boxes[] = array(
		'id'       => "{$prefix}section",
		'title'    => __( 'Блок Ближайшие события', 'ipkirt-admin' ),
		'pages'    => array( $post_type ),
		'context'  => 'normal',
		'priority' => 'default',
		'fields'   => array(
			array(
				'label'   => __( 'Количеаство событий', 'ipkirt-admin' ),
				'id'      => "{$prefix}count_type",
				'type'    => 'text',
				'std'     => 5,
				'desc'    => __( 'По умолчанию: 5' )
			),
		),
		'only_on' => array(
			'function' => 'is_template_study'
		)
	);

	return $meta_boxes;
}

function rw_is_template_study() {
	return rw_is_custom_page_template( 'study' );
}