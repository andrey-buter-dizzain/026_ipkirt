<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

/**
 * Handle every member aspect
 */

add_filter( 'populate_theme_meta_boxes', 'populate_contacts_meta_boxes' );
function populate_contacts_meta_boxes( $meta_boxes = array() ) {
	$post_type = 'page';

	$prefix = "{$post_type}_contacts_";

	$meta_boxes[] = array(
		'id'       => "{$prefix}section",
		'title'    => __( 'Контакты', 'ipkirt-admin' ),
		'pages'    => array( $post_type ),
		'context'  => 'normal',
		'priority' => 'default',
		'fields'   => array(
			array(
				'label'   => __( 'Список схем/карт', 'ipkirt-admin' ),
				'id'      => "{$prefix}maps",
				'type'     => 'list-item',
				'settings' => array(
					// array(
					// 	'label' => __( 'Основной тайтл', 'ipkirt-admin' ),
					// 	'id'    => 'title',
					// 	'type'  => 'text',
					// ),
					array(
						'label' => __( 'Название схемы', 'ipkirt-admin' ),
						'id'    => 'sub_title',
						'type'  => 'text',
					),
					array(
						'label' => __( 'Описание', 'ipkirt-admin' ),
						'id'    => 'text',
						'type'  => 'textarea-simple',
						'rows'  => 3,
					),
					array(
						'label' => __( 'Картинка карты', 'ipkirt-admin' ),
						'id'    => 'image',
						'type'  => 'upload',
						'class' => 'ot-upload-attachment-id',
					),
					array(
						'label' => __( 'Ссылка на карту', 'ipkirt-admin' ),
						'id'    => 'link',
						'type'  => 'text',
					),
				),
			),
		),
		'only_on' => array(
			'function' => 'is_template_contacts'
		)
	);

	return $meta_boxes;
}

function rw_is_template_contacts() {
	return rw_is_custom_page_template( 'contacts' );
}