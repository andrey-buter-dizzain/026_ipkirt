<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

/**
 * Handle every member aspect
 */

add_filter( 'populate_theme_meta_boxes', 'populate_faq_meta_boxes' );
function populate_faq_meta_boxes( $meta_boxes = array() ) {
	$post_type = 'page';

	$prefix = "{$post_type}_faq_";

	$meta_boxes[] = array(
		'id'       => "{$prefix}section",
		'title'    => __( 'FAQ', 'ipkirt-admin' ),
		'pages'    => array( $post_type ),
		'context'  => 'normal',
		'priority' => 'default',
		'fields'   => array(
			array(
				'label'   => __( 'Список', 'ipkirt-admin' ),
				'id'      => "{$prefix}items",
				'type'     => 'list-item',
				'settings' => array(
					array(
						'label' => __( 'Ответ', 'ipkirt-admin' ),
						'id'    => 'text',
						'type'  => 'textarea-simple',
						'rows'  => 6
					),
				),
			),
		),
		'only_on' => array(
			'function' => 'is_template_faq'
		)
	);

	return $meta_boxes;
}

function rw_is_template_faq() {
	return rw_is_custom_page_template( 'faq' );
}