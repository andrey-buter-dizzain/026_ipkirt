<?php if ( !defined( 'ABSPATH' ) ) exit( 'No direct script access allowed' ); 

/**
 * Handle every member aspect
 */

add_filter( 'populate_theme_meta_boxes', 'populate_partners_meta_boxes' );
function populate_partners_meta_boxes( $meta_boxes = array() ) {
	$post_type = 'page';

	$prefix = "{$post_type}_partners_";

	$meta_boxes[] = array(
		'id'       => "{$prefix}section",
		'title'    => __( 'Партнеры', 'ipkirt-admin' ),
		'pages'    => array( $post_type ),
		'context'  => 'normal',
		'priority' => 'default',
		'fields'   => array(
			array(
				'label'   => __( 'Список', 'ipkirt-admin' ),
				'id'      => "{$prefix}items",
				'type'     => 'list-item',
				'settings' => array(
					array(
						'label' => __( 'Логотип', 'ipkirt-admin' ),
						'id'    => 'logo',
						'type'  => 'upload',
						'class' => 'ot-upload-attachment-id',
					),
					array(
						'label' => __( 'URL', 'ipkirt-admin' ),
						'id'    => 'url',
						'type'  => 'text',
					),
				),
			),
		),
		'only_on' => array(
			'function' => 'is_template_partners'
		)
	);

	return $meta_boxes;
}

function rw_is_template_partners() {
	return rw_is_custom_page_template( 'partners' );
}